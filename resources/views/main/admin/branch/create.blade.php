@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        เพิ่มข้อมูล
        <small>ข้อมูลสาขา</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/branch')}}"> จัดการข้อมูลสาขา </a></li>
        <li class="active">เพิ่มข้อมูลสาขา</li>
    </ol>
</section>

<section class="content">
	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> {{Session::get('message')}}
	</div>
	@endif

	@if(Session::has('message_fali'))
	<div class="alert alert-danger alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Fail!</strong> {{Session::get('message_fali')}}
	</div>
	@endif

	<form role="form" method="post" action="{{ url('admin/branch/create-process') }}" id="form-edit" enctype="multipart/form-data" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">
					เพิ่มข้อมูล
				</h3>
			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-md-2">
						<label for="registration_date">ชื่อสาขา :</label>
					</div>
					<div class="col-md-4">
						<div class="form-group">
	              			<input type="text" name="branch_name" class="form-control pull-right">
	            			{!!$errors->first('branch_name', '<span class="control-label" style="color:#FF9494" for="branch_name">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="bill_name">ชื่อร้าน :</label>
					</div>
					<div class="col-md-6">
						<div class="form-group">
	            			<input type="text" name="bill_name" class="form-control pull-right">
	            			{!!$errors->first('bill_name', '<span class="control-label" style="color:#FF9494" for="bill_name">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="bill_address">ที่อยู่ร้าน :</label>
					</div>
					<div class="col-md-8">
						<div class="form-group">
	            			<input type="text" name="bill_address" class="form-control pull-right">
	            			{!!$errors->first('bill_address', '<span class="control-label" style="color:#FF9494" for="bill_address">*:message</span>')!!}
						</div>		
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						
						<a href="{{ url('admin/branch') }}" class="btn btn-primary" ><i class="fa fa-arrow-left"></i> กลับ </a>
					</div>
					<div class="col-md-6">
						
						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> บันทึก </button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
@stop