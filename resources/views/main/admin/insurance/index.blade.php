@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        จัดการข้อมูลบริษัทประกัน
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">จัดการข้อมูลบริษัทประกัน</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <a href="{{ url('admin/insurance/create-company') }}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูล</a>
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%" style="text-align:center">#</th>
                                        <th>ชื่อบริษัท</th>
                                        <th width="10%" style="text-align:center" >ซ่อน/แสดง</th>
                                        <th width="10%" style="text-align:center" >แก้ไข</th>
                                        <th width="10%" style="text-align:center" >ดูข้อมูล</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($list['data'] as $key => $value) {
                                            if($value['active']==1){
                                                $isHidden = '<span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="color:green;"></span>';
                                            } else {
                                                $isHidden = '<span class="glyphicon glyphicon-eye-close" aria-hidden="true" style="color:#ff4d4d;"></span>';
                                            }
                                        ?>
                                            <tr>
                                                <td align="center">{{ $key+1 }}</td>
                                                <td>    {{ $value['company_name'] }}</td>
                                                <td align="center" >
                                                    <a href="{{ url('admin/insurance/hide-company/'.$value['id']).'/'.$value['active'] }}" style="font-size:18px"><?php echo $isHidden;?></a>
                                                </th>
                                                <td align="center"><a href="{{ url('admin/insurance/edit-company/'.$value['id']) }}" style="font-size:18px"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                                <td align="center"><a href="{{ url('admin/insurance/view/'.$value['id']) }}" style="font-size:18px"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite" style="margin-top:15px">
                                Showing {{ $list['from'] }} to {{ $list['to'] }} of {{ $list['total'] }} entries
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <?php 
                                $input = Request::all();
                                unset($input['page']);
                                $param = http_build_query($input);
                            ?>
                            <nav class="pull-right">
                                <ul class="pagination">
                                    <li class="{{ $pages->currentPage() == 1 ? 'disabled' : '' }}">
                                      <a href="{{ $pages->currentPage() == 1 ? '#' : $pages->previousPageUrl().'&'.$param }}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                      </a>
                                    </li>
                                    <?php
                                        for($i=1 ; $i<=$pages->lastPage() ; $i++) {
                                        ?>
                                            <li class="{{ $pages->currentPage() == $i ? 'active':'' }}"><a href="{{ $pages->url($i).'&'.$param }}">{{ $i }}</a></li>
                                        <?php
                                        }
                                    ?>
                                    <li class="{{ $pages->currentPage() == $pages->lastPage() ? 'disabled' : '' }}">
                                        <a href="{{ $pages->currentPage() == $pages->lastPage() ? '#' : $pages->nextPageUrl().'&'.$param }}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>


@stop