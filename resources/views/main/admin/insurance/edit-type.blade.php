@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        แก้ไข้อมูลประเภทประกัน
        <small>{{ $company_name }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/insurance')}}"> จัดการข้อมูลประกัน </a></li>
        <li class="active">แก้ไขข้อมูลประเภทประกัน</li>
    </ol>
</section>


	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> {{Session::get('message')}}
	</div>
	@endif

	@if(Session::has('message_fali'))
	<div class="alert alert-danger alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Fail!</strong> {{Session::get('message_fali')}}
	</div>
	@endif

	<form role="form" method="post" action="{{ url('admin/insurance/edit-type-process') }}" id="form-edit" enctype="multipart/form-data" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id" value="{{ Request::segment(4) }}">
		<input type="hidden" name="id_company" value="{{ $data['insurance_company_id'] }}">

		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">
					ประเภทประกันบริษัท {{ $company_name }}
				</h3>
			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-md-2">
						<label for="registration_date">ชื่อประเภท :</label>
					</div>
					<div class="col-md-4">
						<div class="form-group">
	              			<input type="text" name="type_name" class="form-control pull-right" value="{{ $data['type_name'] }}">
	            			{!!$errors->first('type_name', '<span class="control-label" style="color:#FF9494" for="registration_date">*:message</span>')!!}
						</div>		
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						
						<a href="{{ url('admin/insurance/view/'.$data['insurance_company_id']) }}" class="btn btn-primary" ><i class="fa fa-arrow-left"></i> กลับ </a>
					</div>
					<div class="col-md-6">
						
						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> แก้ไข </button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
@Stop