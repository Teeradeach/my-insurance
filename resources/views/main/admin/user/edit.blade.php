@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        แก้ไข้อมูล
        <small>ข้อมูลผู้ใช้งาน</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/user')}}"> จัดการข้อมูลผู้ใช้งาน </a></li>
        <li class="active">แก้ไขข้อมูลผู้ใช้งาน</li>
    </ol>
</section>

<section class="content">
	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> {{Session::get('message')}}
	</div>
	@endif

	@if(Session::has('message_fali'))
	<div class="alert alert-danger alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Fail!</strong> {{Session::get('message_fali')}}
	</div>
	@endif

	<form role="form" method="post" action="{{ url('admin/user/edit-process') }}" id="form-edit" enctype="multipart/form-data" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id" value="{{ Request::segment(4) }}">

		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">
					ข้อมูลสาขา
				</h3>
			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-md-2">
						<label for="name">ชื่อ-สกุล :</label>
					</div>
					<div class="col-md-4">
						<div class="form-group">
	              			<input type="text" name="name" class="form-control pull-right" value="{{ $data['name'] }}">
	            			{!!$errors->first('name', '<span class="control-label" style="color:#FF9494" for="name">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="username">ชื่อผู้ใช้ :</label>
					</div>
					<div class="col-md-6">
						<div class="form-group">
	            			<input type="text" name="username" class="form-control pull-right" value="{{ $data['username'] }}">
	            			{!!$errors->first('username', '<span class="control-label" style="color:#FF9494" for="username">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="type">ประเภท :</label>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<select name="type" class="form-control pull-right">
								<option <?php echo $data['type']=='admin'? 'selected':'' ?> value="admin" >admin</option>
								<option <?php echo $data['type']=='user'? 'selected':'' ?> value="user" >user</option>
							</select>
	            			{!!$errors->first('type', '<span class="control-label" style="color:#FF9494" for="type">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="type">สาขา :</label>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<select name="branch" class="form-control pull-right">
								<?php
									foreach ($branch as $key => $val) 
									{
								?>
										<option <?php echo $data['branch_id']==$val['id']? 'selected':'' ?> value="<?php echo $val['id']?>" ><?php echo $val['branch_name']?></option>
								<?php
									}
								?>
							</select>
	            			{!!$errors->first('type', '<span class="control-label" style="color:#FF9494" for="type">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="password">รหัสผ่านใหม่ :</label>
					</div>
					<div class="col-md-4">
						<div class="form-group">
	            			<input type="password" name="password" class="form-control pull-right" placeholder="Password">
	            			{!!$errors->first('password', '<span class="control-label" style="color:#FF9494" for="password">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						<label for="password">ยืนยันรหัสผ่านใหม่ :</label>
					</div>
					<div class="col-md-4">
						<div class="form-group">
	            			<input type="password" name="password_confirmation" class="form-control pull-right" placeholder="Retype password">
	            			{!!$errors->first('password_confirmation', '<span class="control-label" style="color:#FF9494" for="password_confirmation">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						
						<a href="{{ url('admin/branch') }}" class="btn btn-primary" ><i class="fa fa-arrow-left"></i> กลับ </a>
					</div>
					<div class="col-md-6">
						
						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> แก้ไข </button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
@stop