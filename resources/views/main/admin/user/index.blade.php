@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        จัดการข้อมูลผู้ใช้งาน
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">จัดการข้อมูลผู้ใช้งาน</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <a href="{{ url('admin/register') }}" class="btn btn-primary pull-right" target="_blank"><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูล</a>
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%" style="text-align:center">#</th>
                                        <th>ชื่อ</th>
                                        <th>ชื่อผู้ใช่งาน</th>
                                        <th width="10%" style="text-align:center" >ประเภท</th>
                                        <th width="10%" style="text-align:center" >สาขา</th>
                                        <th width="10%" style="text-align:center" >แก้ไข</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($list as $key => $value) {
                                        ?>
                                            <tr>
                                                <td align="center">{{ $key+1 }}</td>
                                                <td>    {{ $value['name'] }}</td>
                                                <td>    {{ $value['username'] }}</td>
                                                <td align="center">    {{ $value['type'] }}</td>
                                                <td align="center">    {{ $value['branch']['branch_name'] }}</td>
                                                
                                                <td align="center"><a href="{{ url('admin/user/edit/'.$value['id']) }}" style="font-size:18px"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>


@stop