@extends('main.layouts.template-print')
@section('content')
	<div class="row">
		<div class="col-sm-offset-3 col-sm-6" style="text-align: center;">
			<br/><br/><br/>
			<img src="{{ asset('/assets/images/spin.gif') }}">
		</div>
	</div>
	<form method="post" id="form-print-bill" action="{{url('print/bill')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="hidden" name="cus_id" value="{{ $input['cus_id'] }}">
    	<input type="hidden" name="regis_id" value="{{ $input['regis_id'] }}">
    	<input type="hidden" name="register_expired_date" value="{{ $input['register_expired_date'] }}">
    	<input type="hidden" name="total" id="total" value="{{ $input['total'] }}">
    	<input type="hidden" name="bill_id" value="{{ array_get($input, 'bill_id', 0) }}">
    	<input type="hidden" name="bill_no" value="{{ array_get($input, 'bill_no', 0) }}">
    	<input type="hidden" name="bill_type" value="{{ array_get($input, 'bill_type', 1) }}">
		
		<?php 
			if(isset($input['act']) && $input['act']['price'] > 0)
			{
				echo '<input type="hidden" name="act[chk]" value="'.$input['act']['chk'].'">';
				echo '<input type="hidden" name="act[price]" value="'.$input['act']['price'].'">';
				echo '<input type="hidden" name="act[text]" value="'.$input['act']['text'].'">';
			}
			if(isset($input['vat']) && $input['vat']['price'] > 0)
			{
				echo '<input type="hidden" name="vat[chk]" value="'.$input['vat']['chk'].'">';
				echo '<input type="hidden" name="vat[price]" value="'.$input['vat']['price'].'">';
			}
			if(isset($input['tra']) && $input['tra']['price'] > 0)
			{
				echo '<input type="hidden" name="tra[chk]" value="'.$input['tra']['chk'].'">';
				echo '<input type="hidden" name="tra[price]" value="'.$input['tra']['price'].'">';
			}
			if(isset($input['insurance']['price']) && $input['insurance']['price'] > 0)
			{
				echo '<input type="hidden" name="insurance[chk]" value="'.$input['insurance']['chk'].'">';
				// echo '<input type="hidden" name="insurance[company]" value="'.$input['insurance']['company'].'">';
				// echo '<input type="hidden" name="insurance[type]" value="'.$input['insurance']['type'].'">';
				echo '<input type="hidden" name="insurance[text]" value="'.$input['insurance']['text'].'">';
				echo '<input type="hidden" name="insurance[price]" value="'.$input['insurance']['price'].'">';
			}
			if(isset($input['service']) && $input['service']['price'] > 0)
			{
				echo '<input type="hidden" name="service[chk]" value="'.$input['service']['chk'].'">';
				echo '<input type="hidden" name="service[price]" value="'.$input['service']['price'].'">';
			}
			if(isset($input['discount']) && $input['discount']['price'] > 0)
			{
				echo '<input type="hidden" name="discount[chk]" value="'.$input['discount']['chk'].'">';
				echo '<input type="hidden" name="discount[price]" value="'.$input['discount']['price'].'">';
			}
			if(isset($input['etc']) && $input['etc']['price'] > 0)
			{
				echo '<input type="hidden" name="etc[chk]" value="'.$input['etc']['chk'].'">';
				echo '<input type="hidden" name="etc[price]" value="'.$input['etc']['price'].'">';
				echo '<input type="hidden" name="etc[text]" value="'.$input['etc']['text'].'">';
			}

            if ( array_get($input, 'version') == 2)
            {
                echo '<input type="hidden" name="version" value="2">';
            }

		?>
	</form>
@stop

@section('scripts')
	<script>
		$( document ).ready(function() {
			$('#form-print-bill').submit();
		});
	</script>
@stop

