@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        ออกบิล
        <small>พ.ร.บ. ภาษี ตรอ.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">ตรวจสภาพรถ</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ค้นหาข้อมูลเจ้าของรถ</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- form search -->
                    <form role="form" method="get">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <input type="text" name="regis_number" class="form-control" placeholder="เลขทะเบียนรถ" value="{{Request::input('regis_number')}}">
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <input type="text" name="fullName" class="form-control" placeholder="ชื่อ-สกุล" value="{{Request::input('fullName')}}">
                                </div>       
                            </div>

                            <div class="col-xs-1">
                                <div class="form-group">
                                    <select name="limit" class="form-control">
                                        <option value="20">Limit</option>
                                        <option value="20" {{Request::input('limit')==20?'selected':''}} >20</option>
                                        <option value="50" {{Request::input('limit')==50?'selected':''}} >50</option>
                                        <option value="100" {{Request::input('limit')==100?'selected':''}} >100</option>
                                        <option value="500" {{Request::input('limit')==500?'selected':''}} >500</option>
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"> ค้นหา </button>
                                </div>       
                            </div>

                            <div class="col-xs-2">
                                <a href="{{ url('renew-act/bill-create') }}" class="btn btn-success pull-right"> ออกบิลใหม่ </a>
                            </div>
                        </div>
                    </form>


                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ทะเบียนรถ</th>
                                        <th>วันจดทะเบียน</th>
                                        <th>วันหมดอายุ</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ที่อยู่</th>
                                        <th>เบอร์โทรศัพท์</th>
                                        <th>ดูข้อมูล</th>
                                        <th>ออกบิล</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($list['data'] as $key => $value) {
                                            $classRedColor = '';
                                            $str = (strtotime($value['register_expired_date'])) - strtotime(date("Y-m-d"));
                                            if(floor($str/3600/24) < 30){
                                                $classRedColor = 'style="color:red"';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{ $value['register_number'] }}</td>
                                                <td>{{ convert_TH_FormatDate($value['register_date']) }}</td>
                                                <td <?php echo $classRedColor; ?> >
                                                    {{ convert_TH_FormatDate($value['register_expired_date']) }}

                                                </td>
                                                <td>{{ $value['customer']['person_info']['prefix']['prefix_name'] }} {{ $value['customer']['person_info']['first_name'] }} {{ $value['customer']['person_info']['last_name'] }}</td>
                                                <td>{{ $value['customer']['person_info']['address'] }}</td>
                                                <td>{{ $value['customer']['person_info']['phone'] }}</td>
                                                <td align="center"><a href="{{ url('renew-act/detail/'.$value['customer']['id'].'/'.$value['id']) }}" style="font-size:18px"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></td>
                                                <td align="center"><a href="{{ url('renew-act/bill/'.$value['customer']['id'].'/'.$value['id']) }}" style="font-size:18px"><span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span></a></td>                                        
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite" style="margin-top:15px">
                                Showing {{ $list['from'] }} to {{ $list['to'] }} of {{ $list['total'] }} entries
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <?php 
                                $input = Request::all();
                                unset($input['page']);
                                $param = http_build_query($input);
                            ?>
                            <nav class="pull-right">
                                <ul class="pagination">
                                    <li class="{{ $pages->currentPage() == 1 ? 'disabled' : '' }}">
                                      <a href="{{ $pages->currentPage() == 1 ? '#' : $pages->previousPageUrl().'&'.$param }}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                      </a>
                                    </li>
                                    <?php
                                        for($i=1 ; $i<=$pages->lastPage() ; $i++) {
                                        ?>
                                            <li class="{{ $pages->currentPage() == $i ? 'active':'' }}"><a href="{{ $pages->url($i).'&'.$param }}">{{ $i }}</a></li>
                                        <?php
                                        }
                                    ?>
                                    <li class="{{ $pages->currentPage() == $pages->lastPage() ? 'disabled' : '' }}">
                                        <a href="{{ $pages->currentPage() == $pages->lastPage() ? '#' : $pages->nextPageUrl().'&'.$param }}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

@stop