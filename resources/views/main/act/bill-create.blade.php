@extends('main.layouts.template')
@section('stylesheet')
<?php
$bill_header = 'บิลเงินสด';
if (Request::input('bill_type') == 2) {
    $bill_header = "ใบแจ้งหนี้";
}
?>
	<link rel="stylesheet" href="{{ URL::asset('/assets/plugins/iCheck/all.css') }}">
    <style>
    /*    table, td, th {
            border: 1px solid #AAA;
            border-bottom-color: #AAA;
        }*/
        .table {
            border: 0.5px solid #AAA;
        }
        .table-bordered > thead > tr > th,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > tbody > tr > td,
        .table-bordered > tfoot > tr > td {
            border: 0.5px solid #AAA;
        }
        input[type=checkbox] {
            transform: scale(1.2);
        }

        /*input {
            box-sizing: border-box;
            border: 1px solid #ccc;
            height: 30px;
            padding: 10px;
        }*/
        input.loading {
            background: url({{ asset('/assets/images/loading.gif') }}) no-repeat right center;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop
@section('content')
<section class="content-header">
    <h1>
     -
        <?php
        /* if (Request::input('bill_type') == 2) {
            echo "ออกใบแจ้งหนี้";
        } else {
            echo "ออกบิลเงินสด";
        } */
        ?>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('renew-act')}}">ต่อ พ.ร.บ. ภาษี</a></li>
        <li><a href="{{url('renew-act/detail')}}">ข้อมูลเจ้าของรถ</a></li>
        <li class="active">ออกบิล</li>
    </ol>
</section>

<section class="content">
    <form method="post" id="form-create-bill" action="{{url('renew-act/process-bill-v2')}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">

                <div class="box-body">
                    <div class="alert <?php echo Request::input('bill_type') == 2 ? 'alert-warning': 'alert-success'?>" role="alert">
                    <span style="font-size:20px;font-weight: bold">
                        <?php echo $bill_header; ?>
                    </span>
                        
                    </div>
                    <div class="row" style="margin-bottom:40px">
                        <div class="col-xs-3">
                            <div class="form-inline">
                                <label>ค้นหาทะเบียน :</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="search_regis" name="search_regis" placeholder="ค้นหาทะเบียน" value="{{old('search_regis')}}" autocomplete="off">
                                    <button type="button" onclick="searchRegis()" class="btn btn-primary">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>คำนำหน้าชื่อ :</label>
                                <select class="form-control" name="prefix_id" id="prefix_id">
                                    <?php
                                        foreach ($prefix_name as $key => $value) {
                                            $selected =  old('prefix_id') == $value['id'] ? 'selected="selected"':'';
                                            echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['prefix_name'].'</option>';
                                        }
                                    ?>
                                </select>
                                {!!$errors->first('prefix_id', '<span class="control-label" style="color:#FF9494" for="prefix_id">*:message</span>')!!}
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="firstName">ชื่อ : </label>
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="ชื่อ" value="{{old('firstName')}}" autocomplete="off">
                                {!!$errors->first('firstName', '<span class="control-label" style="color:#FF9494" for="firstName">*:message</span>')!!}
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="lastName">นามสกุล</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="นามสกุล" value="{{old('lastName')}}" autocomplete="off">
                                {!!$errors->first('lastName', '<span class="control-label" style="color:#FF9494" for="lastName">*:message</span>')!!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="phone">เบอร์โทรศัพท์ :</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="เบอร์โทรศัพท์" value="{{old('phone')}}" autocomplete="off">
                                {!!$errors->first('phone', '<span class="control-label" style="color:#FF9494" for="phone">*:message</span>')!!}
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="registration">ทะเบียน :</label>
                                <input type="text" class="form-control" id="registration" name="registration" placeholder="ทะเบียน" value="{{old('registration')}}" autocomplete="off">
                                {!!$errors->first('registration', '<span class="control-label" style="color:#FF9494" for="registration">*:message</span>')!!}
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="province">จังหวัด :</label>
                                <select class="form-control" name="province" id="province">
                                    <option value="">-- เลือก --</option>
                                    @foreach ($province as $key => $value)
                                        <?php $selected =  old('province') == $value['id'] ? 'selected="selected"':''; ?>
                                        <option value="{{ $value['id'] }}" {{ $selected }} >{{ $value['province'] }}</option>
                                    @endforeach
                                </select>
                                {!!$errors->first('province', '<span class="control-label" style="color:#FF9494" for="province">*:message</span>')!!}
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="ry">รย. :</label>
                                <input type="number" class="form-control" id="ry" name="ry" placeholder="รย." value="{{old('ry')}}" >
                                {!!$errors->first('ry', '<span class="control-label" style="color:#FF9494" for="ry">*:message</span>')!!}
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="register_expired_date">ภาษีหมดอายุ :</label>
                                <input type="text" class="form-control" id="register_expired_date" name="register_expired_date" placeholder="วันที่ภาษีหมดอายุ" value="{{old('register_expired_date')}}" autocomplete="off">
                                {!!$errors->first('register_expired_date', '<span class="control-label" style="color:#FF9494" for="register_expired_date">*:message</span>')!!}
                            </div>
                        </div>

                    </div>



                    
                    <hr/>

                	<div class="row">
                		<div class="col-xs-2">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="flat-red" name="act[chk]" id="act" data-name="act_price" checked >  
                                    พ.ร.บ.
                                </label> 
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="act[text]" id="act_text" placeholder="ข้อมูล พ.ร.บ." data-title="พ.ร.บ." value="{{old('act')['text']}}">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="act[price]" id="act_price" placeholder="ราคา พ.ร.บ." data-title="พ.ร.บ." value="{{old('act')['price']}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="flat-red" name="vat[chk]" id="vat" data-name="vat_price" checked > 
                                    ภาษี 
                                </label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="vat[price]" id="vat_price" placeholder="ราคา ภาษี" data-title="ภาษี" value="{{old('vat')['price']}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="tra[chk]" id="tra" data-name="tra_price" checked> 
                					ตรอ.
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="tra[price]" id="tra_price" placeholder="ราคา ตรอ." data-title="ตรอ." value="{{old('tra')['price']}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="insurance[chk]" id="insurance" data-name="insurance" >
                					ประกันภัย
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="insurance[text]" id="insurance_text" placeholder="ข้อมูลประกัน" data-title="ประกันภัย" value="{{old('insurance')['text']}}" <?php echo isset(old('insurance')['chk'])&&old('insurance')['chk'] == 'on' ? '' : 'disabled'; ?> >
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="insurance[price]" id="insurance_price" disabled placeholder="ราคา ประกัน..." data-title="ประกันภัย" value="{{old('insurance')['price']}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="service[chk]" id="service" data-name="service_price" checked> 
                					บริการ
								</label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="service[price]" id="service_price" placeholder="ราคา ค่าบริการ..." data-title="บริการ" value="{{old('service')['price']}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="discount[chk]" id="discount" data-name="discount_price" <?php echo isset(old('discount')['chk'])&&old('discount')['chk'] == 'on' ? 'checked' : ''; ?> >
                					ส่วนลด
								</label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="discount[price]" id="discount_price" placeholder="ส่วนลด..." data-title="ส่วนลด" value="{{old('discount')['price']}}" <?php echo isset(old('discount')['chk'])&&old('discount')['chk'] == 'on' ? '' : 'disabled'; ?>>
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="etc[chk]" id="etc" data-name="etc_price" <?php echo isset(old('etc')['chk'])&&old('etc')['chk'] == 'on' ? 'checked' : ''; ?> >
                					อื่นๆ
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="etc[text]" id="etc_text" placeholder="ข้อความ..." data-title="อื่นๆ" value="{{old('etc')['text']}}" <?php echo isset(old('etc')['chk'])&&old('etc')['chk'] == 'on' ? '' : 'disabled'; ?> >
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="etc[price]" id="etc_price" placeholder="ค่าอื่นๆ..." data-title="อื่นๆ" value="{{old('etc')['price']}}" <?php echo isset(old('etc')['chk'])&&old('etc')['chk'] == 'on' ? '' : 'disabled'; ?> >
                            </div>
                        </div>
                	</div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <button class="btn btn-primary" onclick="window.history.back();">
                                    Back
                                </button>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" id="preview-bill">
                                    Preview
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
	</div>
    <input type="hidden" name="cus_id" value="{{ Request::segment(3) }}">
    <input type="hidden" name="regis_id" value="{{ Request::segment(4) }}">
    <input type="hidden" name="total" id="total" value="">
    <input type="hidden" name="bill_type" id="bill_type" value="{{ Request::input('bill_type')==""?1:Request::input('bill_type') }}">
    </form>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;color: #FFF">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">ตัวอย่างบิล</h4>
            </div>
            <div class="modal-body">
                <div style="border: 1px solid #AAA; padding: 20px">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="pull-right"> <label>เลขที่. <span id="bill-no">00001</span> </label> </p>
                        </div>
                    </div>    
                
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> {{ $bill_name }} </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> {{ $bill_address }} </h4>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="text-center"> {{ $bill_header }} </h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="text-center"> <label>วันที่ :</label> {{ convert_TH_FormatDate(date('Y-m-d')) }} </p>
                        </div>
                    </div>
                    <hr style="border-color: #AAA">
                    
                    <div class="row">
                        <div class="col-xs-6">
                            <p><label>ชื่อ :</label> <span id="modal-fullname"></span> </p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ทะเบียน :</label> <span id="modal-register-number"></span> </p>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-xs-6">
                            <p><label>เบอร์โทรศัพท์ :</label> <span id="modal-phone"></span> </p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ภาษีหมดอายุ :</label> <span id="modal-register-expired-date"></span> </p>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10%">ลำดับที่</th>
                                        <th class="text-center">รายการ</th>
                                        <th class="text-center" width="10%">จำนวน</th>
                                        <th class="text-center" width="20%">จำนวนเงิน/บาท</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-modal">
                                    <!-- <tr>
                                        <td>1</td>
                                        <td>พ.ร.บ.</td>
                                        <td class="text-right">1</td>
                                        <td class="text-right">645</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                                <table width="100%">
                                    <tr>
                                        <td class="text-right" style="color:blue"> <label><u>รวม :  <span id="total-modal">0</span> บาท </u> </label></td>
                                    </tr>
                                </table>
                                
                            </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-offset-6 col-xs-6">
                        <br/>
                                <p class="pull-right">ผู้รับเงิน : ................................................................................</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #EEE !important;color: #FFF">
            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="print-bill">Print & Save</button>
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
    <!-- InputMask -->
    <script src="{{ URL::asset('/assets/bootstrap/js/bootstrap-datepicker-thai.js') }}"></script>
    <script src="{{ URL::asset('/assets/bootstrap/js/locales/bootstrap-datepicker.th.js') }}"></script>

	<script src="{{ URL::asset('/assets/js/jquery.number.min.js') }}"></script>
    
    <script>
        function searchRegis() {
            var search_regis = $('#search_regis').val()
            if(search_regis==""){
                return false;
            }
            var url = '<?php echo url('renew-act/insurance-info/registration')?>/'+search_regis;
            $.ajax({
                type:"GET",
                url: url,
                success: function(data){
                    if(data != 'null')
                    {
                        var dataArray = JSON.parse(data);
                        console.log(dataArray);
                        dataArray = dataArray.data;

                        $('#prefix_id').val(dataArray.prefix.id)
                        $('#firstName').val(dataArray.first_name)
                        $('#lastName').val(dataArray.last_name)
                        $('#phone').val(dataArray.phone)
                        $('#registration').val(dataArray.registration.register_number)
                        $('#province').val(dataArray.registration.province.id)
                        
                    } else {
                        alert("ไม่พบข้อมูล")
                    }
                }
            }); 
        }



        $(function () {
            var total = 0;
            var listBill = '';
            var listBillEmpty = '';
            var status = true;

            var availableTags = [
                <?php echo $first_names; ?>
            ];

            $( "#firstName" ).autocomplete({
                source: availableTags,
            })

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear()+544;
            if(mm<10){
                mm='0'+mm
            }
            if(dd<10){
                dd='0'+dd
            } 
            today = dd+'/'+mm+'/'+yyyy;
            $('#register_expired_date').val(today);

            $('#register_expired_date').datepicker({
                language : 'th-th',
                format:'dd/mm/yyyy',
                autoclose: true,
            });

            $('#register_expired_date').val('');

            var old_chk_insurance = "<?php echo old('insurance')['chk']; ?>";

            $('#preview-bill').click(function() {
                
                total = 0;
                listBill = '';
                listBillEmpty = '';
                status = true;

                total += $('#act_price').val()==null || !$('#act').is(':checked') ? 0 : $('#act_price').val()*1;
                total += $('#vat_price').val()==null || !$('#vat').is(':checked') ? 0 : $('#vat_price').val()*1;
                total += $('#tra_price').val()==null || !$('#tra').is(':checked') ? 0 : $('#tra_price').val()*1;
                total += $('#insurance_price').val()==null ? 0 : $('#insurance_price').val()*1;
                total += $('#service_price').val()==null || !$('#service').is(':checked') ? 0 : $('#service_price').val()*1;
                total -= $('#discount_price').val()==null ? 0 : $('#discount_price').val()*1;
                total += $('#etc_price').val()==null ? 0 : $('#etc_price').val()*1;

                if (total <= 0) {
                    alert('รายการบิลต้องมีจำนวนเงินมากกว่า 0 บาท');
                    return false;
                }

// ------------- Set Data to title bill -----------------
                $('#modal-fullname').text($('#prefix_id option:selected').text() + ' ' + $('#firstName').val() + ' ' + $('#lastName').val());
                $('#modal-register-number').text($('#registration').val() + ' ' + $('#province option:selected').text());
                
                var phone = '-';
                if ($('#phone').val()) {
                    phone = $('#phone').val();
                }

                var i = 1;
                $('input[type=checkbox]').each(function () {
                    
                    if($(this).is(':checked'))
                    {
                        var name = $(this).attr('data-name');
                        var title_name = $('#'+name+'').attr('data-title');
                        var price = $('#'+name).val()==null ? 0 : $('#'+name).val()*1;
                        if(name == 'insurance')
                        {
                            title_name = $('#'+name+'_price').attr('data-title');
                            if( $('#insurance_text').val() != '' )
                                title_name = $('#insurance_text').val();

                            var price = $('#'+name+'_price').val()==null ? 0 : $('#'+name+'_price').val()*1;
                        }

                        if(name == 'discount_price')
                        {
                            var price = $('#'+name).val()==null ? 0 : $('#'+name).val()*-1;
                        }

                        if(name == 'etc_price')
                        {
                            if( $('#etc_text').val() != '' )
                                title_name = $('#etc_text').val();
                        }

                        if(name == 'act_price')
                        {
                            if( $('#act_text').val() != '' )
                                title_name = 'พ.ร.บ. ' + $('#act_text').val();
                        }

                        listBill +='<tr><td align="center">'+i+'</td><td>'+title_name+'</td><td class="text-center">1</td><td class="text-right">'+$.number( price, 2 );+'</td></tr>';
                        i++;
                    }
                    else
                    {
                        listBillEmpty += '<tr><td>&nbsp;</td><td>&nbsp;</td><td class="text-right">&nbsp;</td><td class="text-right">&nbsp;</td></tr>';
                    }
                    
                });

                
                $('#modal_register_expired_date').html($('#register_expired_date').val());
                $('#tbody-modal').html(listBill);
                $('#tbody-modal').append(listBillEmpty);
                $('#total-modal').text(total);
                $('#total-modal').number( true, 2 )

                $('#total').val(total);

                if (status == true)
                    $('#myModal').modal('show');
                else
                    return status;
            });

            $('#act').change(function() {
                if ($('#act').is(':checked'))
                {
                    $('#act_price').prop("disabled", false);
                    $('#act_text').prop("disabled", false);
                }
                else
                {
                    $('#act_price').prop("disabled", true);
                    $('#act_text').prop("disabled", true);
                }

            });

            $('#vat').change(function() {   
                if ($('#vat').is(':checked')) 
                {
                    $('#vat_price').prop("disabled", false);
                }
                else
                {
                    // $('#vat_price').val('');
                    $('#vat_price').prop("disabled", true);   
                }
                
            });

            $('#tra').change(function() {
                if ($('#tra').is(':checked')) 
                {
                    $('#tra_price').prop("disabled", false);
                }
                else
                {
                    // $('#tra_price').val('');
                    $('#tra_price').prop("disabled", true);   
                }
                
            });

            $('#insurance').click(function() {
                if ($('#insurance').is(':checked')) 
                {
                    // $('#insurance_company').prop("disabled", false);
                    $('#insurance_text').prop("disabled", false);
                    // $('#insurance_type').prop("disabled", false);
                    $('#insurance_price').prop("disabled", false);
                }
                else
                {
                    $('#insurance_text').val('');
                    $('#insurance_text').prop("disabled", true);
                    
                    $('#insurance_price').val('');
                    $('#insurance_price').prop("disabled", true);
                }
                
            });

            

            $('#service').change(function() {
                if ($('#service').is(':checked')) 
                {
                    $('#service_price').prop("disabled", false);
                }
                else
                {
                    // $('#service_price').val('');
                    $('#service_price').prop("disabled", true);   
                }
                
            });

            $('#discount').change(function() {
                if ($('#discount').is(':checked')) 
                {
                    $('#discount_price').prop("disabled", false);
                }
                else
                {
                    $('#discount_price').val('');
                    $('#discount_price').prop("disabled", true);   
                }
                
            });

            $('#etc').change(function() {
                if ($('#etc').is(':checked')) 
                {
                    $('#etc_price').prop("disabled", false);
                    $('#etc_text').prop("disabled", false);
                }
                else
                {
                    $('#etc_price').val('');
                    $('#etc_price').prop("disabled", true);

                    $('#etc_text').val('');
                    $('#etc_text').prop("disabled", true);   
                }
                
            });

            $('#print-bill').click(function() {
                $('#form-create-bill').submit();
                
            });


            if (old_chk_insurance == "on") {
                $('#insurance').trigger("click");
            }



            $('#firstName').blur(function() {
                var fullname = '';
                if ($('#firstName').val() != '') 
                {
                    $('#lastName').addClass('loading');
                    $('#phone').addClass('loading');
                    $('#registration').addClass('loading');
                    $('#province').addClass('loading');
                    $('#ry').addClass('loading');

                    fullname = $('#firstName').val();
                    var url = '<?php echo url('renew-act/insurance-info/full-name')?>/'+fullname;
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            if(data != 'null')
                            {
                                var dataArray = JSON.parse(data);

                                if (typeof dataArray['last_names'] != 'undefined' && dataArray['last_names'].length > 0)
                                {
                                    $("#lastName").autocomplete({
                                        source: dataArray['last_names'],
                                        minLength: 0,
                                    }).focus(function(){  
                                        $(this).autocomplete("search");
                                    });
                                }
                            }

                            $('#lastName').removeClass('loading');
                            $('#phone').removeClass('loading');
                            $('#registration').removeClass('loading');
                            $('#province').removeClass('loading');
                            $('#ry').removeClass('loading');
                        }
                    });
                }
            });
            var dataProvince = '';
            var dataRy = '';
            $('#lastName').blur(function() {
                var fullname = '';
                if ($('#firstName').val() != '' && $('#lastName').val() != '') 
                {
                    $('#phone').addClass('loading');
                    $('#registration').addClass('loading');
                    $('#province').addClass('loading');
                    $('#ry').addClass('loading');

                    fullname = $('#firstName').val() + '_' + $('#lastName').val();
                    var url = '<?php echo url('renew-act/insurance-info/full-name')?>/'+fullname;
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            if(data != 'null')
                            {
                                var dataArray = JSON.parse(data);

                                if (typeof dataArray['arr_regis'] != 'undefined' && dataArray['arr_regis'].length > 0)
                                {
                                    $("#registration").autocomplete({
                                        source: dataArray['arr_regis'],
                                        minLength: 0,
                                    }).focus(function(){  
                                        $(this).autocomplete("search");
                                    });
                                    dataProvince = dataArray['arr_province'];
                                    dataRy = dataArray['arr_ry'];
                                }

                                $('#phone').val(dataArray['data'][0]['phone']);
                                if(typeof dataArray['data'][0]['customer2']['registration2'] != 'undefined' && dataArray['data'][0]['customer2']['registration2'].length == 1)
                                {
                                    $('#registration').val(dataArray['data'][0]['customer2']['registration2'][0]['register_number']);
                                    $('#province').val(dataArray['data'][0]['customer2']['registration2'][0]['province']['id']);
                                    $('#ry').val(dataArray['data'][0]['customer2']['registration2'][0]['car_detail2']['ry']);
                                    $('#ry').trigger("blur");
                                }
                            }

                            $('#phone').removeClass('loading');
                            $('#registration').removeClass('loading');
                            $('#province').removeClass('loading');
                            $('#ry').removeClass('loading');
                        }
                    });
                }
            });

            $('#registration').blur(function() {
                if (typeof dataProvince[$('#registration').val()] != 'undefined')
                {
                    $('#province').val(dataProvince[$('#registration').val()]);
                }

                if (typeof dataRy[$('#registration').val()] != 'undefined')
                {
                    $('#ry').val(dataRy[$('#registration').val()]);
                    $('#ry').trigger("blur");
                }


            });

            $('#ry').blur(function() {
                var ry = $('#ry').val();
                var total = 0;
                if(ry != ''){
                    if ( ry == 1 ) {
                        total = 645;
                        
                    }
                    else if (ry == 2) {
                        total = 1182;
                        
                    }
                    else if (ry == 3) {
                        total = 967;
                        
                    }
                    else if (ry == 12) {
                        total = 324;
                    }

                    $('#act_price').val(total);
                }
            });

            
        });
    </script>
@stop

