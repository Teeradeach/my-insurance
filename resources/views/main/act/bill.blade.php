@extends('main.layouts.template')
@section('stylesheet')
	<link rel="stylesheet" href="{{ URL::asset('/assets/plugins/iCheck/all.css') }}">
    <style>
    /*    table, td, th {
            border: 1px solid #AAA;
            border-bottom-color: #AAA;
        }*/
        .table {
            border: 0.5px solid #AAA;
        }
        .table-bordered > thead > tr > th,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > tbody > tr > td,
        .table-bordered > tfoot > tr > td {
            border: 0.5px solid #AAA;
        }
        input[type=checkbox] {
            transform: scale(1.2);
        }
    </style>
@stop
@section('content')
<section class="content-header">
    <h1>
        ต่อทะเบียน
        <small>ทะเบียน [ {{ $registration['register_number'] }} ]</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('renew-act')}}">ต่อ พ.ร.บ. ภาษี</a></li>
        <li><a href="{{url('renew-act/detail')}}">ข้อมูลเจ้าของรถ</a></li>
        <li class="active">ต่อทะเบียน</li>
    </ol>
</section>

<section class="content">
    <form method="post" id="form-create-bill" action="{{url('renew-act/process-bill')}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
        		<div class="box-header with-border">
                    <h3 class="box-title">รายการต่อทะเบียน</h3>
                </div>

                <div class="box-body">

                    <div class="row">
                        <div class="col-xs-6">
                            <p><label>ชื่อ :</label> {{ $customer['person_info']['prefix']['prefix_name'] }} {{ $customer['person_info']['first_name'] }} {{ $customer['person_info']['last_name'] }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ทะเบียน :</label> {{ $registration['register_number'] }} {{ $registration['province']['province'] }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <p> <label>ที่อยู่ :</label> {{ $customer['person_info']['address'] }} </p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>เบอร์โทรศัพท์ :</label> {{ $customer['person_info']['phone'] }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <p>
                                <label>ประเภท :</label>
                                {{ array_get($registration, 'car_detail.category_car.cat_name', '-') }}
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p>
                                <label>รย. :</label>
                                {{ array_get($registration, 'car_detail.ry', '-') }}
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <p>
                                <label>ซีซี :</label>
                                {{ array_get($registration, 'car_detail.cc', '-') }}
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>วันจดทะเบียน :</label> <span id="register_date">{{ convert_TH_FormatDate($registration['register_date']) }}</span></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>ภาษีหมดอายุ :</label> 
                                    <!-- <span id="register_expired_date">{{ $register_expired_date }}</span> -->
                                    <input type="text" name="register_expired_date" id="register_expired_date" class="form-control" value="{{ $register_expired_date }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>

                	<div class="row">
                		<div class="col-xs-2">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="flat-red" name="act[chk]" id="act" data-name="act_price" checked >  
                                    พ.ร.บ.
                                </label> 
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="act[price]" id="act_price" placeholder="ราคา พ.ร.บ." data-title="พ.ร.บ." value="{{ $act_price }}"> 
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="flat-red" name="vat[chk]" id="vat" data-name="vat_price" checked > 
                                    ภาษี 
                                </label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="vat[price]" id="vat_price" placeholder="ราคา ภาษี" data-title="ภาษี" value="{{ $vat_price }}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="tra[chk]" id="tra" data-name="tra_price" checked> 
                					ตรอ.
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="tra[price]" id="tra_price" placeholder="ราคา ตรอ." data-title="ตรอ." value="{{ @$tra_price }}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="insurance[chk]" id="insurance" data-name="insurance"> 
                					ประกันภัย
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <select class="form-control" disabled name="insurance[company]" id="insurance_company">
                                    <option value="">-- บริษัทประกัน --</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <select class="form-control" disabled name="insurance[type]" id="insurance_type">
                                    <option value="">-- ประเภทประกัน --</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="insurance[price]" id="insurance_price" disabled placeholder="ราคา ประกัน..." data-title="ประกันภัย">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="service[chk]" id="service" data-name="service_price" checked> 
                					บริการ
								</label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="service[price]" id="service_price" placeholder="ราคา ค่าบริการ..." data-title="บริการ" value="{{ @$service_price }}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="discount[chk]" id="discount" data-name="discount_price"> 
                					ส่วนลด
								</label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="discount[price]" id="discount_price" disabled placeholder="ส่วนลด..." data-title="ส่วนลด">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="etc[chk]" id="etc" data-name="etc_price"> 
                					อื่นๆ
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="etc[price]" id="etc_price" disabled placeholder="ค่าอื่นๆ..." data-title="อื่นๆ">
                            </div>
                        </div>
                	</div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <button class="btn btn-primary" onclick="window.history.back();">
                                    Back
                                </button>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" id="preview-bill">
                                    Preview
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
        	</div>
        </div>
	</div>
    <input type="hidden" name="cus_id" value="{{ Request::segment(3) }}">
    <input type="hidden" name="regis_id" value="{{ Request::segment(4) }}">
    <!-- <input type="hidden" name="register_expired_date" value="{{ $register_expired_date }}"> -->
    <input type="hidden" name="total" id="total" value="">
    </form>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;color: #FFF">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">ตัวอย่างบิล</h4>
            </div>
            <div class="modal-body">
                <div style="border: 1px solid #AAA; padding: 20px">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="pull-right"> <label>เลขที่. <span id="bill-no">{{ $bill_no }}</span> </label> </p>
                        </div>
                    </div>    
                
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> สถานตรวจสภาพรถ พิชิตกวิน บ้านนา </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> 204/1 หมู่ที่ 5 ตำบลป่าขะ อำเภอบ้านนา จังหวัดนครนายค 26110 โทร. 037-382244 </h4>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="text-center"> บิลเงินสด, ใบแจ้งหนี้  </h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p class="text-center"> <label>วันที่ :</label> {{ convert_TH_FormatDate(date('Y-m-d')) }} </p>
                        </div>
                    </div>
                    <hr style="border-color: #AAA">
                    
                    <div class="row">
                        <div class="col-xs-6">
                            <p><label>ชื่อ :</label> {{ $customer['person_info']['prefix']['prefix_name'] }} {{ $customer['person_info']['first_name'] }} {{ $customer['person_info']['last_name'] }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ทะเบียน :</label> {{ $registration['register_number'] }} {{ $registration['province']['province'] }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <p> <label>ที่อยู่ :</label> {{ $customer['person_info']['address'] }} </p>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-xs-6">
                            <p><label>เบอร์โทรศัพท์ :</label> {{ $customer['person_info']['phone'] }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ภาษีหมดอายุ :</label> <span id="modal_register_expired_date">{{ $register_expired_date }}</span></p>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10%">ลำดับที่</th>
                                        <th class="text-center">รายการ</th>
                                        <th class="text-center" width="10%">จำนวน</th>
                                        <th class="text-center" width="20%">จำนวนเงิน/บาท</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-modal">
                                    <!-- <tr>
                                        <td>1</td>
                                        <td>พ.ร.บ.</td>
                                        <td class="text-right">1</td>
                                        <td class="text-right">645</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                                <table width="100%">
                                    <tr>
                                        <td class="text-right" style="color:blue"> <label><u>รวม :  <span id="total-modal">0</span> บาท </u> </label></td>
                                    </tr>
                                </table>
                                
                            </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-offset-6 col-xs-6">
                        <br/>
                                <p class="pull-right">ผู้รับเงิน : ................................................................................</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #EEE !important;color: #FFF">
            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="print-bill">Print & Save</button>
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
    <!-- InputMask -->
    <script src="{{ URL::asset('/assets/bootstrap/js/bootstrap-datepicker-thai.js') }}"></script>
    <script src="{{ URL::asset('/assets/bootstrap/js/locales/bootstrap-datepicker.th.js') }}"></script>

	<script src="{{ URL::asset('/assets/js/jquery.number.min.js') }}"></script>
    
    <script>
        $(function () {
            
            var total = 0;
            var listBill = '';
            var listBillEmpty = '';
            var status = true;

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear()+544;
            if(mm<10){
                mm='0'+mm
            }
            if(dd<10){
                dd='0'+dd
            } 
            today = dd+'/'+mm+'/'+yyyy;
            $('#register_expired_date').val(today);

            $('#register_expired_date').datepicker({
                language : 'th-th',
                format:'dd/mm/yyyy',
                autoclose: true
            });

            $('#register_expired_date').val('');

            $('#preview-bill').click(function() {
                
                total = 0;
                listBill = '';
                listBillEmpty = '';
                status = true;

                total += $('#act_price').val()==null || !$('#act').is(':checked') ? 0 : $('#act_price').val()*1;
                total += $('#vat_price').val()==null || !$('#vat').is(':checked') ? 0 : $('#vat_price').val()*1;
                total += $('#tra_price').val()==null || !$('#tra').is(':checked') ? 0 : $('#tra_price').val()*1;
                total += $('#insurance_price').val()==null ? 0 : $('#insurance_price').val()*1;
                total += $('#service_price').val()==null || !$('#service').is(':checked') ? 0 : $('#service_price').val()*1;
                total -= $('#discount_price').val()==null ? 0 : $('#discount_price').val()*1;
                total += $('#etc_price').val()==null ? 0 : $('#etc_price').val()*1;

                if (total <= 0) {
                    alert('รายการบิลต้องมีจำนวนเงินมากกว่า 0 บาท');
                    return false
                }

                
                var i = 1;
                $('input[type=checkbox]').each(function () {
                    
                    if($(this).is(':checked'))
                    {
                        var name = $(this).attr('data-name');
                        var title_name = $('#'+name+'').attr('data-title');
                        var price = $('#'+name).val()==null ? 0 : $('#'+name).val()*1;
                        if(name == 'insurance')
                        {
                            if($('#insurance_company option:selected').val() == '' || $('#insurance_type option:selected').val() == '')
                            {
                                alert('กรุณาเลือก บริษัทประกัน และ ประเภทประกัน!');
                                status = false;
                            }

                            title_name = $('#'+name+'_price').attr('data-title');
                            title_name += ' บริษัท <b>' + $('#insurance_company option:selected').text() + '</b>';
                            title_name += ' ประเภท <b>' + $('#insurance_type option:selected').text() + '</b>';
                            var price = $('#'+name+'_price').val()==null ? 0 : $('#'+name+'_price').val()*1;
                        }

                        if(name == 'discount_price')
                        {
                            var price = $('#'+name).val()==null ? 0 : $('#'+name).val()*-1;
                        }

                        listBill +='<tr><td align="center">'+i+'</td><td>'+title_name+'</td><td class="text-center">1</td><td class="text-right">'+$.number( price, 2 );+'</td></tr>';
                        i++;
                    }
                    else
                    {
                        listBillEmpty += '<tr><td>&nbsp;</td><td>&nbsp;</td><td class="text-right">&nbsp;</td><td class="text-right">&nbsp;</td></tr>';
                    }
                    
                });

                
                $('#modal_register_expired_date').html($('#register_expired_date').val());
                $('#tbody-modal').html(listBill);
                $('#tbody-modal').append(listBillEmpty);
                $('#total-modal').text(total);
                $('#total-modal').number( true, 2 )

                $('#total').val(total);

                if (status == true)
                    $('#myModal').modal('show');
                else
                    return status;
            });

            $('#act').change(function() {
                if ($('#act').is(':checked')) 
                {
                    $('#act_price').prop("disabled", false);
                }
                else
                {
                    // $('#act_price').val('');
                    $('#act_price').prop("disabled", true);   
                }
                
            });

            $('#vat').change(function() {   
                if ($('#vat').is(':checked')) 
                {
                    $('#vat_price').prop("disabled", false);
                }
                else
                {
                    // $('#vat_price').val('');
                    $('#vat_price').prop("disabled", true);   
                }
                
            });

            $('#tra').change(function() {
                if ($('#tra').is(':checked')) 
                {
                    $('#tra_price').prop("disabled", false);
                }
                else
                {
                    // $('#tra_price').val('');
                    $('#tra_price').prop("disabled", true);   
                }
                
            });

            $('#insurance').change(function() {
                if ($('#insurance').is(':checked')) 
                {
                    $('#insurance_company').prop("disabled", false);
                    $('#insurance_type').prop("disabled", false);
                    $('#insurance_price').prop("disabled", false);

                    var url = '<?php echo url('renew-act/insurance-info/company')?>';
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            var dataArray = JSON.parse(data);
                            var html_insurance_company = '<option value="">-- บริษัทประกัน --</option>';
                            $.each(dataArray, function( index, value ) {
                                // alert(value['company_name']);
                                html_insurance_company += '<option value="'+ value['id'] +'">'+ value['company_name'] +'</option>';
                            });

                            $('#insurance_company').html(html_insurance_company);
                        }
                    });
                }
                else
                {
                    $('#insurance_company').html('<option value="">-- บริษัทประกัน --</option>');
                    $('#insurance_company').prop("disabled", true);

                    $('#insurance_type').html('<option value="">-- ประเภทประกัน --</option>');
                    $('#insurance_type').prop("disabled", true);
                    
                    $('#insurance_price').val('');
                    $('#insurance_price').prop("disabled", true);
                }
                
            });

            $('#insurance_company').change(function() {
                var company_id = $('#insurance_company').val();
                var url = '<?php echo url('renew-act/insurance-info/type')?>/'+company_id;
                if(company_id != '')
                {
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            var dataArray = JSON.parse(data);
                            var html_insurance_company = '<option value="">-- ประเภทประกัน --</option>';
                            $.each(dataArray, function( index, value ) {
                                html_insurance_company += '<option value="'+ value['id'] +'">'+ value['type_name'] +'</option>';
                            });

                            $('#insurance_type').html(html_insurance_company);
                        }
                    });
                }
                else
                {
                    $('#insurance_type').html('<option value="">-- ประเภทประกัน --</option>');
                } 
            });

            $('#service').change(function() {
                if ($('#service').is(':checked')) 
                {
                    $('#service_price').prop("disabled", false);
                }
                else
                {
                    // $('#service_price').val('');
                    $('#service_price').prop("disabled", true);   
                }
                
            });

            $('#discount').change(function() {
                if ($('#discount').is(':checked')) 
                {
                    $('#discount_price').prop("disabled", false);
                }
                else
                {
                    $('#discount_price').val('');
                    $('#discount_price').prop("disabled", true);   
                }
                
            });

            $('#etc').change(function() {
                if ($('#etc').is(':checked')) 
                {
                    $('#etc_price').prop("disabled", false);
                }
                else
                {
                    $('#etc_price').val('');
                    $('#etc_price').prop("disabled", true);   
                }
                
            });

            $('#print-bill').click(function() {
                $('#form-create-bill').submit();
                
            });                
			
        });
    </script>
@stop

