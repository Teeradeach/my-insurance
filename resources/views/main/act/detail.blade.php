@extends('main.layouts.template')

@section('stylesheet')
<!-- DataTables -->
	<link rel="stylesheet" href="{{ URL::asset('/assets/plugins/datatables/dataTables.bootstrap.css') }}">
@stop
@section('content')
<?php
	$person_info = $customer['person_info'];
	$occupant = $customer['occupant'];
	$file_info = $customer['file_info'];
?>
<section class="content-header">
    <h1>
        ข้อมูลเจ้าของรถ
        <small>{{ $person_info['prefix']['prefix_name'] }} {{ $person_info['first_name'] }} {{ $person_info['last_name'] }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('renew-act')}}">ต่อ พ.ร.บ. ภาษี</a></li>
        <li class="active">ข้อมูลเจ้าของรถ</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
	            	<li class="active"><a href="#ownership" data-toggle="tab">ผู้ถือกรรมสิทธิ์</a></li>
	            	<li><a href="#occupant" data-toggle="tab">ผู้ครอบครอง</a></li>
            	</ul>
            	<div class="tab-content">
            		<div class="tab-pane active" id="ownership">
            			<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="titleName" >คำนำหน้าชื่อ : </label> 
									{{ $person_info['prefix']['prefix_name'] }}
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="firstName">ชื่อ : </label> 
									{{ $person_info['first_name'] }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="lastName">นามสกุล : </label> 
									{{ $person_info['last_name'] }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="idCard">เลขที่บัตร : </label> 
									{{ $person_info['id_card'] }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="birthday">วันเกิด : </label> 
									{{ convert_TH_FormatDate($person_info['birthday']) }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="nationality">สัญชาติ : </label> 
									{{ $person_info['nationality']['nationality'] }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="address">ที่อยู่ : </label> 
									{{ $person_info['address'] }}
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="tel">โทร. : </label> 
									{{ $person_info['phone'] }}
								</div>
							</div>
						</div>
						<?php 
						if (count($file_info)>0):
						?>
							<div class="row">
								<div class="col-md-12">
								<label for="tel">ไฟล์ : </label>
								<?php
	            				foreach ($file_info as $key => $value) {
	            				?>
	            					
										<u><a href="{{ getImgFile($value['file_name']) }}" target="_blank"><i class="fa fa-fw fa-paperclip"></i>{{ $value['file_name'] }}</a></u>
	            				<?php
	            				}
	            				?>
								
								</div>
							</div>
						<?php 
						endif; 
						?>
            		</div>
            		<!-- /.tab-pane -->
	<!-- *******************************Tab2********************************* -->
            		<div class="tab-pane" id="occupant">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="titleName" >คำนำหน้าชื่อ : </label> 
									{{ $occupant['prefix']['prefix_name'] != null ? $occupant['prefix']['prefix_name'] : ' - ' }}
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="firstName">ชื่อ : </label> 
									{{ $occupant['first_name'] != null ? $occupant['first_name'] : ' - ' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="lastName">นามสกุล : </label> 
									{{ $occupant['last_name'] != null ? $occupant['last_name'] : ' - ' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="idCard">เลขที่บัตร : </label> 
									{{ $occupant['id_card'] != null ? $occupant['id_card'] : ' - ' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="birthday">วันเกิด : </label> 
									{{ $occupant['birthday'] != null ? convert_TH_FormatDate($occupant['birthday']) : ' - ' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="nationality">สัญชาติ : </label> 
									{{ $occupant['nationality']['nationality'] != null ? $occupant['nationality']['nationality'] : ' - ' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="address">ที่อยู่ : </label> 
									{{ $occupant['address'] != null ? $occupant['address'] : ' - ' }}
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="tel">โทร. : </label> 
									{{ $occupant['phone'] != null ? $occupant['phone'] : ' - ' }}
								</div>
							</div>
						</div>
            		</div>
            		<!-- /.tab-pane -->
            	</div>
            	<!-- /.tab-content -->
			</div>
			<!-- /.nav-tabs-custom -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->


	<div class="row">
		<div class="col-md-12">

			<div class="box box-primary">
				<div class="box-header with-border">
                    <h3 class="box-title" >รายการจดทะเบียน [ <b>{{ $registration['register_number'] != null ? $registration['register_number'] : '-' }} </b> ]</h3>
                </div>
                <!-- /.box-header -->
                
                 <div class="box-body">

                		<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="registration_date">วันจดทะเบียน : </label>
									{{ $registration['register_date'] != null ? convert_TH_FormatDate($registration['register_date']) : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="expired_registration_date">วันหมดอายุทะเบียน : </label>
									{{ $registration['register_expired_date'] != null ? convert_TH_FormatDate($registration['register_expired_date']) : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="registration_number">เลขทะเบียน : </label>
									{{ $registration['register_number'] != null ? $registration['register_number'] : '-' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="province">จังหวัด : </label>
			              			{{ $registration['province']['province'] != null ? $registration['province']['province'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="car_type">ประเภท : </label>
									{{ $registration['car_detail']['category_car']['cat_name'] != null ? $registration['car_detail']['category_car']['cat_name'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="ry">รย. : </label>
			              			{{ $registration['car_detail']['ry'] != null ? $registration['car_detail']['ry'] : '-' }}
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="manner_car">ลักษณะ : </label>
									{{ $registration['car_detail']['manner_car'] != null ? $registration['car_detail']['manner_car'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="car_brand">ยี่ห้อรถ : </label>
									{{ $registration['car_detail']['car_brand'] != null ? $registration['car_detail']['car_brand'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="car_model">แบบ : </label>
									{{ $registration['car_detail']['model'] != null ? $registration['car_detail']['model'] : '-' }}
								</div>		
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="car_year">รุ่นปี : </label>
			              			{{ $registration['car_detail']['model_year'] != null ? $registration['car_detail']['model_year'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="car_color">สี : </label>
			              			{{ $registration['car_detail']['color'] != null ? $registration['car_detail']['color'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="chassis_number_number">เลขตัวรถ : </label>
			              			{{ $registration['car_detail']['chassis_number'] != null ? $registration['car_detail']['chassis_number'] : '-' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="chassis_number_number_area">เลขตัวรถ อยู่ที่ : </label>
			              			{{ $registration['car_detail']['chassis_number_position'] != null ? $registration['car_detail']['chassis_number_position'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="machine_brand">ยี่ห้อเครื่องยนต์ : </label>
									{{ $registration['car_detail']['machine_brand'] != null ? $registration['car_detail']['machine_brand'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="machine_number">เลขเครื่องยนต์ : </label>
									{{ $registration['car_detail']['machine_number'] != null ? $registration['car_detail']['machine_number'] : '-' }}
								</div>		
							</div>

						</div>
						
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="machine_number_area">เลขเครื่องยนต์ อยู่ที่ : </label>
			              			{{ $registration['car_detail']['machine_number_position'] != null ? $registration['car_detail']['machine_number_position'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="fuel">เชื้อเพลิง : </label>
			              			{{ $registration['car_detail']['fuel'] != null ? $registration['car_detail']['fuel'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="fuel_number">เลขถังแก๊ซ : </label>
			              			{{ $registration['car_detail']['fuel_number'] != null ? $registration['car_detail']['fuel_number'] : '-' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="piston_amount">จำนวนสูบ : </label>
									{{ $registration['car_detail']['piston_amount'] != 0 ? $registration['car_detail']['piston_amount'] : '-' }}
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="cc">ซีซี : </label>
			              			{{ $registration['car_detail']['cc'] != 0 ? $registration['car_detail']['cc'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="horsepower">แรงม้า : </label>
			              			{{ $registration['car_detail']['horsepower'] != 0 ? $registration['car_detail']['horsepower'] : '-' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="wheel_type">ลักษณะล้อ : </label>
			              			{{ $registration['car_detail']['wheel_type'] != null ? $registration['car_detail']['wheel_type'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="weight_car">น้ำหนักรถ : </label>
									{{ $registration['car_detail']['weight_car'] != 0 ? $registration['car_detail']['weight_car'] : '-' }}
								</div>		
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="weight_load">น้ำหนักบรรทุก/น้ำหนักลงเพลา : </label>
			              			{{ $registration['car_detail']['weight_load'] != 0 ? $registration['car_detail']['weight_load'] : '-' }}
								</div>		
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="weight_car_total">น้ำหนักรถรวม : </label>
			              			{{ $registration['car_detail']['weight_car_total'] != 0 ? $registration['car_detail']['weight_car_total'] : '-' }}
								</div>		
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="seat_amount">ที่นั่ง : </label>
			              			{{ $registration['car_detail']['seat_amount'] != 0 ? $registration['car_detail']['seat_amount'] : '-' }}
								</div>		
							</div>
						</div>
					</div>

					<div class="box-footer">
						<div class="row">
							<div class="col-md-10">
								<a href="{{ url('renew-act') }}"  class="btn btn-primary">
									<i class="fa fa-arrow-left"></i> 
									กลับ
								</a>
								
							</div>
							<div class="col-md-2">
								<a href="{{ url('renew-act/bill/'.Request::segment(3).'/'.Request::segment(4)) }}"  class="btn btn-success pull-right">
									ออกบิล
									<i class="fa fa-arrow-right"></i> 
								</a>
							</div>
						</div>
					</div>
                </div> 
                <!-- /.box-body -->
			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>

@stop

@section('scripts')
	<script src="{{ URL::asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $(function () {
        	$("#tb-car-detail").DataTable();
			
        });
    </script>
@stop