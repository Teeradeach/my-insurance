@extends('main.layouts.template-print')
@section('stylesheet')

    <style type="text/css">

        @media print {
            @page{
                margin-left: 0px;
                margin-right: 0px;
                margin-top: 0px;
                margin-bottom: 0px;
            }
            #whiteBg {
                display: block;
                position: absolute;
                top: 0;
                left: 0;

            }

        }
        table tr td {padding-top: 3px; padding-bottom:3px;padding-left: 5px; padding-right: 5px}
        table tr th {padding-top: 3px; padding-bottom:3px}
    </style>
@stop

@section('content')
<?php
$bill_header = 'บิลเงินสด';
if ($bill_type == 2) {
    $bill_header = "ใบแจ้งหนี้";
}
?>
    <section class="content">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">พิชิตกวิน ตรอ.</a>
                </div>

                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i> หน้าหลัก</a>
                    </li>
                    <li>
                        <a href="{{ url('renew-act/bill-create') }}"><i class="glyphicon glyphicon-cog"></i> ออกบิลใหม่</a>
                    </li>
                    <li>
                        <button type="button" class="btn btn-default navbar-btn" onclick="window.print(); return false;">
                            <i class="glyphicon glyphicon-print"></i> สั่งพิมพ์
                        </button>
                    </li>
                </ul>

            </div>
        </nav>

        <div class="row" style="margin-top:50px">
            <div class="col-sm-2">
                &nbsp;
            </div>
            <div class="col-sm-8" id="whiteBg">
                <div style="padding:25px 30px 0px 30px;background-color: #FFF;width: 21cm;height: auto; margin-left: auto ;margin-right: auto ;">

                    <div class="row">
                        <div class="col-xs-12">
                            <span class="pull-right"> <label>เลขที่. <span id="bill-no">{{ $bill_no }}</span> </label> </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> {{ $bill_name }} </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h5 class="text-center"> {{ $bill_address }} </h5>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> {{ $bill_header }} </h4>
                        </div>
                    </div>

                    <div class="row" style="border-bottom:2px solid #dddddd; margin-bottom: 10px">
                        <div class="col-xs-12">
                            <p class="text-center"> <label>วันที่ :</label> {{ convert_TH_FormatDate(date('Y-m-d')) }} </p>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-6">
                            <p><label>ชื่อ :</label> {{ $customer['person_info']['prefix']['prefix_name'] }} {{ $customer['person_info']['first_name'] }} {{ $customer['person_info']['last_name'] }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ทะเบียน :</label> {{ $registration['register_number'] }} {{ $registration['province']['province'] }}</p>
                        </div>
                    </div>

                <!-- <div class="row">
		            <div class="col-xs-12">
		                <p> <label>ที่อยู่ :</label> {{ $customer['person_info']['address'] }} </p>
		            </div>
		        </div> -->

                    <!-- <div class="row">
                        <div class="col-xs-6">
                            <p><label>เบอร์โทรศัพท์ :</label> {{ !empty($customer['person_info']['phone']) ? $customer['person_info']['phone'] : '-' }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ภาษีหมดอายุ :</label> <span id="register_expired_date">{{ $register_expired_date }}</span></p>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="" border="1" width="100%" bordercolor="#ddd" style="font-size: 1.1em">
                                <thead>
                                <tr>
                                    <th class="text-center" width="10%">ลำดับที่</th>
                                    <th class="text-center">รายการ</th>
                                    <th class="text-center" width="10%">จำนวน</th>
                                    <th class="text-center" width="20%">จำนวนเงิน/บาท</th>
                                </tr>
                                </thead>
                                <tbody id="tbody-modal" >
                                <?php
                                foreach ($bill_info as $key => $value) {
                                ?>
                                <tr>
                                    <td align="center">{{ $value['title'] != null ? $key+1 : '&nbsp;' }}</td>
                                    <td>{{ $value['title'] }}</td>
                                    <td class="text-center">{{ $value['amount'] }}</td>
                                    <td class="text-right">{{ $value['price'] != null ? number_format($value['price'], 2) : '' }}</td>
                                </tr>
                                <?php
                                }
                                ?>
                                <!-- <tr>
		                            <td>1</td>
		                            <td>พ.ร.บ.</td>
		                            <td class="text-right">1</td>
		                            <td class="text-right">645</td>
		                        </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table width="100%" style="font-size: 1.1em">
                                <tr>
                                    <td class="text-right"> <label><u>รวม :  <span id="total-modal">{{ number_format($total, 2) }}</span> บาท </u> </label></td>
                                </tr>
                            </table>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-offset-6 col-xs-6">
                            <br/>
                            <p class="pull-right">ผู้รับเงิน : ................................................................................
                            </p>
                        </div>
                    </div>

                </div>


                <div style="padding:0px 30px 0px 30px;background-color: #FFF;width: 21cm;height: auto; margin-left: auto ;margin-right: auto ;">
                    <div class="row">
                        <i class="fa fa-fw fa-scissors"></i>
                        <span>
                            ....................................................................................................................................................................................................................
                        </span>
                    </div>
                </div>




                <div style="padding:10px 30px 15px 30px;background-color: #FFF;width: 21cm;height: auto; margin-left: auto ;margin-right: auto ;">

                    <div class="row">
                        <div class="col-xs-12">
                            <span class="pull-right"> <label>เลขที่. <span id="bill-no">{{ $bill_no }}</span> </label> </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> {{ $bill_header }} </h4>
                        </div>
                    </div>

                    <div class="row" style="border-bottom:2px solid #dddddd; margin-bottom: 10px">
                        <div class="col-xs-12">
                            <p class="text-center"> <label>วันที่ :</label> {{ convert_TH_FormatDate(date('Y-m-d')) }} </p>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-6">
                            <p><label>ชื่อ :</label> {{ $customer['person_info']['prefix']['prefix_name'] }} {{ $customer['person_info']['first_name'] }} {{ $customer['person_info']['last_name'] }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ทะเบียน :</label> {{ $registration['register_number'] }} {{ $registration['province']['province'] }}</p>
                        </div>
                    </div>

                <!-- <div class="row">
		            <div class="col-xs-12">
		                <p> <label>ที่อยู่ :</label> {{ $customer['person_info']['address'] }} </p>
		            </div>
		        </div> -->

                    <!-- <div class="row">
                        <div class="col-xs-6">
                            <p><label>เบอร์โทรศัพท์ :</label> {{ $customer['person_info']['phone'] }}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><label>ภาษีหมดอายุ :</label> <span id="register_expired_date">{{ $register_expired_date }}</span></p>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="" border="1" width="100%" bordercolor="#ddd" style="font-size: 1.1em">
                                <thead>
                                <tr>
                                    <th class="text-center" width="10%">ลำดับที่</th>
                                    <th class="text-center">รายการ</th>
                                    <th class="text-center" width="10%">จำนวน</th>
                                    <th class="text-center" width="20%">จำนวนเงิน/บาท</th>
                                </tr>
                                </thead>
                                <tbody id="tbody-modal" >
                                <?php
                                foreach ($bill_info as $key => $value) {
                                ?>
                                <tr>
                                    <td align="center">{{ $value['title'] != null ? $key+1 : '&nbsp;' }}</td>
                                    <td>{{ $value['title'] }}</td>
                                    <td class="text-center">{{ $value['amount'] }}</td>
                                    <td class="text-right">{{ $value['price'] != null ? number_format($value['price'], 2) : '' }}</td>
                                </tr>
                                <?php
                                }
                                ?>
                                <!-- <tr>
		                            <td>1</td>
		                            <td>พ.ร.บ.</td>
		                            <td class="text-right">1</td>
		                            <td class="text-right">645</td>
		                        </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table width="100%" style="font-size: 1.1em">
                                <tr>
                                    <td class="text-right"> <label><u>รวม :  <span id="total-modal">{{ number_format($total, 2) }}</span> บาท </u> </label></td>
                                </tr>
                            </table>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-offset-6 col-xs-6">
                            <br/>
                            <p class="pull-right">ผู้ออกบิล : ................................................................................
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
@stop