<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ URL::asset('/assets/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ @$user->name }}</p>
          <p>สาขา <label>{{ @$user->branch->branch_name }}</label></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-list"></i> เมนู หลัก</li>
        <?php
          if($user->type == 'admin'):
        ?>
        <li class="{{ (Request::segment(1) == '') ? 'active' : null }}">
          <a href="{{ url('/') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <?php
          endif;
        ?>

        <li class="{{ (Request::segment(1) == 'manage-member') ? 'active' : null }}">
          <a href="{{ url('manage-member') }}">
            <i class="fa fa-list"></i> <span>จัดการข้อมูลลูกค้า</span>
            <!-- <span class="pull-right-container">
              <small class="label pull-right bg-red">hot</small>
            </span> -->
          </a>
        </li>

        <li class="{{ (Request::segment(1) == 'renew-act' && Request::input('bill_type') == 1) ? 'active' : null }}">
          <a href="{{ url('renew-act/bill-create?bill_type=1') }}">
            <i class="fa fa-calendar"></i> <span>ออกบิลเงินสด</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">hot</small>
            </span>
          </a>
        </li>

        <li class="{{ (Request::segment(1) == 'renew-act' && Request::input('bill_type') == 2) ? 'active' : null }}">
          <a href="{{ url('renew-act/bill-create?bill_type=2') }}">
            <i class="fa fa-calendar"></i> <span>ออกใบแจ้งหนี้</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">hot</small>
            </span>
          </a>
        </li>

        <li class="header"><i class="fa fa-file-text"></i> รายงาน</li>

        <li class="{{ (Request::segment(2) == 'bill') ? 'active' : null }}">
          <a href="{{ url('report/bill') }}">
            <i class="fa fa-file-text"></i> <span>รายงานบิลเงินสด</span>
          </a>
        </li>

        <li class="{{ (Request::segment(2) == 'bill2') ? 'active' : null }}">
            <a href="{{ url('report/bill2') }}">
              <i class="fa fa-file-text"></i> <span>รายงานใบแจ้งหนี้</span>
            </a>
          </li>

        <li class="{{ (Request::segment(1) == 'remind') ? 'active' : null }}">
          <a href="{{ url('remind') }}">
            <i class="fa fa-file-text"></i> รายงานการแจ้งเตือน
            <span class="pull-right-container">
              <small class="label label-primary pull-right">{{ Request::session()->get('expired_regis_amount') }}</small>
            </span>
          </a>
        </li>
        <?php
          if($user->type == 'admin'):
        ?>
        <li class="header"><i class="fa fa-gears"></i> จัดการข้อมูล</li>

        <!-- <li>
          <a href="{{ url('admin/register') }}" target="_blank">
            <i class="fa fa-user-plus"></i> <span>สร้างผู้ใช้งาน</span>
          </a>
        </li> -->

        <li class="treeview {{ (Request::segment(2) == 'user') ? 'active' : null }}">
          <a href="#">
            <i class="fa fa-user"></i> 
            <span>จัดการผู้ใช้งาน</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ url('admin/register') }}" target="_blank">
                <i class="fa fa-circle-o"></i> <span>สร้างผู้ใช้งาน</span>
              </a>
            </li>

            <li class="{{ (Request::segment(2) == 'user') ? 'active' : null }}">
              <a href="{{ url('admin/user') }}">
                <i class="fa fa-circle-o"></i> <span>ข้อมูลผู้ใช้งาน</span>
              </a>
            </li>
            
          </ul>
        </li>

        <li class="{{ (Request::segment(2) == 'branch') ? 'active' : null }}">
          <a href="{{ url('admin/branch') }}">
            <i class="fa fa-arrows"></i> <span>ข้อมูลสาขา</span>
          </a>
        </li>

        <li class=" {{ (Request::segment(2) == 'insurance') ? 'active' : null }}">
          <a href="{{ url('admin/insurance') }}">
            <i class="fa fa-automobile"></i> <span>ข้อมูลประกัน</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>  
          <!-- <ul class="treeview-menu">
            <li class="{{ (Request::segment(3) == 'company') ? 'active' : null }}">
              <a href="{{ url('admin/insurance/company') }}"><i class="fa fa-circle-o"></i> บริษัทประกัน</a>
            </li>
            <li class=""><a href="#"><i class="fa fa-circle-o"></i> ประเภทประกัน</a></li>
          </ul> -->
          
        </li>

        <?php
          endif;
        ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>