<!DOCTYPE html>
<head>
	<title>Print Bill</title>
	@include('main.layouts.inc-stylesheet')
	@yield('stylesheet')
</head>
<html>
	<body onload="window.print()" style="background-color:#DDD">
	{{--<body style="background-color:#DDD">--}}
		@yield('content')
	</body>
	@include('main.layouts.inc-scripts')
	@yield('scripts')
</html>