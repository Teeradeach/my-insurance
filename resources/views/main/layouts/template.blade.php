<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>พิชิตกวิน ตรอ.</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap Core CSS -->
  @include('main.layouts.inc-stylesheet')
  @yield('stylesheet')

</head>
<body class="hold-transition skin-purple fixed sidebar-mini">
<div class="wrapper">
  <!-- /.navbar-header -->
  @include('main.layouts.inc-header')
  <!-- /.navbar-top-links -->
  @include('main.layouts.inc-left-sidebar')
  <!-- /.navbar-static-side -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.5
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

@include('main.layouts.inc-scripts')
@yield('scripts')
</body>
</html>
