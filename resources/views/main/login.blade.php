@extends('main.layouts.template-login')
@section('content')
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    @if(Session::has('message'))
		<p class="panel-body bg-danger color-red">
		{{Session::get('message')}}
		</p>
	  @endif

    <form action="{{ url('/login/process') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}" >
        <span class="fa fa-user form-control-feedback"></span>
        {!!$errors->first('username', '<span class="control-label" style="color:#FF9494" for="username">*:message</span>')!!}
      </div>
      
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        {!!$errors->first('password', '<span class="control-label" style="color:#FF9494" for="password">*:message</span>')!!}
      </div>
      
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input name="remember" type="checkbox" value="Remember Me"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- /.social-auth-links -->

    <a href="#">I forgot my password</a><br>
    <!-- <a href="$" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
@stop