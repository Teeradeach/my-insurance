@extends('main.layouts.template')
@section('stylesheet')
<!-- DataTables -->
	<link rel="stylesheet" href="{{ URL::asset('/assets/plugins/datatables/dataTables.bootstrap.css') }}">
@stop
@section('content')
<section class="content-header">
    <h1>
        แก้ไขข้อมูลเจ้าของรถ
        <small>ข้อมูลลูกค้า</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('manage-member')}}"> ข้อมูลลูกค้า </a></li>
        <li class="active">แก้ไขข้อมูลลูกค้า</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
	@if(Session::has('message_member'))
	<div class="alert alert-success alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> {{Session::get('message_member')}}
	</div>
	@endif

	@if(Session::has('message_fali'))
	<div class="alert alert-danger alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Fail!</strong> {{Session::get('message_fali')}}
	</div>
	@endif
	<!-- form start -->
	<form role="form" method="post" action="{{ url('manage-member/edit/process') }}" id="form-edit" enctype="multipart/form-data" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="cus_id" value="{{ Request::segment(3) }}">
		
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
		            	<li class="active"><a href="#ownership" data-toggle="tab">ผู้ถือกรรมสิทธิ์</a></li>
		            	<li><a href="#occupant" data-toggle="tab">ผู้ครอบครอง</a></li>
	            	</ul>
	            	<div class="tab-content">
	            		<div class="tab-pane active" id="ownership">
	            			<input type="hidden" name="person_info_id" value="{{ $person_info['id'] }}">
	            			<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="prefix_id" >คำนำหน้าชื่อ : </label> 
										<select class="form-control" name="prefix_id" id="prefix_id">
											<option value="">เลือก</option>
							                <?php
							                	foreach ($prefix_name as $key => $value) {
							                		$selected =  old('prefix_id') == $value['id'] ? 'selected="selected"':'';
							                		if($selected == ''){
							                			$selected =  $person_info['prefix_id'] == $value['id'] ? 'selected="selected"':'';
							                		}
							                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['prefix_name'].'</option>';
							                	}
							                ?>
						                </select>
						                {!!$errors->first('prefix_id', '<span class="control-label" style="color:#FF9494" for="prefix_id">*:message</span>')!!}
										
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="first_name">ชื่อ : </label> 
										<input type="text" class="form-control" id="first_name" name="first_name" placeholder="ชื่อ" value="{{ old('first_name') == '' ? $person_info['first_name'] : old('first_name')}}">
										{!!$errors->first('first_name', '<span class="control-label" style="color:#FF9494" for="first_name">*:message</span>')!!}
									</div>		
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="last_name">นามสกุล : </label>
										<input type="text" class="form-control" id="last_name" name="last_name" placeholder="นามสกุล" value="{{ old('last_name') == '' ? $person_info['last_name'] : old('last_name')}}">
										{!!$errors->first('last_name', '<span class="control-label" style="color:#FF9494" for="last_name">*:message</span>')!!}
									</div>		
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="id_card">เลขที่บัตร : </label> 
										<input type="number" class="form-control" id="id_card" name="id_card" placeholder="เลขที่บัตรประชาชน" value="{{ old('id_card') == '' ? $person_info['id_card'] : old('id_card')}}">
										{!!$errors->first('id_card', '<span class="control-label" style="color:#FF9494" for="id_card">*:message</span>')!!}
									</div>		
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="birthday">วันเกิด : </label> 
										<div class="input-group">
						                	<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="birthday" name="birthday" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{ old('birthday') == '' ? convert_TH_FormatDate($person_info['birthday']) : old('birthday')}}">
										</div>
										{!!$errors->first('birthday', '<span class="control-label" style="color:#FF9494" for="birthday">*:message</span>')!!}
									</div>		
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="nationality">สัญชาติ : </label> 
										<select class="form-control" name="nationality" id="nationality">
							                <option value="">เลือก</option>
							                <?php
							                	foreach ($nationality as $key => $value) {
							                		$selected =  old('nationality') == $value['id'] ? 'selected="selected"':'';
							                		if($selected == ''){
							                			$selected =  $person_info['nationality_id'] == $value['id'] ? 'selected="selected"':'';
							                		}
							                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['nationality'].'</option>';
							                	}
							                ?>
						                </select>
						                {!!$errors->first('nationality', '<span class="control-label" style="color:#FF9494" for="nationality">*:message</span>')!!}
									</div>		
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="address">ที่อยู่ : </label> 
										<input type="text" class="form-control" id="address" name="address" placeholder="ที่อยู่" value="{{ old('address') == '' ? $person_info['address'] : old('address')}}">
										{!!$errors->first('address', '<span class="control-label" style="color:#FF9494" for="address">*:message</span>')!!}
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="tel">โทร. : </label> 
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-phone"></i>
											</div>
											<input type="text" name="tel" id="tel" class="form-control" value="{{ old('tel') == '' ? $person_info['phone'] : old('tel')}}">
										</div>
										{!!$errors->first('tel', '<span class="control-label" style="color:#FF9494" for="tel">*:message</span>')!!}
									</div>
								</div>
							</div>
							<?php if (count($file_info)>0): ?>
								<div class="row">
									<div class="col-md-12">
									<label for="tel">ไฟล์ : </label>
									<?php
		            				foreach ($file_info as $key => $value) {
		            				?>
		            					<span id="span-file{{$value['id']}}">
		            						<u><a href="{{ getImgFile($value['file_name']) }}" target="_blank"><i class="fa fa-fw fa-paperclip"></i>{{ $value['file_name'] }}</a></u> <a style="cursor:pointer" onclick="remove_file({{$value['id']}}, '{{ $value['file_name'] }}')">[ลบ]</a> |
		            					</span>
		            				<?php
		            				}
		            				?>
									</div>
								</div>
								<hr/>
							<?php endif; ?>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group" id="group-file">
										<label for="tel">อัพโหลดไฟล์</label> [ <a  id="add_img" style="cursor:pointer"> <i class="fa fa-plus"></i> เพิ่ม </a> ]
										<div class="input-group" id="f0" style="padding:10px 0 0 0">
											<div class="input-group-addon">
												<i class="fa fa-file-photo-o"></i>
											</div>
											<input type="file" name="file[]" class="form-control">
											<div class="input-group-addon" style="border-color:#FFF;padding:0 0 0 5px">
												<button type="button" class="btn btn-danger btn-sm" onclick="del_box_file(0);"> <i class="fa fa-minus"></i> </button>
											</div>
										</div>
									</div>
								</div>

							</div>
	            		</div>
	            		<!-- /.tab-pane -->
		<!-- *******************************Tab2********************************* -->
	            		<div class="tab-pane" id="occupant">
	            			<input type="hidden" name="occupant_id" value="{{ $occupant['id'] }}">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="prefix_id2" >คำนำหน้าชื่อ : </label> 
										<select class="form-control" name="prefix_id2" id="prefix_id2">
											<option value="">เลือก</option>
							                <?php
							                	foreach ($prefix_name as $key => $value) {
							                		$selected =  old('prefix_id2') == $value['id'] ? 'selected="selected"':'';
							                		if($selected == ''){
							                			$selected =  $occupant['prefix_id'] == $value['id'] ? 'selected="selected"':'';
							                		}
							                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['prefix_name'].'</option>';
							                	}
							                ?>
						                </select>
						                {!!$errors->first('prefix_id2', '<span class="control-label" style="color:#FF9494" for="prefix_id2">*:message</span>')!!}
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="first_name2">ชื่อ : </label> 
										<input type="text" class="form-control" id="first_name2" name="first_name2" placeholder="ชื่อ" value="{{ old('first_name2') == '' ? $occupant['first_name'] : old('first_name2')}}">
										{!!$errors->first('first_name2', '<span class="control-label" style="color:#FF9494" for="first_name">*:message</span>')!!}
									</div>		
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="last_name2">นามสกุล : </label> 
										<input type="text" class="form-control" id="last_name2" name="last_name2" placeholder="นามสกุล" value="{{ old('last_name2') == '' ? $occupant['last_name'] : old('last_name2')}}">
										{!!$errors->first('last_name2', '<span class="control-label" style="color:#FF9494" for="last_name2">*:message</span>')!!}

									</div>		
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="id_card2">เลขที่บัตร : </label>
										<input type="number" class="form-control" id="id_card2" name="id_card2" placeholder="เลขที่บัตรประชาชน" value="{{ old('id_card2') == '' ? $occupant['id_card'] : old('id_card2')}}">
										{!!$errors->first('id_card2', '<span class="control-label" style="color:#FF9494" for="id_card2">*:message</span>')!!}
									</div>		
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="birthday2">วันเกิด : </label> 
										<div class="input-group">
						                	<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="birthday2" name="birthday2" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{ $occupant['birthday'] != null ? convert_TH_FormatDate($occupant['birthday']) : '' }}">
										</div>
										{!!$errors->first('birthday2', '<span class="control-label" style="color:#FF9494" for="birthday2">*:message</span>')!!}
									</div>		
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="nationality2">สัญชาติ : </label> 
										<select class="form-control" name="nationality2" id="nationality2">
							                <option value="">เลือก</option>
							                <?php
							                	foreach ($nationality as $key => $value) {
							                		$selected =  old('nationality2') == $value['id'] ? 'selected="selected"':'';
							                		if($selected == ''){
							                			$selected =  $occupant['nationality_id'] == $value['id'] ? 'selected="selected"':'';
							                		}
							                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['nationality'].'</option>';
							                	}
							                ?>
						                </select>
						                {!!$errors->first('nationality2', '<span class="control-label" style="color:#FF9494" for="nationality2">*:message</span>')!!}
									</div>		
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="address2">ที่อยู่ : </label> 
										<input type="text" class="form-control" id="address2" name="address2" placeholder="ที่อยู่" value="{{ old('address2') == '' ? $occupant['address'] : old('address2')}}">
										{!!$errors->first('address2', '<span class="control-label" style="color:#FF9494" for="address2">*:message</span>')!!}
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label for="tel2">โทร. : </label> 
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-phone"></i>
											</div>
											<input type="text" name="tel2" id="tel2" class="form-control" value="{{ old('tel2') == '' ? $occupant['phone'] : old('tel2')}}">
										</div>
										{!!$errors->first('tel2', '<span class="control-label" style="color:#FF9494" for="tel2">*:message</span>')!!}
									</div>
								</div>
							</div>
	            		</div>
	            		<!-- /.tab-pane -->
	            		<hr/>
	            		<div class="row">
							<div class="col-md-6">
								<a href="{{ url('manage-member') }}" class="btn btn-primary">
		                			<i class="fa fa-arrow-left"></i> กลับ
		                        </a>
							</div>
								
							<div class="col-md-6">
								<button type="submit" class="btn btn-success pull-right" id="save" >
		                			บันทึก
		                        </button>
							</div>
						</div>

	            	</div>
	            	<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</form>

	<div class="row">
		<div class="col-md-12">

			<div class="box box-primary">
				<div class="box-header with-border">
                    <h3 class="box-title" >รายการจดทะเบียน</h3>

					<a href="{{ url('manage-member/create-car-registration/'.Request::segment('3')) }}" class="btn btn-primary pull-right"> เพิ่มข้อมูลรถ</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                	<div class="row">
                		<div class="col-md-12">
							<table id="tb-car-detail" class="table table-bordered table-striped">
				                <thead>
					                <tr>
					                	<th>เลขทะเบียน</th>
					                	<th>ยี่ห้อ</th>
					                	<th>แบบ</th>
					                	<th>วันที่จดทะเบียน</th>
					                	<th>วันหมดอายุทะเบียน</th>
					                	<th>แก้ไข</th>
					                </tr>
				                </thead>
				                <tbody>
				                	<?php
                                        foreach ($registration as $key => $value) {
                                        ?>
                                            <tr>
                                                <td>{{ $value['register_number'] }}</td>
							                	<td>{{ $value['car_detail']['car_brand'] }}</td>
							                	<td>{{ $value['car_detail']['model']==null?'-':$value['car_detail']['model'] }}</td>
							                	<td>{{ $value['register_date']==null?'-':convert_TH_FormatDate($value['register_date']) }}</td>
							                	<td>{{ convert_TH_FormatDate($value['register_expired_date']) }}</td>
							                	<td><a href="{{ url('manage-member/edit-car-registration').'/'.$value['customer_id'].'/'.$value['id'] }}" style="font-size:18px"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
				                </tbody>
				              </table>
						</div>
                	</div>
                </div>
                <!-- /.box-body -->
			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->

</section>
@stop
@section('scripts')
	<!-- InputMask -->
	<script src="{{ URL::asset('/assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

	<!-- dataTables -->
	<script src="{{ URL::asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
    	
    	function del_box_file(c) {
	    	
	    	$("#f"+c).remove();
		}

		function remove_file(id, file_name) {
			if(confirm('คุณต้องการลบไฟล์นี้หรือไม่!')){
				var url = '<?php echo url('manage-member/file/remove')?>';
				$.ajax({
                        type:"GET",
                        url: url,
                        data: { id : id, file_name : file_name},
                        success: function(data){
                            if(data > 0)
                            {
                            	$("#span-file" + id).remove();
                            }
                            else
                            {
                            	alert('ไม่สามารถลบไฟล์ได้')
                            }
                        }
                    });
			} else {
				return false;
			}
		}

        $(function () {
			
			$("[data-mask]").inputmask();
			
			$("#tb-car-detail").DataTable();

			$('#add_img').click(function() {
				var c = $('#count_f').val()*1 + 1;
				$('#count_f').val(c)
				$('#group-file').append('<div class="input-group" id="f'+ c +'" style="padding:10px 0 0 0"> <div class="input-group-addon"> <i class="fa fa-file-photo-o"></i> </div> <input type="file" name="file[]" class="form-control"> <div class="input-group-addon" style="border-color:#FFF;padding:0 0 0 5px"> <button type="button" class="btn btn-danger btn-sm" onclick="del_box_file('+ c +');"> <i class="fa fa-minus"></i> </button> </div> </div>');
			});
        });
    </script>
@stop
