@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        ข้อมูลรายการจดทะเบียน
        <small>ข้อมูลทะเบียน</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('manage-member')}}"> ข้อมูลลูกค้า </a></li>
        <li class="active">เพิ่มข้อมูลรายการจดทะเบียน</li>
    </ol>
</section>
<section class="content">

	@if(Session::has('message_member'))
	<div class="alert alert-success alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> {{Session::get('message_member')}}
	</div>
	@endif

	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> {{Session::get('message')}}
	</div>
	@endif

	@if(Session::has('message_fali'))
	<div class="alert alert-danger alert-dismissible  fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Fail!</strong> {{Session::get('message_fali')}}
	</div>
	@endif

<!-- form start -->
	<form role="form" method="post" action="{{ url('manage-member/create-car-registration/process') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <input type="hidden" name="cus_id" value="{{ Request::segment(3) }}" >
	    <div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">
					รายการจดทะเบียน
				</h3>
				[ เจ้าของรถ <u><b>{{ $person_info['prefix']['prefix_name'] }} {{ $person_info['first_name'] }} {{ $person_info['last_name'] }}</b></u> ]

				@if(Session::has('message'))
					<a target="_blank" href="{{ url('renew-act/bill/'.Request::segment(3).'/'.Request::get('regis_id')) }}" class="btn btn-primary pull-right">ออกบิล [ {{ Request::get('regis_number') }} ]</a>
				@endif
			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="registration_date">วันจดทะเบียน</label>
							<div class="input-group date">
	              				<div class="input-group-addon">
	                				<i class="fa fa-calendar"></i>
	              				</div>
	              				<input type="text" name="registration_date" class="form-control pull-right" id="datepicker" value="{{ old('registration_date') }}">
	            			</div>
	            			{!!$errors->first('registration_date', '<span class="control-label" style="color:#FF9494" for="registration_date">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="expired_registration_date">วันหมดอายุทะเบียน</label>
							<div class="input-group date">
	              				<div class="input-group-addon">
	                				<i class="fa fa-calendar"></i>
	              				</div>
	              				<input type="text" name="expired_registration_date" class="form-control pull-right" id="datepicker2" value="{{ old('expired_registration_date') }}" >
	            			</div>
	            			{!!$errors->first('expired_registration_date', '<span class="control-label" style="color:#FF9494" for="expired_registration_date">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="registration_number">เลขทะเบียน</label>
	              			<input type="text" name="registration_number" class="form-control" placeholder="เลขทะเบียน" value="{{ old('registration_number') }}">
	              			{!!$errors->first('registration_number', '<span class="control-label" style="color:#FF9494" for="registration_number">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="province">จังหวัด</label>
	              			<select class="form-control" name="province">
	              				<option value="">-- เลือก --</option>
				                	@foreach ($province as $key => $value)
				                		<?php $selected =  old('province') == $value['id'] ? 'selected="selected"':''; ?>
				                		<option value="{{ $value['id'] }}" {{ $selected }} >{{ $value['province'] }}</option>
				                	@endforeach
			                </select>
			                {!!$errors->first('province', '<span class="control-label" style="color:#FF9494" for="province">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="car_type">ประเภท</label>
	              			<select class="form-control" name="car_type" id="car_type">
	              				<option value="" data-ry="">-- เลือก --</option>
	              				@foreach($categoryCar as $key => $value)
	              					<?php $selected =  old('car_type') == $value['id'] ? 'selected="selected"':''; ?>
	              					<option value="{{ $value['id'] }}" {{ $selected }} >{{ $value['cat_name'] }}</option>
	              				@endforeach
			                </select>
			                {!!$errors->first('car_type', '<span class="control-label" style="color:#FF9494" for="car_type">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="ry">รย.</label>
	              			<input type="text" name="ry" id="ry" class="form-control" placeholder="รย. 1" value="{{ old('ry') }}">
	              			{!!$errors->first('ry', '<span class="control-label" style="color:#FF9494" for="ry">*:message</span>')!!}
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="manner_car">ลักษณะ</label>
							<input type="text" name="manner_car" class="form-control" placeholder="ลักษณะ" value="{{ old('manner_car') }}">
							{!!$errors->first('manner_car', '<span class="control-label" style="color:#FF9494" for="manner_car">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="car_brand">ยี่ห้อรถ</label>
							<input type="text" name="car_brand" class="form-control" placeholder="ยี่ห้อรถ" value="{{ old('car_brand') }}">
							{!!$errors->first('car_brand', '<span class="control-label" style="color:#FF9494" for="car_brand">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="car_model">แบบ</label>
	              			<input type="text" name="car_model" class="form-control" placeholder="แบบ" value="{{ old('car_model') }}">
	              			{!!$errors->first('car_model', '<span class="control-label" style="color:#FF9494" for="car_model">*:message</span>')!!}
						</div>		
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="car_year">รุ่นปี ค.ศ.</label>
	              			<div class="input-group">
			                	<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
	              				<input type="text" id="car_year" name="car_year" class="form-control" 
	              				data-date-format="yyyy" placeholder="รุ่นปี ค.ศ." value="{{ old('car_year') }}">
	              			</div>
	              			{!!$errors->first('car_year', '<span class="control-label" style="color:#FF9494" for="car_year">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="car_color">สี</label>
	              			<input type="text" name="car_color" class="form-control" placeholder="สี" value="{{ old('car_color') }}">
	              			{!!$errors->first('car_color', '<span class="control-label" style="color:#FF9494" for="car_color">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="chassis_number_number">เลขตัวรถ</label>
	              			<input type="text" name="chassis_number_number" class="form-control" placeholder="เลขตัวรถ" value="{{ old('chassis_number_number') }}">
	              			{!!$errors->first('chassis_number_number', '<span class="control-label" style="color:#FF9494" for="chassis_number_number">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="chassis_number_number_area">เลขตัวรถ อยู่ที่</label>
	              			<input type="text" name="chassis_number_number_area" class="form-control" placeholder="เลขตัวรถ อยู่ที่" value="{{ old('chassis_number_number_area') }}">
	              			{!!$errors->first('chassis_number_number_area', '<span class="control-label" style="color:#FF9494" for="chassis_number_number_area">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="machine_brand">ยี่ห้อเครื่องยนต์</label>
							 <input type="text" name="machine_brand" class="form-control" placeholder="ยี่ห้อเครื่องยนต์" value="{{ old('machine_brand') }}">
							 {!!$errors->first('machine_brand', '<span class="control-label" style="color:#FF9494" for="machine_brand">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="machine_number">เลขเครื่องยนต์</label>
	              			<input type="text" name="machine_number" class="form-control" placeholder="เลขเครื่องยนต์" value="{{ old('machine_number') }}">
	              			{!!$errors->first('machine_number', '<span class="control-label" style="color:#FF9494" for="machine_number">*:message</span>')!!}
						</div>		
					</div>

				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="machine_number_area">เลขเครื่องยนต์ อยู่ที่</label>
	              			<input type="text" name="machine_number_area" class="form-control" placeholder="เลขเครื่องยนต์ อยู่ที่" value="{{ old('machine_number_area') }}">
	              			{!!$errors->first('machine_number_area', '<span class="control-label" style="color:#FF9494" for="machine_number_area">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="fuel">เชื้อเพลิง</label>
	              			<input type="text" name="fuel" class="form-control" placeholder="เชื้อเพลิง" value="{{ old('fuel') }}">
	              			{!!$errors->first('fuel', '<span class="control-label" style="color:#FF9494" for="fuel">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="fuel_number">เลขถังแก๊ซ</label>
	              			<input type="text" name="fuel_number" class="form-control" placeholder="เลขถังแก๊ซ" value="{{ old('fuel_number') }}">
	              			{!!$errors->first('fuel_number', '<span class="control-label" style="color:#FF9494" for="fuel_number">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="piston_amount">จำนวนสูบ</label>
	              			<input type="number" name="piston_amount" class="form-control" placeholder="จำนวนสูบ" value="{{ old('piston_amount') }}">
	              			{!!$errors->first('piston_amount', '<span class="control-label" style="color:#FF9494" for="piston_amount">*:message</span>')!!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="cc">ซีซี</label>
	              			<input type="number" name="cc" class="form-control" placeholder="ซีซี" value="{{ old('cc') }}">
	              			{!!$errors->first('cc', '<span class="control-label" style="color:#FF9494" for="cc">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="horsepower">แรงม้า</label>
	              			<input type="number" name="horsepower" class="form-control" placeholder="แรงม้า" value="{{ old('horsepower') }}">
	              			{!!$errors->first('horsepower', '<span class="control-label" style="color:#FF9494" for="horsepower">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="wheel_type">ลักษณะล้อ</label>
	              			<input type="text" name="wheel_type" class="form-control" placeholder="ex. 2 เพลา 4 ล้อ ยาง 4 เส้น" value="{{ old('wheel_type') }}">
	              			{!!$errors->first('wheel_type', '<span class="control-label" style="color:#FF9494" for="wheel_type">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="weight_car">น้ำหนักรถ</label>
	              			<input type="number" name="weight_car" class="form-control" placeholder="น้ำหนักรถ" value="{{ old('weight_car') }}">
	              			{!!$errors->first('weight_car', '<span class="control-label" style="color:#FF9494" for="weight_car">*:message</span>')!!}
						</div>		
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="weight_load">น้ำหนักบรรทุก/น้ำหนักลงเพลา</label>
	              			<input type="text" name="weight_load" class="form-control" placeholder="น้ำหนักบรรทุก/น้ำหนักลงเพลา" value="{{ old('weight_load') }}">
	              			{!!$errors->first('weight_load', '<span class="control-label" style="color:#FF9494" for="weight_load">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="weight_car_total">น้ำหนักรถรวม</label>
	              			<input type="number" name="weight_car_total" class="form-control" placeholder="น้ำหนักรถรวม" value="{{ old('weight_car_total') }}">
	              			{!!$errors->first('weight_car_total', '<span class="control-label" style="color:#FF9494" for="weight_car_total">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="seat_amount">ที่นั่ง</label>
	              			<input type="number" name="seat_amount" class="form-control" placeholder="ที่นั่ง" value="{{ old('seat_amount') }}">
	              			{!!$errors->first('seat_amount', '<span class="control-label" style="color:#FF9494" for="seat_amount">*:message</span>')!!}
						</div>		
					</div>
				</div>
			</div>

			<div class="box-footer">
				<div class="row">
					<div class="col-md-10">
						<a href="javascript:history.back()"  class="btn btn-primary">
							<i class="fa fa-arrow-left"></i> 
							กลับ
						</a>
					</div>
					
					<div class="col-md-2">
						<button type="submit" class="btn btn-success pull-right">
							<i class="fa fa-save"></i> 
							บันทึก
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
@stop
@section('scripts')
	<script src="{{ URL::asset('/assets/bootstrap/js/bootstrap-datepicker-thai.js') }}"></script>
	<script src="{{ URL::asset('/assets/bootstrap/js/locales/bootstrap-datepicker.th.js') }}"></script>

    <script>
        $(function () {
        	$('#datepicker').datepicker({
        		language : 'th-th',
        		format:'dd/mm/yyyy',
      			autoclose: true
    		});

    		$('#datepicker2').datepicker({
    			language : 'th-th',
    			format:'dd/mm/yyyy',
      			autoclose: true
    		});

    		$('#car_year').datepicker({
    			language : 'th-th',
      			autoclose: true,
      			minViewMode: "years",
    		});

			// $('#car_type').on('change', function() {
			// 	var ry = $('#car_type option:selected').attr('data-ry');
			// 	$('#ry').val(ry);
			// });

        });
    </script>
@stop
