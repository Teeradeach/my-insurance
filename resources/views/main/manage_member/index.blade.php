@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        ข้อมูลลูกค้า
        <small>รายการข้อมูลเจ้าของรถ</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">ข้อมูลลูกค้า</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ url('manage-member/create-member') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                        </div>
                    </div>
                    <hr/>
                    <!-- form search -->
                    <form role="form" method="get">
                        <div class="row">
                            {{--<div class="col-xs-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" name="idCard" class="form-control" placeholder="รหัสบัตรประชาชน" value="{{Request::input('idCard')}}">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <input type="text" name="fullName" class="form-control" placeholder="ชื่อ-สกุล" value="{{Request::input('fullName')}}">
                                </div>       
                            </div>

                            <div class="col-xs-1">
                                <div class="form-group">
                                    <select name="limit" class="form-control">
                                        <option value="20">Limit</option>
                                        <option value="20" {{Request::input('limit')==20?'selected':''}} >20</option>
                                        <option value="50" {{Request::input('limit')==50?'selected':''}} >50</option>
                                        <option value="100" {{Request::input('limit')==100?'selected':''}} >100</option>
                                        <option value="500" {{Request::input('limit')==500?'selected':''}} >500</option>
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"> ค้นหา </button>
                                </div>       
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ที่อยู่</th>
                                        <th>เบอร์โทรศัพท์</th>
                                        <th>เพิ่มรถ</th>
                                        <th>แก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($personInfo['data'] as $key => $value) {
                                            $page = Request::input('page')!=null?Request::input('page'):1;
                                            $limit = Request::input('limit')!=null?Request::input('limit'):20;
                                            ?>
                                            <tr>
                                                <td align="center">{{ ($key+1)+($limit * ($page - 1)) }}</td>
                                                <td>{{ $value['prefix']['prefix_name'] }} {{ $value['first_name'] }} {{ $value['last_name'] }}</td>
                                                <td>{{ $value['address']==null?'-':$value['address'] }}</td>
                                                <td>{{ $value['phone'] }}</td>
                                                <td align="center"><a href="{{ url('manage-member/create-car-registration/'.$value['customer']['id']) }}"><span class="glyphicon glyphicon-plus" style="font-size:18px" aria-hidden="true"></span></a></td>
                                                <td align="center"><a href="{{ url('manage-member/edit/'.$value['customer']['id']) }}"><span class="glyphicon glyphicon-edit" style="font-size:18px" aria-hidden="true"></span></a></td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite" style="margin-top:15px">
                                Showing {{ $personInfo['from'] }} to {{ $personInfo['to'] }} of {{ $personInfo['total'] }} entries
                            </div>
                        </div>
                        <div class="col-xs-7 align-right ">
                            <?php
                            
                            $total_page = ($personInfo['total'] > 0)?ceil($personInfo['total']/$personInfo['per_page']):'';
                            $page = $pages->currentPage();

                            if ($total_page > 1) {

                            unset($_GET['page']);
                            $param = http_build_query($_GET);
                            $next = (($page+1)<=$total_page)?$page+1:$total_page;
                            $prev = (($page-1)>0)?$page-1:1;
                            ?>
                            <nav class="pull-right">
                                <ul class="pagination">
                                    <li name="first_page" <?php if($page == 1) {echo 'class="disabled"';} ?>><?php if($page == 1) { ?><span>หน้าแรก</span><?php } else { ?><a href="<?php echo url('manage-member');?>?page=<?php echo 1; ?><?php echo !empty($param)?'&'.$param:'';?>">หน้าแรก</a><?php } ?></li>
                                            <li <?php if($page == 1) {echo 'class="disabled"';} ?>><?php if ($page > 1): ?> <a href="<?php echo url('manage-member');?>?page=<?php echo $prev; ?><?php echo !empty($param)?'&'.$param:'';?>">«</a><?php else: ?><span>«</span><?php endif ?></li>
                                    <li <?php if($page==1){ echo 'class="active"';} ?> >
                                        <?php if ($page == 1): ?>
                                            <span>1</span>
                                        <?php else: ?>
                                            <a href="<?php echo url('manage-member');?>?page=<?php echo 1; ?><?php echo !empty($param)?'&'.$param:'';?>">1</a>
                                        <?php endif ?>
                                    </li>
                                    <?php
                                    if ($page <= 5 ) {
                                        $page_display = 2;
                                        $total_loop   = 4;
                                    } else {
                                        $page_display = $page-1;
                                        $total_loop   = 3;
                                    }
                                    for ($i=1;$i<=$total_loop;$i++) {
                                        if($page_display <= $total_page) {
                                            ?>

                                            <?php if (($total_loop == 3)&& ($i == 1)) { ?>
                                                <li <?php if($page==$page_display){ echo 'class="active"';} ?>>
                                                    <a>...</a>
                                                </li>
                                            <?php } ?>

                                            <li <?php if($page == $page_display){ echo 'class="active"';} ?>>
                                                <?php if ($page == $page_display): ?>
                                                    <span><?php echo $page_display ?></span>
                                                <?php else: ?>
                                                    <a href="<?php echo url('manage-member');?>?page=<?php echo $page_display; ?><?php echo !empty($param)?'&'.$param:'';?>"><?php echo $page_display; ?></a>
                                                <?php endif ?>
                                            </li>
                                        <?php
                                        }
                                        $page_display++;
                                    }
                                    ?>
                                    <li <?php if($page == $total_page) {echo 'class="disabled"';} ?>><?php if ($page < $total_page): ?><a href="<?php echo url('manage-member');?>?page=<?php echo $next; ?><?php echo !empty($param)?'&'.$param:'';?>"><i class="icon-double-angle-right">...</i></a><?php else: ?><span><i class="icon-double-angle-right">...</i></span><?php endif ?></li>
                                            <li name="last_page" <?php if($page == $total_page) {echo 'class="disabled"';} ?>><?php if($page == $total_page) {?><span>หน้าสุดท้าย</span><?php } else {?><a href="<?php echo url('manage-member');?>?page=<?php echo $total_page; ?><?php echo !empty($param)?'&'.$param:'';?>">หน้าสุดท้าย</a><?php } ?></li>

                                </ul>
                            </nav>
                        <?php } ?>



                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@stop