@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        เพิ่มข้อมูลเจ้าของรถ
        <small>ข้อมูลลูกค้า</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('manage-member')}}"> ข้อมูลลูกค้า </a></li>
        <li class="active">เพิ่มข้อมูลลูกค้า</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- form start -->
	<form role="form" method="post" action="{{ url('manage-member/create-member/process') }}" id="form-create" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<!-- ผู้ถือกรรมสิทธิ์ -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">ผู้ถือกรรมสิทธิ์</h3>
			</div>
			<!-- /.box-header -->

			<div class="box-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="titleName" >คำนำหน้าชื่อ</label>
							<select class="form-control" name="titleName" id="titleName">
								<option value="">เลือก</option>
				                <?php
				                	foreach ($prefix_name as $key => $value) {
				                		$selected =  old('titleName') == $value['id'] ? 'selected="selected"':'';
				                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['prefix_name'].'</option>';
				                	}
				                ?>
			                </select>
			                {!!$errors->first('titleName', '<span class="control-label" style="color:#FF9494" for="titleName">*:message</span>')!!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="firstName">ชื่อ</label>
							<input type="text" class="form-control" id="firstName" name="firstName" placeholder="ชื่อ" value="{{old('firstName')}}">
							{!!$errors->first('firstName', '<span class="control-label" style="color:#FF9494" for="firstName">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="lastName">นามสกุล</label>
							<input type="text" class="form-control" id="lastName" name="lastName" placeholder="นามสกุล" value="{{old('lastName')}}" >
							{!!$errors->first('lastName', '<span class="control-label" style="color:#FF9494" for="lastName">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="idCard">เลขที่บัตร</label>
							<input type="number" class="form-control" id="idCard" name="idCard" placeholder="เลขที่บัตรประชาชน" value="{{old('idCard')}}">
							{!!$errors->first('idCard', '<span class="control-label" style="color:#FF9494" for="idCard">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="birthday">วันเกิด  (พ.ศ.)</label>
							<div class="input-group">
			                	<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" id="birthday" name="birthday" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{old('birthday')}}">
							</div>
							{!!$errors->first('birthday', '<span class="control-label" style="color:#FF9494" for="birthday">*:message</span>')!!}
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="nationality">สัญชาติ</label>
							<select class="form-control" name="nationality" id="nationality">
				                <option value="">เลือก</option>
				                <?php
				                	foreach ($nationality as $key => $value) {
				                		$selected =  old('nationality') == $value['id'] ? 'selected="selected"':'';
				                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['nationality'].'</option>';
				                	}
				                ?>
			                </select>
			                {!!$errors->first('nationality', '<span class="control-label" style="color:#FF9494" for="nationality">*:message</span>')!!}
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="address">ที่อยู่</label>
							<input type="text" class="form-control" id="address" name="address" placeholder="ที่อยู่" value="{{old('address')}}">
							{!!$errors->first('address', '<span class="control-label" style="color:#FF9494" for="address">*:message</span>')!!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="tel">โทร.</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="text" name="tel" id="tel" class="form-control" value="{{old('tel')}}">
							</div>
							{!!$errors->first('tel', '<span class="control-label" style="color:#FF9494" for="tel">*:message</span>')!!}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
					<input type="hidden" id="count_f" value="0">
						<div class="form-group" id="group-file">
							<label for="tel">อัพโหลดไฟล์</label> [ <a  id="add_img" style="cursor:pointer"> <i class="fa fa-plus"></i> เพิ่ม </a> ]

							<div class="input-group" id="f0" style="padding:10px 0 0 0">
								<div class="input-group-addon">
									<i class="fa fa-file-photo-o"></i>
								</div>
								<input type="file" name="file[]" class="form-control">
								<div class="input-group-addon" style="border-color:#FFF;padding:0 0 0 5px">
									<button type="button" class="btn btn-danger btn-sm" onclick="del_box_file(0);"> <i class="fa fa-minus"></i> </button>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<!-- ผู้ครอบครอง -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">ผู้ครอบครอง</h3>
			</div>
			<!-- /.box-header -->

			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>
								<input type="checkbox" class="minimal" name="chk-dup" id="chk-dup" {{ old('chk-dup') == 'on' ? 'checked="checked"' : null }}> 
								ข้อมูลผ้ครอบครองตรงกับข้อมูลผู้ถือกรรมสิทธิ์
							</label>
						</div>
              
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="titleName2" >คำนำหน้าชื่อ</label>
							<select class="form-control" name="titleName2" id="titleName2">
				                <option value="">เลือก</option>
				                <?php
				                	foreach ($prefix_name as $key => $value) {
				                		$selected =  old('titleName2') == $value['id'] ? 'selected="selected"':'';
				                		echo '<option value="'.$value['id'].'" '. $selected .'>'.$value['prefix_name'].'</option>';
				                	}
				                ?>
			                </select>
			               
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="firstName2">ชื่อ</label>
							<input type="text" class="form-control" id="firstName2" name="firstName2" placeholder="ชื่อ" value="{{ old('firstName2') }}">
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="lastName2">นามสกุล</label>
							<input type="text" class="form-control" id="lastName2" name="lastName2" placeholder="นามสกุล" value="{{ old('lastName2') }}">
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="idCard2">เลขที่บัตร</label>
							<input type="number" class="form-control" id="idCard2" name="idCard2" placeholder="เลขที่บัตรประชาชน" value="{{ old('idCard2') }}">
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="birthday2">วันเกิด (พ.ศ.)</label>
							<div class="input-group" id="div-birthday2">
			                	<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" id="birthday2" name="birthday2" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{ old('birthday2') }}">
							</div>
						</div>		
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="nationality2">สัญชาติ</label>
							<select class="form-control" name="nationality2" id="nationality2">
				                <option value="">เลือก</option>
				                <?php
				                	foreach ($nationality as $key => $value) {
				                		$selected =  old('nationality2') == $value['id'] ? 'selected="selected"':'';
				                		echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['nationality'].'</option>';
				                	}
				                ?>
			                </select>
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="address2">ที่อยู่</label>
							<input type="text" class="form-control" id="address2" name="address2" placeholder="ที่อยู่" value="{{ old('address2') }}">
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label for="tel2">โทร.</label>
							<div class="input-group" id="div-tel2">
								<div class="input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="text" name="tel2" id="tel2" class="form-control" value="{{ old('tel2') }}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<div class="row">
					<div class="col-md-6">
						<a href="{{ url('manage-member') }}"  class="btn btn-primary">
							<i class="fa fa-arrow-left"></i> 
							กลับ
						</a>
					</div>
					<div class="col-md-6">
						<!-- <a href="{{ url('manage-member') }}"  class="btn btn-primary">
							<i class="fa fa-arrow-left"></i> 
							กลับ
						</a> -->
						<button type="button" class="btn btn-success pull-right" id="save-data">
							บันทึก & การจดทะเบียน 
							<i class="fa fa-arrow-right"></i>
						</button>
					</div>
				</div>
			</div>

		</div>
		<!-- /.box -->
	</form>
</section>
@stop
@section('scripts')
	<!-- InputMask -->
	<script src="{{ URL::asset('/assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <script>
	    
	    function del_box_file(c) {
	    	
	    	$("#f"+c).remove();
		}

        $(function () {
        	//Datemask dd/mm/yyyy
			$("[data-mask]").inputmask();

        	$('#chk-dup').click(function() {

   				if($("#chk-dup").is(':checked'))
   				{
   					$("#titleName2").val($("#titleName").val());
   					$("#firstName2").val($("#firstName").val());
   					$("#lastName2").val($("#lastName").val());
   					$("#idCard2").val($("#idCard").val());
   					$("#nationality2").val($("#nationality").val());
   					$("#address2").val($("#address").val());
   					$("#tel2").val($("#tel").val());

   					if($("#birthday").val() != '')
   					{
   						$("#birthday2").remove();
	   					$("#div-birthday2").append('<input type="text" id="birthday2" name="birthday2" class="form-control" value="'+ $("#birthday").val() +'">');
   					}
   				}
				else
				{
					$("#titleName2").val('');
   					$("#firstName2").val('');
   					$("#lastName2").val('');
   					$("#idCard2").val('');
   					$("#birthday2").val('');
   					$("#nationality2").val('');
   					$("#address2").val('');
   					$("#tel2").val('');
   					
   					if($("#birthday").val() != '')
   					{
   						$("#birthday2").remove();
	   					$("#div-birthday2").append('<input type="text" id="birthday2" name="birthday2" class="form-control" data-inputmask="\'alias\': \'dd/mm/yyyy\'" data-mask>');
						
						// $("[data-mask]").inputmask();
   					}

				}
			});

			$('#add_img').click(function() {
				var c = $('#count_f').val()*1 + 1;
				$('#count_f').val(c)
				$('#group-file').append('<div class="input-group" id="f'+ c +'" style="padding:10px 0 0 0"> <div class="input-group-addon"> <i class="fa fa-file-photo-o"></i> </div> <input type="file" name="file[]" class="form-control"> <div class="input-group-addon" style="border-color:#FFF;padding:0 0 0 5px"> <button type="button" class="btn btn-danger btn-sm" onclick="del_box_file('+ c +');"> <i class="fa fa-minus"></i> </button> </div> </div>');
			});

			$('#save-data').click(function() {
				if(confirm("คุณต้องการบันทึกข้อมูล เจ้าของรถใช่หรือไม่"))
				{
					$( "#form-create" ).submit();	
				}
				return false;
				
			});	
		
        });


    </script>
@stop
