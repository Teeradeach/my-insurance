@extends('main.layouts.template')
@section('content')
<section class="content-header">
    <h1>
        แจ้งเตือน
        <small>ต่อทะเบียน</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">แจ้งเตือน</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">ค้นหาข้อมูล</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- form search -->
                    <form role="form" method="get">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <input type="text" name="regis_number" id="regis_number" class="form-control" placeholder="เลขทะเบียนรถ" value="{{Request::input('regis_number')}}">
                                </div>
                            </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <input type="text" name="fullName" id="fullName" class="form-control" placeholder="ชื่อ-สกุล" value="{{Request::input('fullName')}}">
                                </div>       
                            </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <select name="is_follow_up" class="form-control" id="is_follow_up">
                                        <option value="">การติดตามลูกค้า</option>
                                        <option value="1" {{Request::input('is_follow_up')=='1'?'selected':''}} >ติดตามแล้ว</option>
                                        <option value="0" {{Request::input('is_follow_up')=='0'?'selected':''}} >รอติดตาม</option>
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <select name="branch" id="branch" class="form-control">
                                        <option value="">-- สาขา --</option>
                                        @foreach ($branch as $key => $value)
                                            <?php 
                                            $selected =  Request::input('branch') == $value['id'] ? 'selected="selected"':''; 
                                            ?>
                                            <option value="{{ $value['id'] }}" {{ $selected }} >{{ $value['branch_name'] }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-1">
                                <div class="form-group">
                                    <select name="limit" class="form-control" id="limit">
                                        <option value="20">Limit</option>
                                        <option value="5" {{Request::input('limit')==5?'selected':''}} >5</option>
                                        <option value="20" {{Request::input('limit')==20?'selected':''}} >20</option>
                                        <option value="50" {{Request::input('limit')==50?'selected':''}} >50</option>
                                        <option value="100" {{Request::input('limit')==100?'selected':''}} >100</option>
                                        <option value="500" {{Request::input('limit')==500?'selected':''}} >500</option>
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <input type="text" name="register_expired_date" id="register_expired_date" class="form-control" placeholder="วันหมดอายุ" value="{{Request::input('register_expired_date')}}">
                                </div>
                            </div>

                            <div class="col-xs-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"> ค้นหา </button>
                                </div>       
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> ออกรายงาน </button>
                                </div>       
                            </div>
                            
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ทะเบียนรถ</th>
                                        <th>สาขาร้าน</th>
                                        <th>วันจดทะเบียน</th>
                                        <th>วันหมดอายุ</th>
                                        <th>ชื่อ-สุกล</th>
                                        <th>ที่อยู่</th>
                                        <th>เบอร์โทรศัพท์</th>
                                        <th width="130px">การติดตามลูกค้า</th>
                                        <th>ดูข้อมูล</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        
                                        foreach ($list['data'] as $key => $value) {
                                            $color_style = '';
                                            if($value['is_follow_up']==1){
                                                $color_style = 'background-color:#9fdf9f';
                                            }
                                        ?>
                                            <tr style="{{ $color_style }}" >
                                                <td>{{ $value['register_number'] }} <br/> {{ $value['province']['province'] }}</td>
                                                <td>{{ array_get($value, 'user.branch.branch_name') }}</td>
                                                <td>{{ convert_TH_FormatDate($value['register_date']) }}</td>
                                                <td style="color:red">{{ convert_TH_FormatDate($value['register_expired_date']) }}</td>
                                                <td>{{ $value['customer']['person_info']['prefix']['prefix_name'] }} {{ $value['customer']['person_info']['first_name'] }} {{ $value['customer']['person_info']['last_name'] }}</td>
                                                <td>{{ $value['customer']['person_info']['address'] }}</td>
                                                <td>{{ $value['customer']['person_info']['phone'] }}</td>
                                                <td class="form-group">
                                                    <select class="form-control" width="30" onchange="is_follow_up({{$value['id']}})" id="is_follow_up{{$value['id']}}">
                                                        <option value="1" {{$value['is_follow_up']==1?'selected':''}}>ติดตามแล้ว</option>
                                                        <option value="0"  {{$value['is_follow_up']==0?'selected':''}}>รอติดตาม</option>
                                                    </select>
                                                </td>
                                                <td align="center"><a href="{{ url('remind/detail/'.$value['customer']['id'].'/'.$value['id']) }}" style="font-size:18px"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite" style="margin-top:15px">
                                Showing {{ $list['from'] }} to {{ $list['to'] }} of {{ $list['total'] }} entries
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <?php
                            
                            $total_page = ($list['total'] > 0)?ceil($list['total']/$list['per_page']):'';
                            $page = $pages->currentPage();

                            if ($total_page > 1) {

                            unset($_GET['page']);
                            $param = http_build_query($_GET);
                            $next = (($page+1)<=$total_page)?$page+1:$total_page;
                            $prev = (($page-1)>0)?$page-1:1;
                            ?>
                            <nav class="pull-right">
                                <ul class="pagination">
                                    <li name="first_page" <?php if($page == 1) {echo 'class="disabled"';} ?>><?php if($page == 1) { ?><span>หน้าแรก</span><?php } else { ?><a href="<?php echo url('remind');?>?page=<?php echo 1; ?><?php echo !empty($param)?'&'.$param:'';?>">หน้าแรก</a><?php } ?></li>
                                            <li <?php if($page == 1) {echo 'class="disabled"';} ?>><?php if ($page > 1): ?> <a href="<?php echo url('remind');?>?page=<?php echo $prev; ?><?php echo !empty($param)?'&'.$param:'';?>">«</a><?php else: ?><span>«</span><?php endif ?></li>
                                    <li <?php if($page==1){ echo 'class="active"';} ?> >
                                        <?php if ($page == 1): ?>
                                            <span>1</span>
                                        <?php else: ?>
                                            <a href="<?php echo url('remind');?>?page=<?php echo 1; ?><?php echo !empty($param)?'&'.$param:'';?>">1</a>
                                        <?php endif ?>
                                    </li>
                                    <?php
                                    if ($page <= 5 ) {
                                        $page_display = 2;
                                        $total_loop   = 4;
                                    } else {
                                        $page_display = $page-1;
                                        $total_loop   = 3;
                                    }
                                    for ($i=1;$i<=$total_loop;$i++) {
                                        if($page_display <= $total_page) {
                                            ?>

                                            <?php if (($total_loop == 3)&& ($i == 1)) { ?>
                                                <li <?php if($page==$page_display){ echo 'class="active"';} ?>>
                                                    <a>...</a>
                                                </li>
                                            <?php } ?>

                                            <li <?php if($page == $page_display){ echo 'class="active"';} ?>>
                                                <?php if ($page == $page_display): ?>
                                                    <span><?php echo $page_display ?></span>
                                                <?php else: ?>
                                                    <a href="<?php echo url('remind');?>?page=<?php echo $page_display; ?><?php echo !empty($param)?'&'.$param:'';?>"><?php echo $page_display; ?></a>
                                                <?php endif ?>
                                            </li>
                                        <?php
                                        }
                                        $page_display++;
                                    }
                                    ?>
                                    <li <?php if($page == $total_page) {echo 'class="disabled"';} ?>><?php if ($page < $total_page): ?><a href="<?php echo url('remind');?>?page=<?php echo $next; ?><?php echo !empty($param)?'&'.$param:'';?>"><i class="icon-double-angle-right">...</i></a><?php else: ?><span><i class="icon-double-angle-right">...</i></span><?php endif ?></li>
                                            <li name="last_page" <?php if($page == $total_page) {echo 'class="disabled"';} ?>><?php if($page == $total_page) {?><span>หน้าสุดท้าย</span><?php } else {?><a href="<?php echo url('remind');?>?page=<?php echo $total_page; ?><?php echo !empty($param)?'&'.$param:'';?>">หน้าสุดท้าย</a><?php } ?></li>

                                </ul>
                            </nav>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

@stop
@section('scripts')
<script src="{{ URL::asset('/assets/bootstrap/js/bootstrap-datepicker-thai.js') }}"></script>
<script src="{{ URL::asset('/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $('#register_expired_date').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY',
                daysOfWeek: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
                monthNames: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
            }
        });

        var search_date = '<?php echo Request::get('register_expired_date')?>'
                if(search_date == '')
                    $('#register_expired_date').val('');

        $('#export').click(function(){
            var url = '<?php echo url('remind/export');?>';
            var regis_number = $('#regis_number').val();
            var fullName = $('#fullName').val();
            var is_follow_up = $('#is_follow_up').val();
            var branch = $('#branch').val();
            var limit = $('#limit').val();
            var page = '<?php echo Request::get('page'); ?>';
            var register_expired_date = $('#register_expired_date').val();

            url += '?regis_number=' + regis_number + '&fullName=' + fullName + '&is_follow_up=' + is_follow_up + '&branch=' + branch + '&limit=' + limit + '&page=' + page + '&register_expired_date=' + register_expired_date;
            window.open(url,'_blank');
        })
    });

    function is_follow_up(id) {
        var url = window.location.href
        if (confirm("กรุณายืนยันการติดตาม")) {
            var param = url.split('?');
            var query_string = '';

            if (param[1] !== undefined) {
                query_string = '?'+param[1];
            }
            
            window.location.href = "{{url('remind/is_follow_up')}}/"+id+"/"+$('#is_follow_up'+id).val()+query_string;    
        }
    }
    
</script>
@stop

