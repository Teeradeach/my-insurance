@extends('main.layouts.template')

@section('stylesheet')
	<link rel="stylesheet" href="{{ URL::asset('/assets/plugins/daterangepicker/daterangepicker.css') }}">
@stop

@section('content')
<section class="content-header">
    <h1>
        รายงาน
        <small>บิลเงินสด</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">รายงานบิลเงินสด</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="col-xs-12">
        	<div class="box box-success">
        		<div class="box-header with-border">
                    <h3 class="box-title">ข้อมูลบิลเงินสด</h3>
                </div>
                <div class="box-body">
                	<form role="form" method="get">
                        <input type="hidden" id="bill_type" name="bill_type" value="{{ $bill_type }}">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="form-group">
                                    <input type="text" name="regis_number" id="regis_number" class="form-control" placeholder="เลขทะเบียนรถ" value="{{Request::input('regis_number')}}" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <div class="form-group">
                                    <input type="text" name="bill_date" id="bill_date" class="form-control" placeholder="วันที่ออกบิล" value="{{Request::input('bill_date')}}" autocomplete="off">
                                </div>       
                            </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <select name="branch" id="branch" class="form-control">
                                        <option value="">-- สาขา --</option>
                                        @foreach ($branch as $key => $value)
                                            <?php 
                                            $selected =  Request::input('branch') == $value['id'] ? 'selected="selected"':''; 
                                            ?>
                                            <option value="{{ $value['id'] }}" {{ $selected }} >{{ $value['branch_name'] }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-1">
                                <div class="form-group">
                                    <select name="limit" id="limit" class="form-control">
                                        <option value="20">Limit</option>
                                        <option value="20" {{Request::input('limit')==20?'selected':''}} >20</option>
                                        <option value="50" {{Request::input('limit')==50?'selected':''}} >50</option>
                                        <option value="100" {{Request::input('limit')==100?'selected':''}} >100</option>
                                        <option value="500" {{Request::input('limit')==500?'selected':''}} >500</option>
                                        <option value="1000" {{Request::input('limit')==1000?'selected':''}} >1000</option>
                                        <option value="2000" {{Request::input('limit')==2000?'selected':''}} >2000</option>
                                    </select>
                                </div>       
                            </div>

                            <div class="col-xs-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"> ค้นหา </button>
                                </div>       
                            </div>

                            <div class="col-xs-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> ออกรายงาน </button>
                                </div>       
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>เลขที่บิล</th>
                                        <th>ทะเบียนรถ</th>
                                        <th>วันที่ออกบิล</th>
                                        <th>สาขา</th>
                                        <th>พ.ร.บ. (บาท)</th>
                                        <th>ภาษี (บาท)</th>
                                        <th>ตรอ. (บาท)</th>
                                        <th>ประกันภัย (บาท)</th>
                                        <th>บริการ (บาท)</th>
                                        <th>ส่วนลด (บาท)</th>
                                        <th>ชื่อรายการ อื่นๆ</th>
                                        <th>อื่นๆ (บาท)</th>
                                        <th>รวม (บาท)</th>
                                        <th align="center">แก้ไขบิล</th>
                                        <!-- <th align="center">พิมพ์</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($list['data'] as $key => $value) {
                                            ?>
                                            <tr>
                                                <td>{{ array_get($value, 'bill_no', '-') }}</td>
                                                <td>
                                                    {{ array_get($value, 'registration.register_number') }} <br/>
                                                    {{ array_get($value, 'registration.province.province') }}
                                                </td>
                                                <td>{{ convert_TH_FormatDate(array_get($value, 'created_at')) }}</td>
                                                <td>{{ array_get($value, 'user.branch.branch_name') }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'act_price', 0.00), 2) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'vat_price', 0.00), 2) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'tra_price', 0.00), 2) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'insurance_bill.insurance_price', 0.00), 2) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'service_price', 0.00), 2) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'discount_price', 0.00), 2) }}</td>
                                                <td align="right">{{ array_get($value, 'etc_text', 0.00) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'etc_price', 0.00), 2) }}</td>
                                                <td align="right">{{ number_format(array_get($value, 'total', 0.00), 2) }}</td>
                                                <td align="center">
                                                    <a style="font-size:17px" href="{{ url('report/edit-bill').'/'.$value['customer_id'].'/'.$value['regis_id'].'/'.$value['id'] }}">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </a>
                                                </td>
                                                <!--<td align="center">
                                                    <a href="{{ url('report/print/bill/process').'/'.$value['customer_id'].'/'.$value['regis_id'].'/'.$value['id'] }}?version=2" target="_blank">
                                                        <i class="glyphicon glyphicon-print"></i>
                                                    </a>
                                                </td>-->
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite" style="margin-top:15px">
                                Showing {{ $list['from'] }} to {{ $list['to'] }} of {{ $list['total'] }} entries
                            </div>
                        </div>
                        <div class="col-xs-7 align-right ">
                        <?php
                            
                            $total_page = ($list['total'] > 0)?ceil($list['total']/$list['per_page']):'';
                            $page = $pages->currentPage();

                            if ($total_page > 1) {

                            unset($_GET['page']);
                            $param = http_build_query($_GET);
                            $next = (($page+1)<=$total_page)?$page+1:$total_page;
                            $prev = (($page-1)>0)?$page-1:1;
                            ?>
                            <nav class="pull-right">
                                <ul class="pagination">
                                    <li name="first_page" <?php if($page == 1) {echo 'class="disabled"';} ?>><?php if($page == 1) { ?><span>หน้าแรก</span><?php } else { ?><a href="<?php echo url('report/bill');?>?page=<?php echo 1; ?><?php echo !empty($param)?'&'.$param:'';?>">หน้าแรก</a><?php } ?></li>
                                            <li <?php if($page == 1) {echo 'class="disabled"';} ?>><?php if ($page > 1): ?> <a href="<?php echo url('report/bill');?>?page=<?php echo $prev; ?><?php echo !empty($param)?'&'.$param:'';?>">«</a><?php else: ?><span>«</span><?php endif ?></li>
                                    <li <?php if($page==1){ echo 'class="active"';} ?> >
                                        <?php if ($page == 1): ?>
                                            <span>1</span>
                                        <?php else: ?>
                                            <a href="<?php echo url('report/bill');?>?page=<?php echo 1; ?><?php echo !empty($param)?'&'.$param:'';?>">1</a>
                                        <?php endif ?>
                                    </li>
                                    <?php
                                    if ($page <= 5 ) {
                                        $page_display = 2;
                                        $total_loop   = 4;
                                    } else {
                                        $page_display = $page-1;
                                        $total_loop   = 3;
                                    }
                                    for ($i=1;$i<=$total_loop;$i++) {
                                        if($page_display <= $total_page) {
                                            ?>

                                            <?php if (($total_loop == 3)&& ($i == 1)) { ?>
                                                <li <?php if($page==$page_display){ echo 'class="active"';} ?>>
                                                    <a>...</a>
                                                </li>
                                            <?php } ?>

                                            <li <?php if($page == $page_display){ echo 'class="active"';} ?>>
                                                <?php if ($page == $page_display): ?>
                                                    <span><?php echo $page_display ?></span>
                                                <?php else: ?>
                                                    <a href="<?php echo url('report/bill');?>?page=<?php echo $page_display; ?><?php echo !empty($param)?'&'.$param:'';?>"><?php echo $page_display; ?></a>
                                                <?php endif ?>
                                            </li>
                                        <?php
                                        }
                                        $page_display++;
                                    }
                                    ?>
                                    <li <?php if($page == $total_page) {echo 'class="disabled"';} ?>><?php if ($page < $total_page): ?><a href="<?php echo url('report/bill');?>?page=<?php echo $next; ?><?php echo !empty($param)?'&'.$param:'';?>"><i class="icon-double-angle-right">...</i></a><?php else: ?><span><i class="icon-double-angle-right">...</i></span><?php endif ?></li>
                                            <li name="last_page" <?php if($page == $total_page) {echo 'class="disabled"';} ?>><?php if($page == $total_page) {?><span>หน้าสุดท้าย</span><?php } else {?><a href="<?php echo url('report/bill');?>?page=<?php echo $total_page; ?><?php echo !empty($param)?'&'.$param:'';?>">หน้าสุดท้าย</a><?php } ?></li>

                                </ul>
                            </nav>
                        <?php } ?>
                        </div>
                    </div>

                </div>
        	</div>
        </div>
    </div>
</section>
@stop

@section('scripts')
    <script src="{{ URL::asset('/assets/bootstrap/js/bootstrap-datepicker-thai.js') }}"></script>
	<script src="{{ URL::asset('/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script>
        $(function () {
            	$('#bill_date').daterangepicker({
            		// autoUpdateInput: false,
                    locale: {
                        format: 'DD/MM/YYYY',
                        daysOfWeek: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
                        monthNames: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
                    }
                });
                
                var search_date = '<?php echo Request::get('bill_date')?>'
                if(search_date == '')
                    $('#bill_date').val('');
                 
                

                $('#export').click(function(){
                    var url = '<?php echo url('report/export-bill');?>';
                    var regis_number = $('#regis_number').val();
                    var bill_date = $('#bill_date').val();
                    var branch = $('#branch').val();
                    var limit = $('#limit').val();
                    var page = '<?php echo Request::get('page'); ?>';
                    var bill_type = $('#bill_type').val();

                    url += '?regis_number=' + regis_number +
                    '&bill_date=' + bill_date +
                    '&branch=' + branch +
                    '&limit=' + limit +
                    '&page=' + page +
                    '&bill_type=' + bill_type;

                    window.open(url,'_blank');
                })
        	});
    </script>
@stop
