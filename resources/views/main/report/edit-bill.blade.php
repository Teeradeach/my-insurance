@extends('main.layouts.template')
@section('stylesheet')
<?php
$bill_header = 'บิลเงินสด';
if ($bill['bill_type'] == 2) {
    $bill_header = "ใบแจ้งหนี้";
}
?>
	<link rel="stylesheet" href="{{ URL::asset('/assets/plugins/iCheck/all.css') }}">
    <style>
    /*    table, td, th {
            border: 1px solid #AAA;
            border-bottom-color: #AAA;
        }*/
        .table {
            border: 0.5px solid #AAA;
        }
        .table-bordered > thead > tr > th,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > tbody > tr > td,
        .table-bordered > tfoot > tr > td {
            border: 0.5px solid #AAA;
        }
        input[type=checkbox] {
            transform: scale(1.2);
        }

        /*input {
            box-sizing: border-box;
            border: 1px solid #ccc;
            height: 30px;
            padding: 10px;
        }*/
        input.loading {
            background: url({{ asset('/assets/images/loading.gif') }}) no-repeat right center;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop
@section('content')
<section class="content-header">
    <h1>
        แก้ไข{{$bill_header}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('report/bill')}}">รายงานบิลเงินสด</a></li>
        <li class="active">แก้ไขออกบิล</li>
    </ol>
</section>

<section class="content">
    <form method="post" id="form-create-bill" action="{{url('report/process-edit-bill')}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="cus_id" value="{{ Request::segment(3) }}">
    <input type="hidden" name="regis_id" value="{{ Request::segment(4) }}">
    <input type="hidden" name="bill_id" value="{{ Request::segment(5) }}">
    <input type="hidden" name="bill_no" value="{{ array_get($bill, 'bill_no') }}">
    <input type="hidden" name="bill_type" value="{{ array_get($bill, 'bill_type') }}">
    <!-- แก้ไข -->
    <input type="hidden" name="person_info_id" value="{{ array_get($customer, 'person_info.id') }}">

    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>คำนำหน้าชื่อ :</label>
                                <select class="form-control" name="prefix_id" id="prefix_id">
                                    <?php
                                        foreach ($prefix_name as $key => $value) {
                                            $selected =  array_get($customer, 'person_info.prefix.id') == $value['id'] ? 'selected="selected"':'';
                                            echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['prefix_name'].'</option>';
                                        }
                                    ?>
                                </select>
                                {!!$errors->first('prefix_id', '<span class="control-label" style="color:#FF9494" for="prefix_id">*:message</span>')!!}
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="firstName">ชื่อ : </label>
                                <!-- แก้ไข -->
                                <input type="text" class="form-control" id="firstName" name="first_name" placeholder="ชื่อ" value="{{array_get($customer, 'person_info.first_name')}}">
                                {!!$errors->first('firstName', '<span class="control-label" style="color:#FF9494" for="firstName">*:message</span>')!!}
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="lastName">นามสกุล</label>
                                <!-- แก้ไข -->
                                <input type="text" class="form-control" id="lastName" name="last_name" placeholder="นามสกุล" value="{{array_get($customer, 'person_info.last_name')}}" >
                                {!!$errors->first('lastName', '<span class="control-label" style="color:#FF9494" for="lastName">*:message</span>')!!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="phone">เบอร์โทรศัพท์ :</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="เบอร์โทรศัพท์" value="{{array_get($customer, 'person_info.phone')}}" >
                                {!!$errors->first('phone', '<span class="control-label" style="color:#FF9494" for="phone">*:message</span>')!!}
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="registration">ทะเบียน :</label>
                                <input type="text" class="form-control" id="registration" name="registration" placeholder="ทะเบียน" value="{{array_get($regis, 'register_number')}}" >
                                {!!$errors->first('registration', '<span class="control-label" style="color:#FF9494" for="registration">*:message</span>')!!}
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="province">จังหวัด :</label>
                                <select class="form-control" name="province" id="province">
                                    <option value="">-- เลือก --</option>
                                    @foreach ($province as $key => $value)
                                        <?php $selected =  array_get($regis, 'province_id') == $value['id'] ? 'selected="selected"':''; ?>
                                        <option value="{{ $value['id'] }}" {{ $selected }} >{{ $value['province'] }}</option>
                                    @endforeach
                                </select>
                                {!!$errors->first('province', '<span class="control-label" style="color:#FF9494" for="province">*:message</span>')!!}
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="ry">รย. :</label>
                                <input type="number" class="form-control" id="ry" name="ry" placeholder="รย." value="{{array_get($regis, 'car_detail.ry')}}" >
                                {!!$errors->first('ry', '<span class="control-label" style="color:#FF9494" for="ry">*:message</span>')!!}

                                <input type="hidden" name="car_id" value="{{array_get($regis, 'car_detail.id')}}" >
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="register_expired_date">ภาษีหมดอายุ :</label>
                                <input type="text" class="form-control" id="register_expired_date" name="register_expired_date" placeholder="วันที่ภาษีหมดอายุ" value="{{ convert_TH_FormatDate( substr($bill['register_expired_date'],0,4)=='0000'?$regis['register_expired_date']:$bill['register_expired_date'] ) }}" >
                                {!!$errors->first('register_expired_date', '<span class="control-label" style="color:#FF9494" for="register_expired_date">*:message</span>')!!}
                            </div>
                        </div>

                    </div>



                    
                    <hr/>

                	<div class="row">
                		<div class="col-xs-2">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="flat-red" name="act[chk]" id="act" data-name="act_price" checked >  
                                    พ.ร.บ.
                                </label> 
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="act[text]" id="act_text" placeholder="ข้อมูล พ.ร.บ." data-title="พ.ร.บ." value="{{str_replace('พ.ร.บ. ','',array_get($bill, 'act_text'))}}">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="act[price]" id="act_price" placeholder="ราคา พ.ร.บ." data-title="พ.ร.บ." value="{{array_get($bill, 'act_price')}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="flat-red" name="vat[chk]" id="vat" data-name="vat_price" checked > 
                                    ภาษี 
                                </label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="vat[price]" id="vat_price" placeholder="ราคา ภาษี" data-title="ภาษี" value="{{array_get($bill, 'vat_price')}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="tra[chk]" id="tra" data-name="tra_price" checked> 
                					ตรอ.
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="tra[price]" id="tra_price" placeholder="ราคา ตรอ." data-title="ตรอ." value="{{array_get($bill, 'tra_price')}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="insurance[chk]" id="insurance" data-name="insurance" >
                					ประกันภัย
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="insurance[text]" id="insurance_text" placeholder="ข้อมูลประกัน" data-title="ประกันภัย" value="{{array_get($bill, 'insurance_bill.insurance_text')}}" <?php echo array_get($bill, 'insurance_bill.insurance_price') > 0 ? '' : 'disabled';?> >
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="insurance[price]" id="insurance_price" disabled placeholder="ราคา ประกัน..." data-title="ประกันภัย" value="{{array_get($bill, 'insurance_bill.insurance_price')}}">

                                <input type="hidden" class="form-control" name="insurance[insurance_id]" id="insurance_id" value="{{array_get($bill, 'insurance_bill.id')}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="service[chk]" id="service" data-name="service_price" checked> 
                					บริการ
								</label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="service[price]" id="service_price" placeholder="ราคา ค่าบริการ..." data-title="บริการ" value="{{array_get($bill, 'service_price')}}">
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="discount[chk]" id="discount" data-name="discount_price" <?php echo array_get($bill, 'discount_price') > 0 ? 'checked' : ''; ?> >
                					ส่วนลด
								</label>
                            </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="discount[price]" id="discount_price" placeholder="ส่วนลด..." data-title="ส่วนลด" value="{{array_get($bill, 'discount_price')}}" <?php echo array_get($bill, 'discount_price') > 0 ? '' : 'disabled'; ?>>
                            </div>
                        </div>
                	</div>

                	<div class="row">
                		<div class="col-xs-2">
                			<div class="form-group">
                				<label>
                					<input type="checkbox" class="flat-red" name="etc[chk]" id="etc" data-name="etc_price" <?php echo array_get($bill, 'etc_price') > 0 ? 'checked' : ''; ?> >
                					อื่นๆ
								</label>
				              </div>
                		</div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="etc[text]" id="etc_text" placeholder="ข้อความ..." data-title="อื่นๆ" value="{{array_get($bill, 'etc_text')}}" <?php echo array_get($bill, 'etc_price') > 0 ? '' : 'disabled'; ?> >
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="number" class="form-control" name="etc[price]" id="etc_price" placeholder="ค่าอื่นๆ..." data-title="อื่นๆ" value="{{array_get($bill, 'etc_price')}}" <?php echo array_get($bill, 'etc_price') > 0 ? '' : 'disabled'; ?> >
                            </div>
                        </div>
                	</div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <a class="btn btn-primary" href="{{ url('report/bill') }}">
                                    Back
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" id="edit-bill">
                                <i class="fa fa-fw fa-save"></i> Print & Save
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
        	</div>
        </div>
	</div>
    
    <input type="hidden" name="total" id="total" value="">
    </form>
</section>

@stop
@section('scripts')
    <!-- InputMask -->
    <script src="{{ URL::asset('/assets/bootstrap/js/bootstrap-datepicker-thai.js') }}"></script>
    <script src="{{ URL::asset('/assets/bootstrap/js/locales/bootstrap-datepicker.th.js') }}"></script>

	<script src="{{ URL::asset('/assets/js/jquery.number.min.js') }}"></script>
    
    <script>
        $(function () {
            
            var total = 0;
            var listBill = '';
            var listBillEmpty = '';
            var status = true;

            $('#register_expired_date').datepicker({
                language : 'th-th',
                format:'dd/mm/yyyy',
                autoclose: true,
            });

            var old_chk_insurance = "<?php echo array_get($bill, 'insurance_bill.insurance_price')>0?'on':''; ?>";


            $('#edit-bill').click(function() {
                total = 0;
                listBill = '';
                listBillEmpty = '';
                status = true;

                total += $('#act_price').val()==null || !$('#act').is(':checked') ? 0 : $('#act_price').val()*1;
                total += $('#vat_price').val()==null || !$('#vat').is(':checked') ? 0 : $('#vat_price').val()*1;
                total += $('#tra_price').val()==null || !$('#tra').is(':checked') ? 0 : $('#tra_price').val()*1;
                total += $('#insurance_price').val()==null ? 0 : $('#insurance_price').val()*1;
                total += $('#service_price').val()==null || !$('#service').is(':checked') ? 0 : $('#service_price').val()*1;
                total -= $('#discount_price').val()==null ? 0 : $('#discount_price').val()*1;
                total += $('#etc_price').val()==null ? 0 : $('#etc_price').val()*1;

                if (total <= 0) {
                    alert('รายการบิลต้องมีจำนวนเงินมากกว่า 0 บาท');
                    return false;
                }

                $('#total').val(total);

                if(confirm('คุณต้องการแก้ไขบิล "<?php echo getBillNo(array_get($bill, 'bill_no', 0))?>" ใช่หรือไม่')){
                    $('#form-create-bill').submit();
                }
                return false;
                
            });

            $('#act').change(function() {
                if ($('#act').is(':checked'))
                {
                    $('#act_price').prop("disabled", false);
                    $('#act_text').prop("disabled", false);
                }
                else
                {
                    $('#act_price').prop("disabled", true);
                    $('#act_text').prop("disabled", true);
                }

            });

            $('#vat').change(function() {   
                if ($('#vat').is(':checked')) 
                {
                    $('#vat_price').prop("disabled", false);
                }
                else
                {
                    $('#vat_price').prop("disabled", true);   
                }
                
            });

            $('#tra').change(function() {
                if ($('#tra').is(':checked')) 
                {
                    $('#tra_price').prop("disabled", false);
                }
                else
                {
                    $('#tra_price').prop("disabled", true);   
                }
                
            });

            $('#insurance').click(function() {
                if ($('#insurance').is(':checked')) 
                {
                    $('#insurance_text').prop("disabled", false);
                    $('#insurance_price').prop("disabled", false);
                }
                else
                {
                    $('#insurance_text').prop("disabled", true);
                    $('#insurance_price').prop("disabled", true);
                }
                
            });

            $('#insurance_company').change(function() {
                var company_id = $('#insurance_company').val();
                var url = '<?php echo url('renew-act/insurance-info/type')?>/'+company_id;
                if(company_id != '')
                {
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            var dataArray = JSON.parse(data);
                            var html_insurance_company = '<option value="">-- ประเภทประกัน --</option>';
                            $.each(dataArray, function( index, value ) {
                                html_insurance_company += '<option value="'+ value['id'] +'">'+ value['type_name'] +'</option>';
                            });

                            $('#insurance_type').html(html_insurance_company);

                            if(old_chk_insurance == "on"){
                                $('#insurance_type').val('<?php echo array_get($bill, 'insurance_bill.insurance_type.id'); ?>');
                            }
                        }
                    });
                }
                else
                {
                    $('#insurance_type').html('<option value="">-- ประเภทประกัน --</option>');
                } 
            });

            $('#service').change(function() {
                if ($('#service').is(':checked')) 
                {
                    $('#service_price').prop("disabled", false);
                }
                else
                {
                    // $('#service_price').val('');
                    $('#service_price').prop("disabled", true);   
                }
                
            });

            $('#discount').change(function() {
                if ($('#discount').is(':checked')) 
                {
                    $('#discount_price').prop("disabled", false);
                }
                else
                {
                    $('#discount_price').val('');
                    $('#discount_price').prop("disabled", true);   
                }
                
            });

            $('#etc').change(function() {
                if ($('#etc').is(':checked')) 
                {
                    $('#etc_price').prop("disabled", false);
                }
                else
                {
                    $('#etc_price').val('');
                    $('#etc_price').prop("disabled", true);   
                }
                
            });

            $('#print-bill').click(function() {
                $('#form-create-bill').submit();
                
            });


            if (old_chk_insurance == "on") {
                $('#insurance').trigger("click");
            }

            $('#firstName').blur(function() {
                var fullname = '';
                if ($('#firstName').val() != '') 
                {
                    $('#lastName').addClass('loading');
                    $('#phone').addClass('loading');
                    $('#registration').addClass('loading');
                    $('#province').addClass('loading');
                    $('#ry').addClass('loading');

                    fullname = $('#firstName').val();
                    var url = '<?php echo url('renew-act/insurance-info/full-name')?>/'+fullname;
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            if(data != 'null')
                            {
                                var dataArray = JSON.parse(data);

                                if (typeof dataArray['last_names'] != 'undefined' && dataArray['last_names'].length > 0)
                                {
                                    $("#lastName").autocomplete({
                                        source: dataArray['last_names'],
                                        minLength: 0,
                                    }).focus(function(){  
                                        $(this).autocomplete("search");
                                    });
                                }

                                if (typeof dataArray['data'] != 'undefined' && dataArray['data'].length == 1)
                                {
                                    $('#lastName').val(dataArray['data'][0]['last_name']);
                                    $('#phone').val(dataArray['data'][0]['phone']);

                                    if(typeof dataArray['data'][0]['customer2']['registration2'] != 'undefined' && dataArray['data'][0]['customer2']['registration2'].length == 1)
                                    {
                                        $('#registration').val(dataArray['data'][0]['customer2']['registration2'][0]['register_number']);
                                        $('#province').val(dataArray['data'][0]['customer2']['registration2'][0]['province']['id']);

                                        $('#ry').val(dataArray['data'][0]['customer2']['registration2'][0]['car_detail2']['ry']);
                                        $('#ry').trigger("blur");
                                        
                                    }
                                }
                            }

                            $('#lastName').removeClass('loading');
                            $('#phone').removeClass('loading');
                            $('#registration').removeClass('loading');
                            $('#province').removeClass('loading');
                            $('#ry').removeClass('loading');
                        }
                    });
                }
            });
            var dataProvince = '';
            var dataRy = '';
            $('#lastName').blur(function() {
                var fullname = '';
                if ($('#firstName').val() != '' && $('#lastName').val() != '') 
                {
                    $('#phone').addClass('loading');
                    $('#registration').addClass('loading');
                    $('#province').addClass('loading');
                    $('#ry').addClass('loading');

                    fullname = $('#firstName').val() + '_' + $('#lastName').val();
                    var url = '<?php echo url('renew-act/insurance-info/full-name')?>/'+fullname;
                    $.ajax({
                        type:"GET",
                        url: url,
                        success: function(data){
                            if(data != 'null')
                            {
                                var dataArray = JSON.parse(data);

                                if (typeof dataArray['arr_regis'] != 'undefined' && dataArray['arr_regis'].length > 0)
                                {
                                    $("#registration").autocomplete({
                                        source: dataArray['arr_regis'],
                                        minLength: 0,
                                    }).focus(function(){  
                                        $(this).autocomplete("search");
                                    });
                                    dataProvince = dataArray['arr_province'];
                                    dataRy = dataArray['arr_ry'];
                                }

                                $('#phone').val(dataArray['data'][0]['phone']);
                                if(typeof dataArray['data'][0]['customer2']['registration2'] != 'undefined' && dataArray['data'][0]['customer2']['registration2'].length == 1)
                                {
                                    $('#registration').val(dataArray['data'][0]['customer2']['registration2'][0]['register_number']);
                                    $('#province').val(dataArray['data'][0]['customer2']['registration2'][0]['province']['id']);
                                    $('#ry').val(dataArray['data'][0]['customer2']['registration2'][0]['car_detail2']['ry']);
                                    $('#ry').trigger("blur");
                                }
                            }

                            $('#phone').removeClass('loading');
                            $('#registration').removeClass('loading');
                            $('#province').removeClass('loading');
                            $('#ry').removeClass('loading');
                        }
                    });
                }
            });

            $('#registration').blur(function() {
                if (typeof dataProvince[$('#registration').val()] != 'undefined')
                {
                    $('#province').val(dataProvince[$('#registration').val()]);
                }

                if (typeof dataRy[$('#registration').val()] != 'undefined')
                {
                    $('#ry').val(dataRy[$('#registration').val()]);
                    $('#ry').trigger("blur");
                }


            });

            $('#ry').blur(function() {
                var ry = $('#ry').val();
                var total = 0;
                if(ry != ''){
                    if ( ry == 1 ) {
                        total = 645;
                        
                    }
                    else if (ry == 2) {
                        total = 1182;
                        
                    }
                    else if (ry == 3) {
                        total = 968;
                        
                    }
                    else if (ry == 12) {
                        total = 324;
                    }

                    $('#act_price').val(total);
                }
            });
			
        });
    </script>
@stop

