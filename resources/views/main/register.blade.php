@extends('main.layouts.template-login')
@section('content')
<div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>
    @if(Session::has('status'))
		<P class="panel-body bg-success color-red">
		{{Session::get('status')}}
		</P>
	@endif
    
    <form action="{{ url('admin/register/process') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="name" placeholder="Full name" value="{{old('name')}}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        {!!$errors->first('name', '<span class="control-label" style="color:#FF9494" for="name">*:message</span>')!!}
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        {!!$errors->first('username', '<span class="control-label" style="color:#FF9494" for="username">*:message</span>')!!}
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        {!!$errors->first('password', '<span class="control-label" style="color:#FF9494" for="password">*:message</span>')!!}
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        {!!$errors->first('password_confirmation', '<span class="control-label" style="color:#FF9494" for="password_confirmation">*:message</span>')!!}
      </div>

      <div class="form-group has-feedback">
      <select class="form-control" name="branch">
  			<option value="">-- Select Branch --</option>
        <?php
        foreach ($branch as $key => $value) {
          $select = old('branch') == $value['id'] ? 'selected="selected"' : null;
        ?>
          <option value="{{ $value['id'] }}" {{ $select }} >{{ $value['branch_name'] }}</option>
        <?php  
        }
        ?>
		</select>
		{!!$errors->first('branch', '<span class="control-label" style="color:#FF9494" for="branch">*:message</span>')!!}
      </div>

      <div class="form-group has-feedback">
        <select class="form-control" name="type">
			<option value="">-- Select User Role --</option>
			<option value="admin" {{ (old('type') == 'Admin') ? 'selected="selected"' : null }}>Admin</option>
			<option value="user" {{ (old('type') == 'User') ? 'selected="selected"' : null }}>User</option>
		</select>
		{!!$errors->first('type', '<span class="control-label" style="color:#FF9494" for="type">*:message</span>')!!}
      </div>


      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>

    </form>

    <a href="{{ url('/login') }}" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
@stop