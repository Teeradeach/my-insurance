<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupant extends Model
{
    // protected $visible = ['id', 'prefix_id', 'em_id', 'first_name', 'last_name', 'id_card', 'birthday', 'nationality_id', 'address', 'phone', 'created_at', 'updated_at'];

    public function createOccupant($params = null)
    {
    	$response = false;
    	if($params)
    	{
    		$Occupant = new Occupant;

    		$Occupant->prefix_id = array_get($params, 'titleName2');
    		$Occupant->em_id = array_get($params, 'em_id');
    		$Occupant->first_name = array_get($params, 'firstName2');
    		$Occupant->last_name = array_get($params, 'lastName2');
    		$Occupant->id_card = array_get($params, 'idCard2');
    		$Occupant->birthday = array_get($params, 'birthday2');
    		$Occupant->nationality_id = array_get($params, 'nationality2');
    		$Occupant->address = array_get($params, 'address2');
    		$Occupant->phone = array_get($params, 'tel2');

			$response = $Occupant->save();
			$response = $Occupant->id;
    	}

    	return $response;
    }

    public function updateOccupant($params = null)
    {
        $response = false;
        if($params)
        {
            $Occupant = \App\Occupant::find($params['occupant_id']);
            
            $Occupant->prefix_id = $params['prefix_id2'];
            $Occupant->em_id = $params['em_id'];
            $Occupant->first_name = $params['first_name2'];
            $Occupant->last_name = $params['last_name2'];
            $Occupant->id_card = $params['id_card2'];
            $Occupant->birthday = $params['birthday2'];
            $Occupant->nationality_id = $params['nationality2'];
            $Occupant->address = $params['address2'];
            $Occupant->phone = $params['tel2'];

            $response = $Occupant->save();
        }

        return $response;
    }

    public function prefix()
    {
        return $this->hasOne('\App\Prefix', 'id', 'prefix_id')->select();
    }

    public function nationality()
    {
        return $this->hasOne('\App\Nationality', 'id', 'nationality_id')->select();
    }
}