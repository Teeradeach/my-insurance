<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonInfo extends Model
{
    // protected $visible = ['id', 'prefix_id', 'em_id', 'first_name', 'last_name', 'id_card', 'birthday', 'nationality_id', 'address', 'phone', 'created_at', 'updated_at'];

    public function createPersonInfo($params = null)
    {
    	$response = false;
        // alert($params);exit;
    	if($params)
    	{
    		$PersonInfo = new PersonInfo;

    		$PersonInfo->prefix_id = array_get($params, 'titleName');
    		$PersonInfo->em_id = array_get($params, 'em_id');
    		$PersonInfo->first_name = array_get($params, 'firstName');
    		$PersonInfo->last_name = array_get($params, 'lastName');
    		$PersonInfo->id_card = array_get($params, 'idCard');
    		$PersonInfo->birthday = array_get($params, 'birthday');
    		$PersonInfo->nationality_id = array_get($params, 'nationality', 1);
    		$PersonInfo->address = array_get($params, 'address', '-');
    		$PersonInfo->phone = array_get($params, 'tel', '-');

			$response = $PersonInfo->save();
			$response = $PersonInfo->id;
    	}

    	return $response;
    }

    public function updatePersonInfo($params = null)
    {
        $response = false;
        if($params)
        {
            $PersonInfo = \App\PersonInfo::find($params['person_info_id']);
            
            $PersonInfo->prefix_id = array_get($params, 'prefix_id');
            $PersonInfo->em_id = array_get($params, 'em_id');
            $PersonInfo->first_name = array_get($params, 'first_name');
            $PersonInfo->last_name = array_get($params, 'last_name');
            $PersonInfo->id_card = array_get($params, 'id_card');
            $PersonInfo->birthday = array_get($params, 'birthday');
            $PersonInfo->nationality_id = array_get($params, 'nationality', 1);
            $PersonInfo->address = array_get($params, 'address', '-');
            $PersonInfo->phone = array_get($params, 'tel', '-');

            $response = $PersonInfo->save();
        }

        return $response;
    }

    public function selectPersonInfo($param=null, $limit=20)
    {
    	$request = \App\PersonInfo::with(array('prefix', 'customer'))->select(array('*'));

    	if(isset($param['idCard']) && trim($param['idCard']) != '')
    	{
    		$request = $request->where('id_card', 'like', $param['idCard'].'%');
    	}

    	if(isset($param['fullName']) && trim($param['fullName']) != '')
    	{
    		$name = explode(' ', $param['fullName']);
    		$request = $request->where('first_name', 'like', '%'.$name[0].'%')
    		->Orwhere('last_name', 'like', '%'.array_get($name, '0').'%');
    	}
        $request->orderBy('created_at','DESC');
        $request = $request->paginate($limit);

    	return $request;
    }

    public function prefix()
    {
        return $this->hasOne('\App\Prefix', 'id', 'prefix_id')->select();
    }

    public function nationality()
    {
        return $this->hasOne('\App\Nationality', 'id', 'nationality_id')->select();
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer', 'id', 'person_info_id')->select();
    }

    public function customer2()
    {
        return $this->belongsTo('\App\Customer', 'id', 'person_info_id')->select()->with(array('registration2'));
    }

    
}