<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    // protected $visible = ['id', 'person_info_id', 'occupant_id', 'em_id', 'customer_code', 'created_at', 'updated_at'];

    public function createCustomer($params = null)
    {
    	$response = false;
    	if($params)
    	{
    		$customer = new Customer;

    		$customer->person_info_id = array_get($params, 'personInfo_id');
    		$customer->occupant_id = array_get($params, 'occupant_id');
    		$customer->em_id = array_get($params, 'em_id');
    		$customer->customer_code = null;

			$response = $customer->save();
			$response = $customer->id;
    	}

    	return $response;
    }

    public function personInfo()
    {
        return $this->hasOne('\App\PersonInfo', 'id', 'person_info_id')->select()->with(array('prefix', 'nationality'));
    }

    public function occupant()
    {
        return $this->hasOne('\App\Occupant', 'id', 'occupant_id')->select()->with(array('prefix', 'nationality'));
    }

    public function registration()
    {
        return $this->hasMany('\App\Registration', 'customer_id', 'id')->select()->with(array('carDetail'));
    }

    public function fileInfo()
    {
        return $this->hasMany('\App\FileInfo', 'customer_id', 'id')->select();
    }

    public function registration2()
    {
        return $this->hasMany('\App\Registration', 'customer_id', 'id')->select()->with(array('carDetail2', 'province'));
    }

}