<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    protected $visible = ['id', 'nationality', 'created_at'];
}