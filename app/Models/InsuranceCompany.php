<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceCompany extends Model
{

	public function insuranceType()
    {
        return $this->hasMany('\App\InsuranceType', 'insurance_company_id', 'id')->select();
    }
}

