<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
	public function createBill($params = null)
	{
		$response = false;
    	if($params)
    	{
    		$bill = new Bill;

    		$bill->customer_id = array_get($params, 'cus_id');
    		$bill->regis_id = array_get($params, 'regis_id');
    		$bill->em_id = array_get($params, 'em_id');
            $bill->act_price = array_get($params, 'act.price', 0);
    		$bill->act_text = array_get($params, 'act.text', null);
    		$bill->vat_price = array_get($params, 'vat.price', 0);
    		$bill->tra_price = array_get($params, 'tra.price', 0);
    		$bill->insurance_id = array_get($params, 'insurance_bill_id', null);
    		$bill->service_price = array_get($params, 'service.price', 0);
    		$bill->discount_price = array_get($params, 'discount.price', 0);
            $bill->etc_text = array_get($params, 'etc.text', null);
    		$bill->etc_price = array_get($params, 'etc.price', 0);
            $bill->total = array_get($params, 'total', 0);
    		$bill->register_expired_date = convertFormatDate(array_get($params, 'register_expired_date', 0));
            $bill->bill_type = array_get($params, 'bill_type', 1);

			$response = $bill->save();
			$response = $bill->id;
    	}

    	return $response;
	}

    public function updateBill($params = null)
    {
        $response = false;
        if($params)
        {
            $bill = \App\Bill::find($params['bill_id']);

            $bill->customer_id = array_get($params, 'cus_id');
            $bill->regis_id = array_get($params, 'regis_id');
            $bill->em_id = array_get($params, 'em_id');
            $bill->act_price = array_get($params, 'act.price', 0);
            $bill->act_text = array_get($params, 'act.text', null);
            $bill->vat_price = array_get($params, 'vat.price', 0);
            $bill->tra_price = array_get($params, 'tra.price', 0);
            $bill->insurance_id = array_get($params, 'insurance_bill_id', null);
            $bill->service_price = array_get($params, 'service.price', 0);
            $bill->discount_price = array_get($params, 'discount.price', 0);
            $bill->etc_text = array_get($params, 'etc.text', null);
            $bill->etc_price = array_get($params, 'etc.price', 0);
            $bill->total = array_get($params, 'total', 0);
            $bill->register_expired_date = convertFormatDate(array_get($params, 'register_expired_date', 0));
            if (isset($params['bill_type'])) {
                $bill->bill_type = array_get($params, 'bill_type', 1);
            }


            $response = $bill->save();
        }

        return $response;
    }

    public function selectBill($params = null, $type = 1)
    {
        $request = \App\Bill::with(array('user','customer', 'registration', 'insuranceBill'))
        ->orderBy('created_at','DESC')
        ->where('bill_type', $type);

        if(isset($params['regis_number']) && trim($params['regis_number']) != '')
        {
            $regis_number = $params['regis_number'];
            $request = $request->whereHas('Registration', function ($q) use ($regis_number) {
                $q->where('register_number', 'like', $regis_number.'%');
            });
        }

        if(isset($params['branch']) && trim($params['branch']) != '')
        {
            $branch_id = $params['branch'];
            $request = $request->whereHas('User', function ($q) use ($branch_id) {
                $q->whereHas('Branch', function ($q2) use ($branch_id) {
                    $q2->where('id', '=', $branch_id);
                });
            });
        }

        if(isset($params['bill_date_start']) && trim($params['bill_date_start']) != '')
        {
            $request = $request->where('created_at', '>=', $params['bill_date_start'].' 00:00:00');
        }

        if(isset($params['bill_date_end']) && trim($params['bill_date_end']) != '')
        {
            $request = $request->where('created_at', '<=', $params['bill_date_end'].' 23:59:59');
        }

        return $request;
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer', 'customer_id', 'id')->select()->with(array('personInfo'));
    }

    public function registration()
    {
        return $this->belongsTo('\App\Registration', 'regis_id', 'id')->select()->with(array('province'));
    }

    public function insuranceBill()
    {
        return $this->belongsTo('\App\InsuranceBill', 'insurance_id', 'id')->select()->with(array('insuranceCompany', 'insuranceType'));
    }

    public function user()
    {
        return $this->belongsTo('\App\User', 'em_id', 'id')->select()->with(array('branch'));
    }

}