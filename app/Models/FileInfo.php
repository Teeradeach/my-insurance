<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FileInfo extends Model
{
	public function createfileInfo($params=array(), $cus_id = null)
	{
		$response = false;
    	if($params)
    	{
    		foreach ($params as $key => $value) {
    			$file = new fileInfo;

	    		$file->customer_id = $cus_id;
	    		$file->title = 'file';
	    		$file->file_name = $value['file_name'];
	    		$file->extension = $value['ext'];

	    		$response = $file->save();
    		}
			
    	}

    	return $response;
	}
}