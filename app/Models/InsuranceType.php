<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceType extends Model
{
	public function insuranceCompany()
    {
        return $this->belongsTo('\App\InsuranceCompany', 'insurance_company_id', 'id')->select();
    }	
}