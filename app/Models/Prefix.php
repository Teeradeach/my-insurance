<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Prefix extends Model
{
    protected $visible = ['id', 'prefix_name', 'created_at'];
}
