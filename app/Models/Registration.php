<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
	public function createRegistration($params = null)
    {
    	$response = false;
    	if($params)
    	{
            $res = Registration::select()
                    ->where('province_id', '=', $params['province'])
                    ->where('register_number', '=', $params['registration_number'])
                    ->get()->toArray();

            if(count($res)==0)
            {
        		$registration = new Registration;
        		$registration->customer_id = array_get($params, 'cus_id');
                $registration->car_detail_id = array_get($params, 'car_detail_id');
                $registration->province_id = array_get($params, 'province');
                $registration->em_id = array_get($params, 'em_id');
                $registration->register_number = array_get($params, 'registration_number', null);
                $registration->register_date = array_get($params, 'registration_date');
                $registration->register_expired_date = array_get($params, 'expired_registration_date');
                $registration->is_follow_up = 0;

    			$response = $registration->save();
    			$response = $registration->id;
            }
            else
            {
                $registration = \App\Registration::find($res[0]['id']);

                $registration->customer_id = array_get($params, 'cus_id');
                // $registration->car_detail_id = array_get($params, 'car_detail_id');
                $registration->province_id = array_get($params, 'province');
                $registration->em_id = array_get($params, 'em_id');
                $registration->register_number = array_get($params, 'registration_number', null);
                $registration->register_date = array_get($params, 'registration_date');
                $registration->register_expired_date = array_get($params, 'expired_registration_date');
                $registration->is_follow_up = 0;

                $response = $registration->save();
                $response = $registration->id;
                // CarDetail::where('id', '=', trim($params['car_detail_id']))->delete();

            }
    	}

    	return $response;
    }

    public function updateRegistration($params = null)
    {
        $response = false;
        if($params)
        {

            $registration = \App\Registration::find($params['regis_id']);

            if($registration)
            {
                $registration->province_id = trim($params['province']);
                $registration->em_id = trim($params['em_id']);
                $registration->register_number = trim($params['registration_number']);
                if(isset($params['registration_date'])){
                    $registration->register_date = trim($params['registration_date']);    
                }
                $registration->register_expired_date = trim($params['expired_registration_date']);
                $registration->is_follow_up = 0;

                $response = $registration->save();
            }
            
        }

        return $response;
    }

    public function updateRegistrationExpiredDate($params = null)
    {
        $response = false;
        if($params)
        {

            $registration = \App\Registration::find($params['regis_id']);
            if($registration)
            {
                $registration->register_expired_date = trim(convertFormatDate($params['register_expired_date']));
                $registration->is_follow_up = 0;

                $response = $registration->save();
            }
            
        }

        return $response;
    }

    public function selectRegistrationInfo($param=null, $limit=20)
    {
        $request = \App\Registration::with(array('customer'))->select(array('*'));

        if(isset($param['regis_number']) && trim($param['regis_number']) != '')
        {
            $request = $request->where('register_number', 'like', $param['regis_number'].'%');
        }

        if(isset($param['fullName']) && trim($param['fullName']) != '')
        {
            $name = explode(' ', $param['fullName']);
            
            $request = $request->whereHas('Customer', function ($q) use ($name) {
                $q->whereHas('PersonInfo', function ($q2) use ($name) {
                    $q2->where('first_name', 'like', '%'.$name[0].'%')
                    ->Orwhere('last_name', 'like', '%'.array_get($name, '0').'%');
                });
            });
        }
        $request = $request->orderBy('created_at', 'DESC');
        $request = $request->paginate($limit);

        return $request;
    }

    public function selectRegistrationRemind($param=null, $limit=20, $count = false)
    {
        $request = \App\Registration::with(array('user','customer', 'province'))->select(array('*'));

        $request = $request->whereRaw('30 > TIMESTAMPDIFF(DAY, now(), register_expired_date)');

        if(isset($param['regis_number']) && trim($param['regis_number']) != '')
        {
            $request = $request->where('register_number', 'like', $param['regis_number'].'%');
        }

        if(isset($param['fullName']) && trim($param['fullName']) != '')
        {
            $name = explode(' ', $param['fullName']);
            
            $request = $request->whereHas('Customer', function ($q) use ($name) {
                $q->whereHas('PersonInfo', function ($q2) use ($name) {
                    $q2->where('first_name', 'like', '%'.$name[0].'%')
                    ->Orwhere('last_name', 'like', '%'.array_get($name, '0').'%');
                });
            });
        }

        if(isset($param['branch']) && trim($param['branch']) != '')
        {
            $branch_id = $param['branch'];
            $request = $request->whereHas('User', function ($q) use ($branch_id) {
                $q->whereHas('Branch', function ($q2) use ($branch_id) {
                    $q2->where('id', '=', $branch_id);
                });
            });
        }

        if(isset($param['is_follow_up']) && trim($param['is_follow_up']) != '')
        {
            $request = $request->where('is_follow_up', '=', $param['is_follow_up']);
        }

        if(isset($param['register_expired_date_start']) && trim($param['register_expired_date_start']) != '')
        {
            $request = $request->where('register_expired_date', '>=', $param['register_expired_date_start'].' 00:00:00');
        }

        if(isset($param['register_expired_date_end']) && trim($param['register_expired_date_end']) != '')
        {
            $request = $request->where('register_expired_date', '<=', $param['register_expired_date_end'].' 23:59:59');
        }
        

        $request = $request->orderBy('register_expired_date', 'desc');
        $request = $request->paginate($limit);
        if($count){
            return $request->count();
        }
         // dd(\DB::getQueryLog());
        return $request;
    }


    public function carDetail()
    {
        return $this->hasOne('\App\CarDetail', 'id', 'car_detail_id')->select()->with(array('categoryCar'));
    }

    public function province()
    {
        return $this->hasOne('\App\Province', 'id', 'province_id')->select();
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer', 'customer_id', 'id')->select()->with(array('personInfo'));
    }

    public function carDetail2()
    {
        return $this->hasOne('\App\CarDetail', 'id', 'car_detail_id')->select(array('id','ry'));
    }

    public function user()
    {
        return $this->belongsTo('\App\User', 'em_id', 'id')->select()->with(array('branch'));
    }
}
