<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CarDetail extends Model
{
	public function createCarDetail($params = null)
    {
    	$response = false;
    	if($params)
    	{
    		$carDetail = new CarDetail;

			$carDetail->category_car_id = array_get($params, 'car_type');
			$carDetail->manner_car = array_get($params, 'manner_car');
			$carDetail->ry = array_get($params, 'ry');
			$carDetail->car_brand = array_get($params, 'car_brand', '-');
			$carDetail->model = array_get($params, 'car_model', '-');
			$carDetail->model_year = array_get($params, 'car_year');
			$carDetail->color = array_get($params, 'car_color');
			$carDetail->chassis_number = array_get($params, 'chassis_number_number');
			$carDetail->chassis_number_position = array_get($params, 'chassis_number_number_area');
			$carDetail->machine_brand = array_get($params, 'machine_brand');
			$carDetail->machine_number = array_get($params, 'machine_number');
			$carDetail->machine_number_position = array_get($params, 'machine_number_area');
			$carDetail->fuel = array_get($params, 'fuel');
			$carDetail->fuel_number = array_get($params, 'fuel_number');
			$carDetail->piston_amount = array_get($params, 'piston_amount');
			$carDetail->cc = array_get($params, 'cc');
			$carDetail->horsepower = array_get($params, 'horsepower');
			$carDetail->wheel_type = array_get($params, 'wheel_type');
			$carDetail->weight_car = array_get($params, 'weight_car');
			$carDetail->weight_load = array_get($params, 'weight_load');
			$carDetail->weight_car_total = array_get($params, 'weight_car_total');
			$carDetail->seat_amount = array_get($params, 'seat_amount');

			$response = $carDetail->save();
			$response = $carDetail->id;
    	}

    	return $response;
    }

    public function updateCarDetail($params = null)
    {
    	$response = false;
    	if($params)
    	{
    		$carDetail = \App\CarDetail::find($params['car_id']);
    		
			$carDetail->category_car_id = array_get($params, 'car_type');
			$carDetail->manner_car = array_get($params, 'manner_car');
			$carDetail->ry = array_get($params, 'ry');
			$carDetail->car_brand = array_get($params, 'car_brand', '-');
			$carDetail->model = array_get($params, 'car_model', '-');
			$carDetail->model_year = array_get($params, 'car_year');
			$carDetail->color = array_get($params, 'car_color');
			$carDetail->chassis_number = array_get($params, 'chassis_number_number');
			$carDetail->chassis_number_position = array_get($params, 'chassis_number_number_area');
			$carDetail->machine_brand = array_get($params, 'machine_brand');
			$carDetail->machine_number = array_get($params, 'machine_number');
			$carDetail->machine_number_position = array_get($params, 'machine_number_area');
			$carDetail->fuel = array_get($params, 'fuel');
			$carDetail->fuel_number = array_get($params, 'fuel_number');
			$carDetail->piston_amount = array_get($params, 'piston_amount');
			$carDetail->cc = array_get($params, 'cc');
			$carDetail->horsepower = array_get($params, 'horsepower');
			$carDetail->wheel_type = array_get($params, 'wheel_type');
			$carDetail->weight_car = array_get($params, 'weight_car');
			$carDetail->weight_load = array_get($params, 'weight_load');
			$carDetail->weight_car_total = array_get($params, 'weight_car_total');
			$carDetail->seat_amount = array_get($params, 'seat_amount');

			$response = $carDetail->save();
    	}

    	return $response;
    }

    public function categoryCar()
    {
        return $this->hasOne('\App\CategoryCar', 'id', 'category_car_id')->select();
    }
}
