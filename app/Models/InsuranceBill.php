<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceBill extends Model
{
	public function createInsuranceBill($params = null)
	{
		$response = false;
    	if($params)
    	{
    		$insuranceBill = new InsuranceBill;
    		// $insuranceBill->insurance_company_id = $params['company'];
            // $insuranceBill->insurance_type_id = $params['type'];
    		$insuranceBill->insurance_text = array_get($params, 'text');
    		$insuranceBill->insurance_price = $params['price'];

			$response = $insuranceBill->save();
			$response = $insuranceBill->id;
    	}

    	return $response;
	}

    public function updateInsuranceBill($params = null)
    {
        $response = false;
        if($params)
        {
            $insuranceBill =  \App\InsuranceBill::find($params['insurance_id']);

            // $insuranceBill->insurance_company_id = $params['company'];
            // $insuranceBill->insurance_type_id = $params['type'];
            $insuranceBill->insurance_text = $params['text'];
            $insuranceBill->insurance_price = $params['price'];

            $response = $insuranceBill->save();
            $response = $insuranceBill->id;
        }

        return $response;
    }

    public function insuranceCompany()
    {
        return $this->belongsTo('\App\InsuranceCompany', 'insurance_company_id', 'id')->select();
    }

    public function insuranceType()
    {
        return $this->belongsTo('\App\InsuranceType', 'insurance_type_id', 'id')->select();
    }
}