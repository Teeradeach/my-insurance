<?php

namespace App\Jobs;

use App\User;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReminderEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $from;
    protected $to;
    protected $subject;
    protected $html;

    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @return void
     */
    public function __construct(array $data)
    {
        $this->to = $data['to'];
        $this->from = array_get($data, 'from', 'test i_kai20@hotmail.com');
        $this->subject = array_get($data, 'subject', 'Test SQS');
        $this->html = array_get($data, 'html', '<h2>Hello SQS</h2>');
    }

    /**
     * Execute the job.
     *
     * @param  Mailer  $mailer
     * @return void
     */
    public function handle()
    {
        $body = array(
                'from'    => $this->from,
                'to'      => $this->to,
                'subject' => $this->subject,
                'html'    => $this->html
            );
        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://api.mailgun.net/v3/mg.weloveshopping.com/messages',[
            'auth' => ['api', 'key-1440fc50e9be39195683771cf87dd27e'],
            'body'=>$body,
        ]);   
    }
}