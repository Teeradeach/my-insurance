<?php
namespace App\Libs;
use Auth;

class Etc
{
	public function setExpiredRegisAmount() 
	{
		// $count = \App\Registration::with(array('user','customer'))->select(array('*'))->whereRaw('30 > TIMESTAMPDIFF(DAY, now(), register_expired_date)')->count();
		$request = \App\Registration::with(array('user','customer'))->select(array('*'));
        $request = $request->whereRaw('30 > TIMESTAMPDIFF(DAY, now(), register_expired_date)');
		$branch_id = @Auth::user()->branch->id;
        $request = $request->whereHas('User', function ($q) use ($branch_id) {
            $q->whereHas('Branch', function ($q2) use ($branch_id) {
                $q2->where('id', '=', $branch_id);
            });
		});

		$count = $request->count();
		
		\Request::session()->put('expired_regis_amount', $count);
		\Request::session()->save();
	}
}