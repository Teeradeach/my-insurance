<?php

if (!function_exists('alert')) {
    function alert()
    {
        $arg_list = func_get_args();
        foreach ($arg_list as $k => $v) {
            print '<pre>';
            print_r($v);
            print '</pre>';
        }
    }
}

if (!function_exists('convertFormatDate')) {
    function convertFormatDate($date)
    {
        if($date)
        {
            $arr_birthday = explode('/', $date);
            return ($arr_birthday[2] - 543).'-'.$arr_birthday[1].'-'.$arr_birthday[0];
        }
        return '-';
    }
}

if (!function_exists('convert_TH_FormatDate')) {
    function convert_TH_FormatDate($date)
    {
        if($date)
        {
            $date = date("d/m/Y", strtotime($date));
            $arr_birthday = explode('/', $date);

            return $arr_birthday[0].'/'.$arr_birthday[1].'/'.($arr_birthday[2] + 543);    
        }
        return '-';
        
    }
}

if (!function_exists('getImgFile')) {
    function getImgFile($filename = '')
    {
        $path = url('upload/images').'/'.$filename;
        return $path;
    }
}

if (!function_exists('genBillNo')) {
    function genBillNo($id = '', $no_length = 7)
    {
        $id_length = strlen((string) $id);
        $n = $no_length - $id_length;
        $zero_num = '';
        
        for ($i=0 ; $i < $n ; $i++) {
            $zero_num .= '0';
        }
        $res = $zero_num.($id+1);
        
        return $res;
    }
}

if (!function_exists('getBillNo')) {
    function getBillNo($id = '', $no_length = 7)
    {
        $id_length = strlen((string) $id);
        $n = $no_length - $id_length;
        $zero_num = '';
        
        for ($i=0 ; $i < $n ; $i++) {
            $zero_num .= '0';
        }
        $res = $zero_num.($id);
        
        return $res;
    }
}

if (!function_exists('convertDateRange')) {
    function convertDateRange($dateRange)
    {
        $return = null;
        $dateRange = explode(' - ', $dateRange);
        $date = $dateRange[0];
        $arrDate = explode('/', $date);
        $return['bill_date_start'] = $arrDate[2].'-'.$arrDate[1].'-'.($arrDate[0]);
        
        $date = $dateRange[1];
        $arrDate = explode('/', $date);

        $return['bill_date_end'] = $arrDate[2].'-'.$arrDate[1].'-'.($arrDate[0]);
        
        return $return;
    }
}