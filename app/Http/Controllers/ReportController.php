<?php 
namespace App\Http\Controllers;

use Auth;
use Symfony\Component\Console\Input\Input;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;

class ReportController extends Controller {
	
	public function __construct()
    {
        $etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();
    }

	public function getIndex() {
		
	}

	public function getBillReport(Request $request)
	{
		$input = $request->all();

		$limit = array_get($input, 'limit', 20);
		
		if(isset($input['bill_date']) && $input['bill_date'] != '')
		{
			$dateRange = convertDateRange($input['bill_date']);
			$input['bill_date_start'] = $dateRange['bill_date_start'];
			$input['bill_date_end'] = $dateRange['bill_date_end'];
		}
		// else 
		// {
		// 	$date = date('Y-m-d');
		// 	$d = date('Y-m-d',strtotime($date . "-30 days"));
			
		// 	$input['bill_date_start'] = $d;
		// 	$input['bill_date_end'] = date('Y-m-d');
		// }
		$branch = \App\Branch::all()->toArray();

		if(@Auth::user()->branch->branch_name && array_get($input, 'branch', null) == null ) {
			
			// foreach ($branch as $key => $value) {
			// 	if($value['branch_name'] == Auth::user()->branch->branch_name){
			// 		$input['branch'] = $value['id'];
			// 	}
			// }
			$input['branch'] = Auth::user()->branch->id;
		}

		$bill = new \App\Bill();
		$res = $bill->selectBill($input);

		$view['branch'] = $branch;
		$view['pages'] = $res->paginate($limit);
		$view['list'] = $res->paginate($limit)->toArray();
        $view['user'] = Auth::user();
        $view['bill_type'] = 1;

		return view('main.report.bill', $view);
    }
    
    public function getBillReport2(Request $request)
	{
		$input = $request->all();

		$limit = array_get($input, 'limit', 20);
		
		if(isset($input['bill_date']) && $input['bill_date'] != '')
		{
			$dateRange = convertDateRange($input['bill_date']);
			$input['bill_date_start'] = $dateRange['bill_date_start'];
			$input['bill_date_end'] = $dateRange['bill_date_end'];
		}
		// else 
		// {
		// 	$date = date('Y-m-d');
		// 	$d = date('Y-m-d',strtotime($date . "-30 days"));
			
		// 	$input['bill_date_start'] = $d;
		// 	$input['bill_date_end'] = date('Y-m-d');
		// }
		$branch = \App\Branch::all()->toArray();

		if(@Auth::user()->branch->branch_name && array_get($input, 'branch', null) == null ) {
			
			// foreach ($branch as $key => $value) {
			// 	if($value['branch_name'] == Auth::user()->branch->branch_name){
			// 		$input['branch'] = $value['id'];
			// 	}
			// }
			$input['branch'] = Auth::user()->branch->id;
		}

		$bill = new \App\Bill();
		$res = $bill->selectBill($input, 2);

		$view['branch'] = $branch;
		$view['pages'] = $res->paginate($limit);
		$view['list'] = $res->paginate($limit)->toArray();
        $view['user'] = Auth::user();
        $view['bill_type'] = 2;

		return view('main.report.bill2', $view);
	}

	public function processPrintBill($cus_id = null, $regis_id = null, $bill_id = null)
	{
		$customer = \App\Customer::with(array('personInfo'))->where('id', '=', $cus_id)->get()->toArray();
		
		$registration = \App\Registration::select('register_expired_date')->where('id', '=', $regis_id)->get()->toArray();
		
		$bill = \App\Bill::where('id', '=', $bill_id)
						   ->where('customer_id', '=', $cus_id)
						   ->where('regis_id', '=', $regis_id)
						   ->get()->toArray();
		
		if(count($bill) <= 0){
			$view['user'] = Auth::user();
			return view('main.404', $view);
		}

		$view['input']['cus_id'] = $cus_id;
		$view['input']['regis_id'] = $regis_id;
		$view['input']['register_expired_date'] = $registration[0]['register_expired_date'];
		$view['input']['total'] = $bill[0]['total'];
		$view['input']['bill_id'] = $bill_id;

		if (isset($bill[0]['act_price']) && $bill[0]['act_price'] > 0) {
			$view['input']['act']['chk'] = 'on';
			$view['input']['act']['price'] = $bill[0]['act_price'];
		}

		if (isset($bill[0]['vat_price']) && $bill[0]['vat_price'] > 0) {
			$view['input']['vat']['chk'] = 'on';
			$view['input']['vat']['price'] = $bill[0]['vat_price'];
		}

		if (isset($bill[0]['tra_price']) && $bill[0]['tra_price'] > 0) {
			$view['input']['tra']['chk'] = 'on';
			$view['input']['tra']['price'] = $bill[0]['tra_price'];
		}

		if (isset($bill[0]['insurance_id']) && $bill[0]['insurance_id'] != null) {
			$insurance = \App\InsuranceBill::with(array('insuranceCompany', 'insuranceType'))->where('id', '=', $bill[0]['insurance_id'])->get()->toArray();
			$view['input']['insurance']['chk'] = 'on';
			// $view['input']['insurance']['company'] = $insurance[0]['insurance_company']['id'];
			// $view['input']['insurance']['type'] = $insurance[0]['insurance_type']['id'];
			$view['input']['insurance']['text'] = $insurance[0]['insurance_text'];
			$view['input']['insurance']['price'] = $insurance[0]['insurance_price'];
		}

		if (isset($bill[0]['service_price']) && $bill[0]['service_price'] > 0) {
			$view['input']['service']['chk'] = 'on';
			$view['input']['service']['price'] = $bill[0]['service_price'];
		}

		if (isset($bill[0]['discount_price']) && $bill[0]['discount_price'] > 0) {
			$view['input']['discount']['chk'] = 'on';
			$view['input']['discount']['price'] = $bill[0]['discount_price'];
		}

		if (isset($bill[0]['etc_price']) && $bill[0]['etc_price'] > 0) {
			$view['input']['etc']['chk'] = 'on';
			$view['input']['etc']['price'] = $bill[0]['etc_price'];
		}

		if(\Request::get('version') == '2'){
            $view['input']['version'] = \Request::get('version');
        }

		return view('main.act.process-bill', $view);
	}

	public function getEditBill($cus_id=null, $regis_id=null, $bill_id=null)
	{
		$view['user'] = Auth::user();

		$prefix = \App\Prefix::all()->toArray();
		$view['prefix_name'] = $prefix;

		$view['province'] = \App\Province::all()->toArray();
		
		$cus = \App\Customer::with(array('personInfo'))->where('id', '=', $cus_id)->get()->toArray();
		$view['customer'] = $cus[0];
		// alert($view['customer']);

		$regis = \App\Registration::with(array('carDetail'))->where('id', '=', $regis_id)->get()->toArray();
		$view['regis'] = array_get($regis, '0', 0);
		// alert($view['regis']);

		$bill = \App\Bill::with(array('insuranceBill'))->where('id', '=', $bill_id)->get()->toArray();
		$view['bill'] = $bill[0];
		// alert($view['bill']);exit;

		return view('main.report.edit-bill', $view);
	}

	public function postEditBill(Request $request)
	{
		$input = $request->all();
		$input['em_id'] = Auth::user()->id;
		
		$validator = Validator::make($input, [
            'cus_id' => 'required',
            'regis_id' => 'required',
            'bill_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            // 'phone' => 'required|numeric',
            'ry' => 'required|numeric',
            'registration' => 'required',
            'province' => 'required',
            'register_expired_date' => 'required|date_format:"d/m/Y"',
        ]);
        
        if ( $validator->fails() ) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

        if (!empty(array_get($input,'act.text'))) {
        	$input['act']['text'] = 'พ.ร.บ. ' . $input['act']['text'];
        }

		// alert($input);exit;

		$cus = \App\Customer::with(array('personInfo'))->where('id', '=', $input['cus_id'])->get()->toArray();
		// alert($input);exit;

		$input['person_info_id'] = $cus[0]['person_info_id'];
		$input['titleName'] = $input['prefix_id'];
    	$input['tel'] = $input['phone'];
    	$input['registration_number'] = $input['registration'];

    	$input['expired_registration_date'] = convertFormatDate($input['register_expired_date']);

        // Update data info
        $personInfo = new \App\PersonInfo();
		$input['personInfo_id'] = $personInfo->updatePersonInfo($input);
		// แก้ไข
		if($input['regis_id'] == 0){
			$carDetail = new \App\CarDetail();
			$input['car_detail_id'] = $carDetail->createCarDetail($input);

			$registration = new \App\Registration();
			$registration_id = $registration->createRegistration($input);
			$input['regis_id'] = $registration_id;
		} else {
			$registration = new \App\Registration();
		    $registration_id = $registration->updateRegistration($input);

		    $carDetail = new \App\CarDetail();
			$car_id = $carDetail->updateCarDetail($input);
		}
		
		// if(!isset($input['insurance']) || $input['insurance']['insurance_id']==null||!isset($input['insurance']['chk'])){
		// 	unset($input['insurance']);
		// }
		// alert($input);exit;
		// Update Bill
		$bill_id = $this->updateBill($input);

		$input['version'] = 2;

		
		
		$view['input'] = $input;
        $view['input']['version'] = 2;

		return view('main.act.process-bill', $view);

	}

	public function updateBill($input)
	{
		$insurance_bill_id = null;
		
		if(isset($input['insurance']) && $input['insurance']['insurance_id'] > 0)
		{

			$insur =  \App\InsuranceBill::find($input['insurance']['insurance_id']);
			if($insur)
			{
				$insuranceBill = new \App\InsuranceBill();
				$insurance_bill_id = $insuranceBill->updateInsuranceBill($input['insurance']);	
			}
			else
			{
				$insuranceBill = new \App\InsuranceBill();
				$insurance_bill_id = $insuranceBill->createInsuranceBill($input['insurance']);
			}
			
		} elseif(isset($input['insurance']['price']) && $input['insurance']['price'] > 0) {

			$insuranceBill = new \App\InsuranceBill();
			$insurance_bill_id = $insuranceBill->createInsuranceBill($input['insurance']);
		}

		$input['insurance_bill_id'] = $insurance_bill_id;
		$bill = new \App\Bill();
		$response = $bill->updateBill($input);

		return $response;
	}

	public function exportBill(Request $request)
	{
		$input = $request->all();

		// $limit = array_get($input, 'limit', 500);
		
		if(isset($input['bill_date']) && $input['bill_date'] != '')
		{
			$dateRange = convertDateRange($input['bill_date']);
			$input['bill_date_start'] = $dateRange['bill_date_start'];
			$input['bill_date_end'] = $dateRange['bill_date_end'];
		}

		$branch = \App\Branch::all()->toArray();

		if(@Auth::user()->branch->branch_name && array_get($input, 'branch', null) == null ) {
			// foreach ($branch as $key => $value) {
			// 	if($value['branch_name'] == Auth::user()->branch->branch_name){
			// 		$input['branch'] = $value['id'];
			// 	}
			// }
			$input['branch'] = Auth::user()->branch->id;
		}

		$bull = new \App\Bill();
        $res = $bull->selectBill($input, $input['bill_type']);
        $bill_type[1] = 'บิลเงินสด';
        $bill_type[2] = 'ใบแจ้งหนี้';
		// $dataList['pages'] = $res->paginate($limit);
		// $dataList = $res->paginate($limit)->toArray();
        $dataList['data'] = $res->get()->toArray();
		if(isset($dataList['data']) && count($dataList['data']) > 0)
		{
			foreach ($dataList['data'] as $key => $value) 
			{
                $data['เลขที่บิล'] = getBillNo(array_get($value, 'id', 0));
                $data['ประเภทบิล'] = $bill_type[array_get($value, 'bill_type', 1)];
				$data['ชื่อ-สกุล'] = array_get($value, 'customer.person_info.first_name').'  '.array_get($value, 'customer.person_info.last_name');
				$data['เบอร์โทรศัพท์'] = array_get($value, 'customer.person_info.phone');
	            $data['เลขทะเบียน'] = array_get($value, 'registration.register_number');
	            $data['จังหวัด'] = array_get($value, 'registration.province.province');
	            $data['วันที่ออกบิล'] = convert_TH_FormatDate(array_get($value, 'created_at'));
	            $data['วันที่หมดอายุ'] = convert_TH_FormatDate(array_get($value, 'registration.register_expired_date'));
	            $data['สาขา'] = array_get($value, 'user.branch.branch_name');
	            $data['พ.ร.บ. (บาท)'] = (float) array_get($value, 'act_price', 0.00);
	            $data['ภาษี (บาท)'] = (float) array_get($value, 'vat_price', 0.00);
	            $data['ตรอ. (บาท)'] = (float) array_get($value, 'tra_price', 0.00);
	            $data['ประกันภัย (บาท)'] = (float) array_get($value, 'insurance_bill.insurance_price', 0.00);
	            $data['บริการ (บาท)'] = (float) array_get($value, 'service_price', 0.00);
	            $data['ส่วนลด (บาท)'] = (float) array_get($value, 'discount_price', 0.00);
	            $data['ชื่อรายการ อื่นๆ'] = array_get($value, 'etc_text', null);
	            $data['อื่นๆ (บาท)'] = (float) array_get($value, 'etc_price', 0.00);
	            $data['รวม (บาท)'] = (float) array_get($value, 'total', 0.00);
	            $excel_data[] = $data;
			}
		}
		else
		{
            $data['เลขที่บิล'] = '';
            $data['ประเภทบิล'] = '';
			$data['ชื่อ-สกุล'] = '';
			$data['เบอร์โทรศัพท์'] =  '';
            $data['เลขทะเบียน'] = '';
            $data['จังหวัด'] = '';
            $data['วันที่ออกบิล'] = '';
            $data['วันที่หมดอายุ'] = '';
            $data['สาขา'] = '';
            $data['พ.ร.บ. (บาท)'] = '';
            $data['ภาษี (บาท)'] = '';
            $data['ตรอ. (บาท)'] = '';
            $data['ประกันภัย (บาท)'] = '';
            $data['บริการ (บาท)'] = '';
            $data['ส่วนลด (บาท)'] = '';
            $data['ชื่อรายการ อื่นๆ'] = '';
            $data['อื่นๆ (บาท)'] = '';
            $data['รวม (บาท)'] = '';
            $excel_data[] = $data;
		}
		
		// alert($excel_data);exit;
		$title = 'Bill_report'.'_'.date('dmYHis');
		$filename = 'Bill_report'.'_'.date('dmYHis');


		return Excel::create($filename, function ($excel) use ($excel_data, $title) {
            $excel->sheet($title, function ($sheet) use ($excel_data) {
                $sheet->setColumnFormat(array(
                    'H' => '0.00',
                    'I' => '0.00',
                    'J' => '0.00',
                    'K' => '0.00',
                    'L' => '0.00',
                    'M' => '0.00',
                    'N' => '0.00',
                    'P' => '0.00',
                ));

                $sheet->cells('I:N', function($row) {
                	$row->setAlignment('right');
				});

				$sheet->cells('P:Q', function($row) {
                	$row->setAlignment('right');
				});

                $sheet->fromArray($excel_data);
                $sheet->row(1, function ($row) {
                    // call cell manipulation methods
                    $row->setAlignment('center');
                    $row->setBackground('#CCCCCC');
                    $row->setFontWeight('bold');
                    $row->setFontSize(14);
                });

            });

        })->download('xlsx');
	}


}