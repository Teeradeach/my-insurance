<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;
class LabController extends Controller
{
	public function getTest()
	{
		echo '<img src="'.getImgFile('1471668785.jpeg').'" >';
	}


	public function getGenBillNo()
	{
		$branch = \App\Branch::all()->toArray();
		foreach ($branch as $key => $value) {
			$branch_id = $value['id'];
			$request = \App\Bill::with(array('user'));
			
			$request->whereHas('User', function ($q) use ($branch_id) {
                $q->whereHas('Branch', function ($q2) use ($branch_id) {
                    $q2->where('id', '=', $branch_id);
                });
            });
			
			$bill[$value['branch_name']] = $request->orderBy('id', 'ASC')->get()->toArray();

		}

		foreach ($bill as $key => $value) {
			if(count($value) > 0) {
				$n=0;
				foreach ($value as $kk => $vv) {
					$n++;
					$rr = \App\Bill::find($vv['id']);
					$rr->bill_no = sprintf("%07s",   $n);
					$rr->save();
					echo $key.'->'.'id_'.$vv['id'].'->'.sprintf("%07s",   $n)."<br/>";
				}
			}
		}

	}
}