<?php 
namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class LoginController extends Controller {
	
	public function getLogin() {
		if(Auth::check()){
			return redirect('/');
		}else{
			return view('main.login');
		}
	}

	public function postProcess(Request $request) {

		$input = $request->all();
		$validator = Validator::make($input, [
        	'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
        	$request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$username = $request->input('username');
		$password = $request->input('password');
		
		if(Auth::attempt(['username' => $username,'password'=>$password],$request->has('remember'))){
			return redirect()->intended('/renew-act/bill-create');
		}else{
			return redirect()->back()->with('message',"Error!! Username or Password Incorrect. \nPlease try again.");
		}
	}

	public function getLogout() {
		Auth::logout();
		return redirect('/login');
	}
}