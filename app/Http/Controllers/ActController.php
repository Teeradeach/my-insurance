<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;

class ActController extends Controller
{
	public function getIndex(Request $request)
	{
		$etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();
        
		$input = $request->all();

		$limit = array_get($input, 'limit', 20);
		$view['user'] = Auth::user();

		$regis = new \App\Registration();
		$res = $regis->selectRegistrationInfo($input, $limit);

		$view['pages'] = $res;
		$view['list'] = $res->toArray();

		return view('main.act.index', $view);
	}

	public function getDetail($cus_id, $regis_id)
	{
		$view['user'] = Auth::user();
		$customer = \App\Customer::with(array('personInfo', 'occupant', 'fileInfo'))->where('id', '=', $cus_id)->get()->toArray();

		$registration = \App\Registration::with(array('province', 'carDetail'))->where('id', '=', $regis_id)->get()->toArray();

		if(count($customer) == 0 || count($registration) == 0)
		{
			return view('main.404', $view);
		}
		
		$view['customer'] = $customer[0];
		$view['registration'] = $registration[0];

		return view('main.act.detail', $view);
	}

	public function getBill($cus_id = null, $regis_id = null)
	{
		\Request::session()->forget('chk_data');

		$view['user'] = Auth::user();

		$customer = \App\Customer::with(array('personInfo'))->where('id', '=', $cus_id)->get()->toArray();
		
		$registration = \App\Registration::with(array('province', 'carDetail'))->where('id', '=', $regis_id)->get()->toArray();
		
		$bill = \App\Bill::where('customer_id', '=', $customer[0]['id'])->orderBy('created_at', 'desc')->first();
		
		if($bill) {
			$bill = $bill->toArray();
			$bill_no = genBillNo($bill['bill_no']);
		}
		else {
			$bill_no = genBillNo(0);
		}

		if(count($customer) == 0 || count($registration) == 0)
		{
			return view('main.404', $view);
		}

		$view['customer'] = $customer[0];
		$view['registration'] = $registration[0];
		$view['bill_no'] = $bill_no;
		
		$view['service_price'] = $registration[0]['car_detail']['ry']==12?50:0;
		$view['act_price'] = 0;
		$view['vat_price'] = 0;
		
		if (in_array($registration[0]['car_detail']['ry'], array(1,2,3))) {
			$view['tra_price'] = 200;
		}
		else if ($registration[0]['car_detail']['ry'] == 12) {
			$view['tra_price'] = 60;
		}
		else {
			$view['tra_price'] = 0;
		}

		
		if ( $registration[0]['car_detail']['ry'] == 1 ) {
			$total = 645;
			$vat = 0;
			// $vat = round($total*7/107,2);
			$view['act_price'] = $total-$vat;
			$view['vat_price'] = $vat;
		}
		else if ($registration[0]['car_detail']['ry'] == 2) {
			$total = 1182;
			$vat = 0;
			// $vat = round($total*7/107,2);
			$view['act_price'] = $total-$vat;
			$view['vat_price'] = $vat;
		}
		else if ($registration[0]['car_detail']['ry'] == 3) {
			$total = 968;
			$vat = 0;
			// $vat = round($total*7/107,2);
			$view['act_price'] = $total-$vat;
			$view['vat_price'] = $vat;
		}
		else if ($registration[0]['car_detail']['ry'] == 12) {
			$total = 324;
			$vat = 0;
			// $vat = round($total*7/107,2);
			$view['act_price'] = $total-$vat;
			$view['vat_price'] = $vat;
		}

		$register_expired_date = explode(' ', $view['registration']['register_expired_date'])[0];
		$register_expired_date = explode('-', $register_expired_date);

		$view['register_expired_date'] = $register_expired_date[2].'/'.$register_expired_date[1].'/'.(date('Y')+544);

	
		
		return view('main.act.bill', $view);
	}

	public function postProcessBill(Request $request)
	{
		$input = $request->all();
		$input['em_id'] = Auth::user()->id;
		$request->session()->forget('chk_data');

		$wrapper = $this->wrapperBillData($input);
		$wrapper['cus_id'] = $input['cus_id'];
		$wrapper['regis_id'] = $input['regis_id'];
		$wrapper['total'] = $input['total'];

		$md5_data = md5(serialize($wrapper));
		$sessionData = $request->session()->get('chk_data');

		if($request->session()->has('chk_data'))
		{
			if($sessionData != $md5_data)
			{
				$request->session()->put('chk_data', $md5_data);
				$request->session()->save();

				$bill_id = $this->createBill($input);

				$registration = new \App\Registration();
				$registration_id = $registration->updateRegistrationExpiredDate($input);

				$input['bill_id'] = $bill_id;
			}
			
		}
		else 
		{
			$request->session()->put('chk_data', $md5_data);
			$request->session()->save();
    		
    		$bill_id = $this->createBill($input);

    		$registration = new \App\Registration();
    		$registration_id = $registration->updateRegistrationExpiredDate($input);

    		$input['bill_id'] = $bill_id;
		}

		\Request::session()->forget('expired_regis_amount');

		$view['input'] = $input;

		return view('main.act.process-bill', $view);
	}

	public function postPrintBill(Request $request)
	{
		$input = $request->all();
		$user = Auth::user();
		// alert($input);exit;

		$wrapper = $this->wrapperBillData($input);

		$branch = \App\Branch::where('id', '=', $user->branch_id)->get()->toArray();
		
		if(isset($branch[0]['bill_name']) && $branch[0]['bill_name'])
		{
			$view['bill_name'] = array_get($branch,'0.bill_name');
			
		}
		else
		{
			$view['bill_name'] = 'สถานตรวจสภาพรถ พิชิตกวิน บ้านนา';
			
		}

		if(isset($branch[0]['bill_address']) && $branch[0]['bill_address']) 
		{
			$view['bill_address'] = array_get($branch,'0.bill_address');	
		}
		else
		{
			$view['bill_address'] = '204/1 หมู่ที่ 5 ตำบลป่าขะ อำเภอบ้านนา จังหวัดนครนายค 26110 โทร. 037-382244';
		}

		

		$customer = \App\Customer::with(array('personInfo'))->where('id', '=', $input['cus_id'])->get()->toArray()[0];
		$registration = \App\Registration::with(array('province'))->where('id', '=', $input['regis_id'])->get()->toArray()[0];
		
		// $bill_no = getBillNo($input['bill_id']);
		$bill_no = $input['bill_no'];

		$view['register_expired_date'] = convert_TH_FormatDate($registration['register_expired_date']);

		$view['customer'] = $customer;
		$view['registration'] = $registration;
		$view['bill_no'] = $bill_no;
		$view['bill_info'] = $wrapper;
		$view['total'] = $input['total'];
		$view['bill_type'] = $input['bill_type'];

        if ( array_get($input, 'version') ==2 )
        {
            return view('main.act.print-bill-v2', $view);
        }

		return view('main.act.print-bill', $view);
	}

	public function createBill($input)
	{
		$insurance_bill_id = null;
		if(isset($input['insurance']))
		{
			$insuranceBill = new \App\InsuranceBill();
			$insurance_bill_id = $insuranceBill->createInsuranceBill($input['insurance']);
		}
		$input['insurance_bill_id'] = $insurance_bill_id;

		$bill = new \App\Bill();
		$response = $bill->createBill($input);

		return $response;
	}
	
	public function wrapperBillData($input)
	{
		$response = null;

		if(isset($input['act']) && $input['act']['price'] != '')
		{
			$title = 'พ.ร.บ.';

			if(isset($input['act']['text']) && $input['act']['text'])
				$title = $input['act']['text'];

			$response[] = [
					'title' => $title,
					'amount' => 1,
					'price' => $input['act']['price'],
			];
		}
		if(isset($input['vat']) && $input['vat']['price'] != '')
		{
			$response[] = [
					'title' => 'ภาษี',
					'amount' => 1,
					'price' => $input['vat']['price'],
			];
		}
		if(isset($input['tra']) && $input['tra']['price'] != '')
		{
			$response[] = [
					'title' => 'ตรอ.',
					'amount' => 1,
					'price' => $input['tra']['price'],
			];
		}
		if(isset($input['insurance']) && $input['insurance']['price'] != '')
		{
			// $company = \App\InsuranceCompany::where('id', '=', $input['insurance']['company'])->get()->toArray();
			// $type = \App\InsuranceType::where('id', '=', $input['insurance']['type'])->get()->toArray();

			$title = $input['insurance']['text'];
			$response[] = [
					'title' => $title,
					'amount' => 1,
					'price' => $input['insurance']['price'],
			];
		}
		if(isset($input['service']) && $input['service']['price'] != '')
		{
			$response[] = [
					'title' => 'บริการ',
					'amount' => 1,
					'price' => $input['service']['price'],
			];
		}
		if(isset($input['discount']) && $input['discount']['price'] != '')
		{
			$response[] = [
					'title' => 'ส่วนลด',
					'amount' => 1,
					'price' => -$input['discount']['price'],
			];
		}
		if(isset($input['etc']) && $input['etc']['price'] != '')
		{
			$title = 'อื่นๆ';

			if(isset($input['etc']['text']) && $input['etc']['text'])
				$title = $input['etc']['text'];
			
			$response[] = [
					'title' => $title,
					'amount' => 1,
					'price' => $input['etc']['price'],
			];
		}

		for ($i=0; $i < 7; $i++) { 
			if(!isset($response[$i])){
				$response[] = [
					'title' => null,
					'amount' => null,
					'price' => null,
				];
			}
		}

		return $response;
	}

	public function getInsuranceCompany()
	{
		$insuranceCompany = \App\InsuranceCompany::select()->where('active', '=', 1)->get()->toArray();
		$response = json_encode($insuranceCompany);
		
		return $response;
	}

	public function getInsuranceType($company_id = null)
	{
		$insuranceType = \App\InsuranceType::where('insurance_company_id', '=', $company_id)->where('active', '=', 1)->get()->toArray();
		$response = json_encode($insuranceType);
		
		return $response;
	}

	public function getDataByName($full_name = null)
	{
		$data = explode('_', $full_name);
		
		$request = \App\PersonInfo::select(array('id', 'first_name', 'last_name', 'phone'))->with('customer2');

		if (isset($data[0]) && trim($data[0]) != '') {
			$request = $request->where('first_name', '=', $data[0]);
		}

		if (isset($data[1]) && trim($data[1]) != '') {
			$request = $request->where('last_name', '=', $data[1]);
		}
					
		$request = $request->get()->toArray();

        $c = count($request);

        if(!$c)
        {
            return 'null';
        }

        $arr_lname = array();
        foreach ($request as $value) {
            $arr_lname[] = $value['last_name'];
        }

        $arr_regis = array();
        $arr_province = array();
        $arr_ry = array();
        if ( count($request[0]['customer2']['registration2']) > 0 )
        {
            foreach ($request[0]['customer2']['registration2'] as $value)
            {
                $arr_regis[] = $value['register_number'];
                $arr_province[$value['register_number']] = $value['province']['id'];
                $arr_ry[$value['register_number']] = $value['car_detail2']['ry'];
            }
        }


        $data2['data'] = $request;
        $data2['last_names'] = $arr_lname;
        $data2['arr_regis'] = $arr_regis;
        $data2['arr_province'] = $arr_province;
        $data2['arr_ry'] = $arr_ry;
        $response = json_encode($data2);

		return $response;
    }
    
    public function getDataByRegistration($registration = null)
	{	
        // $request = \App\PersonInfo::select(array('id', 'prefix_id', 'first_name', 'last_name', 'phone'))->with(['customer2', 'prefix']);
        // $request = $request->whereHas('customer2', function($query) use ($registration){
        //     $query->whereHas('registration2', function($query2)use ($registration) {
        //         $query2->where('register_number', 'like', $registration."%");
        //     });
            
        // });

        $request = \App\Registration::select(['id', 'customer_id', 'province_id', 'register_number'])->with('province')->where('register_number', 'like', $registration."%");
        $regis = $request->first();
        if($regis== null){
            return 'null';
        }
        

        $request = \App\Customer::select()->with(['personInfo'])->where('id',$regis['customer_id']);
        $request = $request->first();

        if($request== null){
            return 'null';
        }
        $request = $request->toArray();
        
        // alert($request);
        // exit;

        $c = count($request);

        if(!$c)
        {
            return 'null';
        }

        $request = $request['person_info'];
        $request['registration'] = $regis->toArray();
        // $arr_lname = array();
        // foreach ($request as $value) {
        //     $arr_lname[] = $value['last_name'];
        // }

        // $arr_regis = array();
        // $arr_province = array();
        // $arr_ry = array();
        // if ( count($request[0]['customer2']['registration2']) > 0 )
        // {
        //     foreach ($request[0]['customer2']['registration2'] as $value)
        //     {
        //         $arr_regis[] = $value['register_number'];
        //         $arr_province[$value['register_number']] = $value['province']['id'];
        //         $arr_ry[$value['register_number']] = $value['car_detail2']['ry'];
        //     }
        // }


        $data2['data'] = $request;
        // $data2['last_names'] = $arr_lname;
        // $data2['arr_regis'] = $arr_regis;
        // $data2['arr_province'] = $arr_province;
        // $data2['arr_ry'] = $arr_ry;
        $response = json_encode($data2);

		return $response;
	}

	public function getBillCreate()
	{
		$view['user'] =  Auth::user();
		$prefix = \App\Prefix::all()->toArray();
		$view['prefix_name'] = $prefix;
		$view['province'] = \App\Province::all()->toArray();
		$person_info = \App\PersonInfo::select(array('first_name'))->get()->toArray();
		$arrName = array();
		foreach ($person_info as $value) {
			$arrName[] = '"'.$value['first_name'].'"';
		}
		$arrName = array_unique($arrName);
		$names = implode(',', $arrName);

		$view['first_names'] = $names;

		$branch = \App\Branch::where('id', '=', Auth::user()->branch_id)->get()->toArray();
		
		if(isset($branch[0]['bill_name']) && $branch[0]['bill_name'])
		{
			$view['bill_name'] = array_get($branch,'0.bill_name');
		}
		else
		{
			$view['bill_name'] = 'สถานตรวจสภาพรถ พิชิตกวิน บ้านนา';
		}

		if(isset($branch[0]['bill_address']) && $branch[0]['bill_address']) 
		{
			$view['bill_address'] = array_get($branch,'0.bill_address');	
		}
		else
		{
			$view['bill_address'] = '204/1 หมู่ที่ 5 ตำบลป่าขะ อำเภอบ้านนา จังหวัดนครนายค 26110 โทร. 037-382244';
		}


		return view('main.act.bill-create', $view);
	}

	public function postProcessBillV2(Request $request)
	{
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'firstName' => 'required',
            'lastName' => 'required',
            // 'phone' => 'required',
            'ry' => 'required|numeric',
            'registration' => 'required',
            'province' => 'required',
            'register_expired_date' => 'required|date_format:"d/m/Y"',
        ]);
        if ( $validator->fails() ) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

        
        
        $input['em_id'] = Auth::user()->id;
        
        // $wrapper = $this->wrapperBillData($input);
        
        if (!empty(array_get($input,'act.text'))) {
        	$input['act']['text'] = 'พ.ร.บ. ' . $input['act']['text'];
        }


        $countData = \App\PersonInfo::where('first_name','=', $input['firstName'])
                        ->where('last_name','=', $input['lastName'])
                        ->get()
                        ->count();

        if ( $countData > 0 )
        {
            $data = \App\PersonInfo::select(array('id', 'phone'))
                ->where('first_name','=', $input['firstName'])
                ->where('last_name','=', $input['lastName'])
                ->get()->toArray();
			
            $idPerson = $data[0]['id'];
            $dataRegis = \App\Customer::select(array('customers.id as cus_id', 'registrations.id as regis_id'))
                        ->join('registrations', 'registrations.customer_id', '=', 'customers.id')
                        ->where('customers.person_info_id','=', $idPerson)
                        ->where('registrations.register_number','=', $input['registration'])
                        ->get()->toArray();

            if(!empty($input['phone'])) {
            	if ($input['phone'] != $data[0]['phone']) {
            		$update_person = \App\PersonInfo::find($idPerson);
		            $update_person->phone = array_get($input, 'phone');
		            $update_person->save();
            	}
            }

            if( count($dataRegis) > 0 ) 
            {
            	$input['cus_id'] = $dataRegis[0]['cus_id'];
            	$input['regis_id'] = $dataRegis[0]['regis_id'];

                $bill_id = $this->createBill($input);
        		
	    		$registration = new \App\Registration();
	    		$registration_id = $registration->updateRegistrationExpiredDate($input);

	    		$input['bill_id'] = $bill_id;
            } 
            else 
            {
            	
            	$arr_date = explode('/', $input['register_expired_date']);
        		$input['expired_registration_date'] = ($arr_date[2]-543).'-'.$arr_date[1].'-'.$arr_date[0];

            	$dataRegis = \App\Customer::select(array('customers.id as cus_id'))
                        ->where('customers.person_info_id','=', $idPerson)
                        ->get()->toArray();
            	
            	$input['cus_id'] = $dataRegis[0]['cus_id'];
            	$input['registration_number'] = $input['registration'];

            	$carDetail = new \App\CarDetail();
				$input['car_detail_id'] = $carDetail->createCarDetail($input);

				$registration = new \App\Registration();
				$registration_id = $registration->createRegistration($input);

				$input['regis_id'] = $registration_id;

				$bill_id = $this->createBill($input);
				$input['bill_id'] = $bill_id;
            }

        }
        else
        {
        	$input['titleName'] = $input['prefix_id'];
        	$input['tel'] = empty($input['phone']) ? '-' : $input['phone'];
        	$input['registration_number'] = $input['registration'];
        	
        	$arr_date = explode('/', $input['register_expired_date']);
        	// $input['register_expired_date'] = $arr_date[2].'/'.$arr_date[1].'/'.($arr_date[0]);
        	// $input['expired_registration_date'] = $input['register_expired_date'];
        	$input['expired_registration_date'] = ($arr_date[2]-543).'-'.$arr_date[1].'-'.$arr_date[0];

            // alert($input);exit;
            // Create PersonInfo
            $personInfo = new \App\PersonInfo();
			$input['personInfo_id'] = $personInfo->createPersonInfo($input);

			// Create Occupant
			$occupant = new \App\Occupant();
			$input['occupant_id'] = $occupant->createOccupant($input);

			// Create Customer
			$customer = new \App\Customer();
			$cus_id = $customer->createCustomer($input);
			$input['cus_id'] = $cus_id;

			// Create Car -----------
			$res = \App\Registration::select()
                    ->where('province_id', '=', $input['province'])
                    ->where('register_number', '=', $input['registration_number'])
                    ->get()->toArray();

            if(count($res)==0)
            {
				$carDetail = new \App\CarDetail();
				$input['car_detail_id'] = $carDetail->createCarDetail($input);	
            }
            // ----------------------

			$registration = new \App\Registration();
			$registration_id = $registration->createRegistration($input);
			$input['regis_id'] = $registration_id;

			// Create bill
			$bill_id = $this->createBill($input);
    		$input['bill_id'] = $bill_id;
        }
        
        // --------------- Bill no -----------------
        $branch_id = Auth::user()->branch->id;
        $request = \App\Bill::select('bill_no')
        			->with(array('user'))
					->whereHas('User', function ($q) use ($branch_id) {
            			$q->whereHas('Branch', function ($q2) use ($branch_id) {
                			$q2->where('id', '=', $branch_id);
            			});
        			})->limit(2)->orderBy('id', 'DESC')->get()->toArray();
        $n = ((int)$request[1]['bill_no']) + 1;
        $bill_no = sprintf("%07s",   $n);
        
        $rr = \App\Bill::find($input['bill_id']);
					$rr->bill_no = sprintf("%07s",   $n);
					$rr->save();

        $input['bill_no'] = $bill_no;
        // --------------- Bill no -----------------

        $view['input'] = $input;
        $view['input']['version'] = 2;

		return view('main.act.process-bill', $view);
    }

}


