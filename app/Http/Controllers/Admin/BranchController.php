<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;

class BranchController extends Controller
{
	public function getIndex()
	{
		$res = \App\Branch::select('*')->orderBy('created_at','DESC');
		$view['list'] = $res->paginate(20)->toArray();
		$view['pages'] = $res->paginate(20);

		$view['user'] = Auth::user();
		return view('main.admin.branch.index', $view);
	}

	public function getCreate()
	{
		$view['user'] = Auth::user();
		return view('main.admin.branch.create', $view);
	}

	public function postCreateProcess(Request $request)
	{
		$input = $request->all();

		$validator = Validator::make($input, [
        	'branch_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$data = new \App\Branch();
		$data->branch_name = trim($input['branch_name']);
		$data->bill_name = array_get($input, 'bill_name');
		$data->bill_address = array_get($input, 'bill_address');
		$response = $data->save();

		return redirect('admin/branch');
	}
	

	public function getEdit($id)
	{
		$res = \App\Branch::find($id)->toArray();

		$view['user'] = Auth::user();
		$view['branch'] = $res;

		return view('main.admin.branch.edit', $view);
	}

	public function postEditProcess(Request $request)
	{
		$input = $request->all();

		$validator = Validator::make($input, [
        	'branch_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$data = \App\Branch::find($input['id']);
		$data->branch_name = trim($input['branch_name']);
		$data->bill_name = $input['bill_name'];
		$data->bill_address = $input['bill_address'];

		$response = $data->save();

		return redirect('admin/branch');
	}
}