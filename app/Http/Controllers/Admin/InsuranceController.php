<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;

class InsuranceController extends Controller
{
	public function getIndex()
	{
		$insur = \App\InsuranceCompany::with(array('insuranceType'))->orderBy('created_at','DESC');
		$view['list'] = $insur->paginate(20)->toArray();
		$view['pages'] = $insur->paginate(20);

		$view['user'] = Auth::user();
		
		return view('main.admin.insurance.index', $view);
	}

	public function getCreateCompany()
	{
		$view['user'] = Auth::user();
		return view('main.admin.insurance.create-company', $view);
	}

	public function postCreateCompanyProcess(Request $request)
	{
		$input = $request->all();

		$validator = Validator::make($input, [
        	'company_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$data = new \App\InsuranceCompany();
		$data->company_name = trim($input['company_name']);
		$response = $data->save();

		return redirect('admin/insurance');
	}

	
	public function getEditCompany($id)
	{
		$insur = \App\InsuranceCompany::find($id)->toArray();
		$view['data'] = $insur;

		$view['user'] = Auth::user();
		
		return view('main.admin.insurance.edit-company', $view);
	}

	public function postEditCompanyProcess(Request $request)
	{
		$input = $request->all();

		$validator = Validator::make($input, [
        	'company_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$data = \App\InsuranceCompany::find($input['id']);
		$data->company_name = trim($input['company_name']);

		$response = $data->save();

		return redirect('admin/insurance');
		
	}

	public function getView($id)
	{
		$insurCompany = \App\InsuranceCompany::select('company_name')->where('id', '=', $id)->get()->toArray();
		$view['company_name'] = $insurCompany[0]['company_name'];
		$view['company_id'] = $id;

		$insur = \App\InsuranceType::where('insurance_company_id', '=', $id)->orderBy('created_at','ASC');
		$view['list'] = $insur->paginate(20)->toArray();
		$view['pages'] = $insur->paginate(20);

		$view['user'] = Auth::user();
		// alert($view['list']);die;
		return view('main.admin.insurance.view', $view);
	}

	public function getCreateType($id)
	{
		$view['user'] = Auth::user();
		$insurCompany = \App\InsuranceCompany::select('company_name')->where('id', '=', $id)->get()->toArray();
		$view['company_name'] = $insurCompany[0]['company_name'];

		$view['company_id'] = $id;
		return view('main.admin.insurance.create-type', $view);
	}

	public function postCreateTypeProcess(Request $request)
	{
		$input = $request->all();
		
		$validator = Validator::make($input, [
        	'type_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$data = new \App\InsuranceType();
		$data->insurance_company_id = trim($input['company_id']);
		$data->type_name = trim($input['type_name']);
		$response = $data->save();

		return redirect('admin/insurance/view/'.$input['company_id']);
	}

	public function getEditType($id)
	{
		$insur = \App\InsuranceType::with(array('insuranceCompany'))->find($id)->toArray();
		$view['data'] = $insur;
		$view['company_name'] = $insur['insurance_company']['company_name']; 

		$view['user'] = Auth::user();
		
		return view('main.admin.insurance.edit-type', $view);
	}

	public function postEditTypeProcess(Request $request)
	{
		$input = $request->all();

		$validator = Validator::make($input, [
        	'type_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$data = \App\InsuranceType::find($input['id']);
		$data->type_name = trim($input['type_name']);

		$response = $data->save();

		return redirect('admin/insurance/view/'.$input['id_company']);
		
	}

	public function getHideCompany($id, $active)
	{
		$data = \App\InsuranceCompany::find($id);
		$data->active = $active==1?0:1;

		$response = $data->save();

		return redirect('admin/insurance');
	}

	public function getHideType($id, $active, $company_id)
	{
		$data = \App\InsuranceType::find($id);
		$data->active = $active==1?0:1;

		$response = $data->save();

		return redirect('admin/insurance/view/'.$company_id);
	}

	
}