<?php
namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;

class RemindController extends Controller {
	
	public function getIndex(Request $request)
	{
		$etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();

		$input = $request->all();

		$limit = array_get($input, 'limit', 20);
		$view['user'] = Auth::user();
		// -----
		// if (!isset($input['is_follow_up'])) {
		// 	$input['is_follow_up'] = 0;
		// }
		// -----

		$branch = \App\Branch::all()->toArray();
		if(@Auth::user()->branch->branch_name && array_get($input, 'branch', null) == null ) {
			// foreach ($branch as $key => $value) {
			// 	if($value['branch_name'] == Auth::user()->branch->branch_name){
			// 		$input['branch'] = $value['id'];
			// 	}
			// }
			$input['branch'] = Auth::user()->branch->id;
		}
		if(isset($input['register_expired_date']) && $input['register_expired_date'] != '')
		{
			$dateRange = convertDateRange($input['register_expired_date']);
			$input['register_expired_date_start'] = $dateRange['bill_date_start'];
			$input['register_expired_date_end'] = $dateRange['bill_date_end'];
		}

		$regis = new \App\Registration();
		$res = $regis->selectRegistrationRemind($input, $limit);

		$view['pages'] = $res;
		$view['list'] = $res->toArray();
		$view['branch'] = $branch;
		// alert($view['list']);exit;
		
		return view('main.remind.index', $view);
	}

	public function getDetail($cus_id, $regis_id)
	{
		$view['user'] = Auth::user();
		$customer = \App\Customer::with(array('personInfo', 'occupant', 'fileInfo'))->where('id', '=', $cus_id)->get()->toArray();

		$registration = \App\Registration::with(array('province', 'carDetail'))->where('id', '=', $regis_id)->get()->toArray();

		if(count($customer) == 0 || count($registration) == 0)
		{
			return view('main.404', $view);
		}
		
		$view['customer'] = $customer[0];
		$view['registration'] = $registration[0];

		return view('main.remind.detail', $view);
	}

	public function setFollow(Request $request,$regis_id, $is_follow_up=0)
	{
		$query_string = '';
		if (count($request->all())>0) {
			$query_string = '?'.http_build_query($request->all());
		}
		
		$registration = \App\Registration::find($regis_id);
		if($registration)
        {
            $registration->is_follow_up = $is_follow_up;
            $response = $registration->save();
        }


		return redirect('remind'.$query_string);
	}

	public function exportRemind(Request $request) {
		$input = $request->all();

		$limit = array_get($input, 'limit', 20);
		$view['user'] = Auth::user();
		// -----
		// if (!isset($input['is_follow_up'])) {
		// 	$input['is_follow_up'] = 0;
		// }
		// -----

		$branch = \App\Branch::all()->toArray();
		if(@Auth::user()->branch->branch_name && array_get($input, 'branch', null) == null ) {
			// foreach ($branch as $key => $value) {
			// 	if($value['branch_name'] == Auth::user()->branch->branch_name){
			// 		$input['branch'] = $value['id'];
			// 	}
			// }
			$input['branch'] = Auth::user()->branch->id;
		}

		if(isset($input['register_expired_date']) && $input['register_expired_date'] != '')
		{
			$dateRange = convertDateRange($input['register_expired_date']);
			$input['register_expired_date_start'] = $dateRange['bill_date_start'];
			$input['register_expired_date_end'] = $dateRange['bill_date_end'];
		}

		$regis = new \App\Registration();
		$res = $regis->selectRegistrationRemind($input, $limit);
		
		// $dataList['pages'] = $res;
		$dataList = $res->toArray();

		if(isset($dataList['data']) && count($dataList['data']) > 0)
		{
			foreach ($dataList['data'] as $key => $value) 
			{
				$data['ทะเบียนรถ'] = $value['register_number'].' '. $value['province']['province'];
				$data['สาขาร้าน'] = array_get($value, 'user.branch.branch_name');
				$data['วันจดทะเบียน'] =  convert_TH_FormatDate($value['register_date']);
	            $data['วันหมดอายุ'] = convert_TH_FormatDate($value['register_expired_date']);
	            $data['ชื่อ-สุกล'] = $value['customer']['person_info']['prefix']['prefix_name'].' '.$value['customer']['person_info']['first_name'].' '.$value['customer']['person_info']['last_name'];
	            $data['ที่อยู่'] = $value['customer']['person_info']['address'];
	            $data['เบอร์โทรศัพท์'] = $value['customer']['person_info']['phone'];
	            $data['การติดตามลูกค้า'] = $value['is_follow_up']==1?'ติดตามแล้ว':'รอติดตาม';
	            $excel_data[] = $data;
	        }
		} else {
			$data['ทะเบียนรถ'] = '';
			$data['สาขาร้าน'] = '';
			$data['วันจดทะเบียน'] =  '';
            $data['วันหมดอายุ'] = '';
            $data['ชื่อ-สุกล'] = '';
            $data['ที่อยู่'] = '';
            $data['เบอร์โทรศัพท์'] = '';
            $data['การติดตามลูกค้า'] = '';
            $excel_data[] = $data;
		}

		// alert($excel_data);exit;
		$title = 'remind_report'.'_'.date('dmYHis');
		$filename = 'remind_report'.'_'.date('dmYHis');

		return Excel::create($filename, function ($excel) use ($excel_data, $title) {
            $excel->sheet($title, function ($sheet) use ($excel_data) {

				$sheet->cells('D', function($row) {
                	$row->setFontColor('#c61925');
				});

                $sheet->fromArray($excel_data);
                $sheet->row(1, function ($row) {
                    // call cell manipulation methods
                    $row->setFontColor('#000000');
                    $row->setFontWeight('bold');
                    $row->setFontSize(14);
                    $row->setBackground('#CCCCCC');
                    $row->setAlignment('center');
                });

            });

        })->download('xlsx');
	}
}