<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;
class CarController extends Controller
{
	public function getCreateCarRegistration($cus_id)
	{
		$view['user'] = Auth::user();
		$customer = \App\Customer::with(array('personInfo'))->select(array('person_info_id'))->where('id', '=', $cus_id)->get()->toArray();
		
		if(count($customer) == 0)
		{
			return view('main.404', $view);
		}

		$view['person_info'] = $customer[0]['person_info'];
		$view['province'] = \App\Province::all()->toArray();
		$view['categoryCar'] = \App\CategoryCar::all()->toArray();
		
		return view('main.manage_member.create_car_registration', $view);
	}

	public function postProcessCreateCarRegistration(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make($input, [
			'cus_id' => 'required',
        	'registration_date' => 'required|date_format:"d/m/Y"',
        	'expired_registration_date' => 'required|date_format:"d/m/Y"',
            'registration_number' => 'required',
            'province' => 'required',
            'car_type' => 'required',
            'ry' => 'required',
            // 'manner_car' => 'required',
            // 'car_brand' => 'required',
            // 'car_model' => 'required',
            // 'car_year' => 'required|date_format:"Y"',
            // 'car_color' => 'required',
            'chassis_number_number' => 'required',
            // 'chassis_number_number_area' => 'required',
            // 'machine_brand' => 'required',
            'machine_number' => 'required',
            // 'machine_number_area' => 'required',
            // 'piston_amount' => 'required',
            // 'cc' => 'required',
            // 'horsepower' => 'required',
        ]);

        if ($validator->fails()) {
        	$request->flash();
            return redirect()->back()->withErrors($validator);
        }

        $registration_id = false;

        $etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();
        // echo \Request::session()->get('expired_regis_amount');exit;
		$input['em_id'] = Auth::user()->id;
		$input['registration_date'] = convertFormatDate($input['registration_date']);
		$input['expired_registration_date'] = convertFormatDate($input['expired_registration_date']);

		$carDetail = new \App\CarDetail();
		$input['car_detail_id'] = $carDetail->createCarDetail($input);

		$registration = new \App\Registration();
		$registration_id = $registration->createRegistration($input);
		
		if($registration_id)
        {
        	return redirect('manage-member/create-car-registration/'.$input['cus_id'].'?regis_id='.$registration_id.'&regis_number='.$input['registration_number'])->with('message',"บันทึกข้อมูล ทะเบียนสำเร็จ");	
        }
        else
        {
        	$request->flash();
            return redirect()->back()->with('message_fali',"บันทึกข้อมูลไม่สำเร็จโปรดลองใหม่อีกครั้งสำเร็จ");
        }
	}

	public function getEditCarRegistration($cus_id, $regis_id)
	{
		$view['registration'] = \App\Registration::with(array('carDetail', 'province'))
		->where('customer_id', '=', $cus_id)
		->where('id', '=', $regis_id)
		->get()->toArray();
        // alert($view['registration']);exit;
		$view['province'] = \App\Province::all()->toArray();
		$view['categoryCar'] = \App\CategoryCar::all()->toArray();
		$view['cus_id'] = $cus_id;
		$view['regis_id'] = $regis_id;
		$view['user'] = Auth::user();

		return view('main.manage_member.edit_car_registration', $view);
	}

	public function postProcessEditCarRegistration(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make($input, [
			'cus_id' => 'required',
            'registration_date' => 'required|date_format:"d/m/Y"',
            'expired_registration_date' => 'required|date_format:"d/m/Y"',
            'registration_number' => 'required',
            'province' => 'required',
            'car_type' => 'required',
            'ry' => 'required',
            // 'manner_car' => 'required',
            // 'car_brand' => 'required',
            // 'car_model' => 'required',
            // 'car_year' => 'required|date_format:"Y"',
            // 'car_color' => 'required',
            'chassis_number_number' => 'required',
            // 'chassis_number_number_area' => 'required',
            // 'machine_brand' => 'required',
            'machine_number' => 'required',
            // 'machine_number_area' => 'required',
            // 'piston_amount' => 'required',
            // 'cc' => 'required',
            // 'horsepower' => 'required',
        ]);

        if ($validator->fails()) {
        	$request->flash();
            return redirect()->back()->withErrors($validator);
        }

        $registration_id = false;

        $etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();

		$input['em_id'] = Auth::user()->id;
		$input['registration_date'] = convertFormatDate($input['registration_date']);
		$input['expired_registration_date'] = convertFormatDate($input['expired_registration_date']);

		$carDetail = new \App\CarDetail();
		$input['car_detail_id'] = $carDetail->updateCarDetail($input);
		// alert($input);exit;
		$registration = new \App\Registration();
		$registration_id = $registration->updateRegistration($input);
		
		if($registration_id)
        {
        	return redirect('manage-member/edit-car-registration/'.$input['cus_id'].'/'.$input['regis_id'])->with('message',"บันทึกข้อมูล ทะเบียนสำเร็จ");
        }
        else
        {
        	$request->flash();
            return redirect()->back()->with('message_fali',"บันทึกข้อมูลไม่สำเร็จโปรดลองใหม่อีกครั้งสำเร็จ");
        }
	}
}


