<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades;
use App\Jobs\SendReminderEmail;
use Aws\Sqs\SqsClient;

class QueueController extends Controller {
	
	public function pushQueue(Request $request){
		$input = $request->all();
		// $config = new config();
		// \Config::set('queue.connections.sqs.queue','Url');
		// $xx = \Config::get('queue.connections.sqs');
		// dd($xx);

        // $test = $sqs->getQueueArn($url);
// dd($test);

		if($request->has('to')){
			$data['to'] = $request->input('to');
			if($request->has('from')){
				$data['from'] = $request->input('from');
			}
			if($request->has('subject')){
				$data['subject'] = $request->input('subject');
			}
			if($request->has('html')){
				$data['html'] = $request->input('html');
			}
			$this->dispatch(new SendReminderEmail($data));
			return response()->json($input);
		} else {
			return 'plase input send to';
		}
		
	}
}