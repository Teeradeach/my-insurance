<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
class DashboardController extends Controller {

	public function getIndex()
	{
		$etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();

		$user = Auth::user();
		$view = array(
					'user'	=> $user,
				);
		return view('main.dashboard.index', $view);
	}
}