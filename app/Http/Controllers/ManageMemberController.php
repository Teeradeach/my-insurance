<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\Request;
class ManageMemberController extends Controller
{
	public function getIndex(Request $request)
	{
		$etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();

		$input = $request->all();
		
		$limit = array_get($input, 'limit', 20);

		$view['user'] = Auth::user();

		$personInfo = new \App\PersonInfo();
		$res = $personInfo->selectPersonInfo($input, $limit);

		$view['pages'] = $res;
		$view['personInfo'] = $res->toArray();

		// alert($view['personInfo']);exit;
		return view('main.manage_member.index', $view);
	}

	public function getCreateMember()
	{
		$prefix = \App\Prefix::all()->toArray();
		$nationality = \App\Nationality::all()->toArray();

		$view['user'] = Auth::user();
		$view['prefix_name'] = $prefix;
		$view['nationality'] = $nationality;
		
		return view('main.manage_member.create_member', $view);
	}

	public function postProcessCreateMember(Request $request)
	{
		$input = $request->all();
		$files = array_get($input, 'file', null);
		$data_file = null;

		$validator = Validator::make($input, [
        	'titleName' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'idCard' => 'required|digits:13|numeric',
            'birthday' => 'required|date_format:"d/m/Y"',
            'nationality' => 'required',
            'address' => 'required',
            'tel' => 'required|numeric',
        ]);

        if ($validator->fails()) {
        	$request->flash();
            return redirect()->back()->withErrors($validator);
        }
        
        $input['birthday'] = convertFormatDate($input['birthday']);
        
        if($input['birthday2'] != '')
        	$input['birthday2'] = convertFormatDate($input['birthday2']);
        else
        	$input['birthday2'] = null;

		$input['em_id'] = Auth::user()->id;

		$personInfo = new \App\PersonInfo();
		$input['personInfo_id'] = $personInfo->createPersonInfo($input);

		$occupant = new \App\Occupant();
		$input['occupant_id'] = $occupant->createOccupant($input);

		$customer = new \App\Customer();
		$cus_id = $customer->createCustomer($input);

		foreach ($files as $k => $file) {
			if($file)
			{
				$path = base_path() . '/public/upload/images/';
				$imageName = uniqid('').'.'.$file->getClientOriginalExtension();
				$file->move($path, $imageName);
				$data_file[$k]['file_name'] = $imageName;
				$data_file[$k]['ext'] = $file->getClientOriginalExtension();
			}
			
		}

		if(isset($data_file))
		{
			$f = new \App\FileInfo();
			$f->createfileInfo($data_file, $cus_id);
		}
		
        if($cus_id)
        {
        	return redirect('manage-member/create-car-registration/'.$cus_id)->with('message_member',"บันทึกข้อมูล เจ้าของรถสำเร็จ");	
        }
        else
        {
        	$request->flash();
            return redirect()->back()->with('message',"บันทึกข้อมูลไม่สำเร็จโปรดลองใหม่อีกครั้งสำเร็จ");
        }
		
	}

	public function getEditMember($cus_id)
	{
		$prefix = \App\Prefix::all()->toArray();
		$nationality = \App\Nationality::all()->toArray();

		$view['user'] =  Auth::user();
		$view['prefix_name'] = $prefix;
		$view['nationality'] = $nationality;
					
		$customer = \App\Customer::with(array('personInfo', 'occupant', 'fileInfo'))->where('id', '=', $cus_id)->get()->toArray();

		$registration = \App\Registration::with(array('carDetail'))->where('customer_id', '=', array_get($customer, '0.id'))->get()->toArray();
		
		$view['person_info'] = $customer[0]['person_info'];
		$view['occupant'] = $customer[0]['occupant'];
		$view['file_info'] = $customer[0]['file_info'];
		$view['registration'] = $registration;
		
		return view('main.manage_member.edit_member', $view);
	}

	public function postProcessEditMember(Request $request)
	{
		$input = $request->all();
		$files = array_get($input, 'file', null);

		$validator = Validator::make($input, [
        	'prefix_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'id_card' => 'required|digits:13|numeric',
            'birthday' => 'required|date_format:"d/m/Y"',
            'nationality' => 'required',
            'address' => 'required',
            'tel' => 'required|numeric',
        ]);

        if ($validator->fails()) {
        	$request->flash();
            return redirect()->back()->withErrors($validator);
        }

		$input['em_id'] = Auth::user()->id;

		$input['birthday'] = convertFormatDate($input['birthday']);
        
        if($input['birthday2'] != '')
        	$input['birthday2'] = convertFormatDate($input['birthday2']);
        else
        	$input['birthday2'] = null;
        
		$personInfo = new \App\PersonInfo();
		$input['personInfo_id'] = $personInfo->updatePersonInfo($input);

		$occupant = new \App\Occupant();
		$input['occupant_id'] = $occupant->updateOccupant($input);

		foreach ($files as $k => $file) {
			if($file)
			{
				$path = base_path() . '/public/upload/images/';
				$imageName = uniqid('').'.'.$file->getClientOriginalExtension();
				$file->move($path, $imageName);
				$data_file[$k]['file_name'] = $imageName;
				$data_file[$k]['ext'] = $file->getClientOriginalExtension();
			}
			
		}

		if(isset($data_file))
		{
			$f = new \App\FileInfo();
			$f->createfileInfo($data_file, $input['cus_id']);
		}

		if($input['personInfo_id'])
        {
        	return redirect('manage-member/edit/'.$input['cus_id'])->with('message_member',"บันทึกข้อมูลสำเร็จ");	
        }
        else
        {
        	$request->flash();
            return redirect()->back()->with('message_fail',"บันทึกข้อมูลไม่สำเร็จโปรดลองใหม่อีกครั้งสำเร็จ");
        }

		
	}

	public function getRemoveFile(Request $request)
	{
		$input = $request->all();
		$response = 0;
		$path = base_path() . '/public/upload/images/';
		$resUnlink = unlink($path.$input['file_name']);

		@unlink($path.$input['file_name']);
		$response = \App\FileInfo::where('id', '=', trim($input['id']))->delete();
		

		return $response;
	}

}


