<?php 
namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller {
	
    public function __construct()
    {
        $etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();
    }

	public function getIndex() 
    {
        $etc = new \App\Libs\Etc();
        $etc->setExpiredRegisAmount();

        $branch = \App\Branch::All()->toArray();
        $view['branch'] = $branch;
        
		return view('main.register', $view);
	}

	public function postProcess(Request $request) {
		$input = $request->all();

        $type['admin'] = 1;
        $type['user'] = 2;

		$validator = Validator::make($input, [
        	'name' => 'required',
        	'username' => 'required',
            'password' => 'required|confirmed',
            'branch' => 'required',
            'type' => 'required',

        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }
        // alert($input);exit;
        $user = User::create([
            'name' => $input['name'],
            'username' => $input['username'],
            'password' => bcrypt($input['password']),
            'branch_id' => $input['branch'],
            'type' => $type[$input['type']],
        ]);

        $request->session()->flash('status', 'Register successful!');
        return redirect()->back();
	}

    public function userList()
    {
        $view['list'] = \App\User::with(array('branch'))->get()->toArray();
        $view['user'] = Auth::user();
        // alert($view);exit;
        return view('main.admin.user.index', $view);
    }

    public function userEdit($id)
    {
        $view['data'] = \App\User::with(array('branch'))->where('id', '=', $id)->get()->toArray()[0];
        $view['branch'] = \App\Branch::select(['id', 'branch_name'])->get()->toArray();
        $view['user'] = Auth::user();

        // alert($view);exit;
        return view('main.admin.user.edit', $view);
    }

    public function editProcess(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'password' => 'confirmed',
        ]);
        
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()->withErrors($validator);
        }

        $data = \App\User::find($input['id']);
        $data->name = $input['name'];
        $data->username = $input['username'];
        $data->type = $input['type'];
        $data->branch_id = $input['branch'];

        if($input['password']){
            $data->password = bcrypt($input['password']);
        }

        $response = $data->save();

        return redirect('admin/user');
    }
}