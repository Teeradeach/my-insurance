<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/pushQueue', 'QueueController@pushQueue');


Route::get('login','LoginController@getLogin');
Route::get('logout','LoginController@getLogout');
Route::post('login/process','LoginController@postProcess');


Route::controller('lab','LabController');

Route::group(['middleware' => 'auth'], function () {

	// // ข้อมูลเจ้าของรถ
	Route::get('manage-member', 'ManageMemberController@getIndex');
	Route::get('manage-member/create-member', 'ManageMemberController@getCreateMember');
	Route::post('manage-member/create-member/process', 'ManageMemberController@postProcessCreateMember');
	
	Route::get('manage-member/file/remove', 'ManageMemberController@getRemoveFile');
	
	// Edit ข้อมูลเจ้าของรถ
	Route::get('manage-member/edit/{cus_id}', 'ManageMemberController@getEditMember');
	Route::get('manage-member/edit', function(){
		return view('main.404', ['user'=>Auth::user()]);
	});
	Route::post('manage-member/edit/process', 'ManageMemberController@postProcessEditMember');

	// ข้อมูลทะเบียนรถ	
	Route::get('manage-member/create-car-registration/{cus_id}', 'CarController@getCreateCarRegistration');
	Route::get('manage-member/create-car-registration', function(){
		return view('main.404', ['user'=>Auth::user()]);
	});
	Route::post('manage-member/create-car-registration/process', 'CarController@postProcessCreateCarRegistration');

	Route::get('manage-member/edit-car-registration/{cus_id}/{regis_id}', 'CarController@getEditCarRegistration');
	Route::get('manage-member/edit-car-registration', function(){
		return view('main.404', ['user'=>Auth::user()]);
	});
	Route::post('manage-member/edit-car-registration/process', 'CarController@postProcessEditCarRegistration');
	
	
	// ต่อทะเบียน 
	Route::get('renew-act', 'ActController@getIndex');
	
	Route::get('renew-act/detail/{cus_id}/{regis_id}', 'ActController@getDetail');
	Route::get('renew-act/detail', function(){
		return view('main.404', ['user'=>Auth::user()]);
	});

	Route::get('renew-act/bill/{cus_id}/{regis_id}', 'ActController@getBill');
	Route::get('renew-act/bill', function(){
		return view('main.404', ['user'=>Auth::user()]);
	});

	Route::get('renew-act/bill-create', 'ActController@getBillCreate');

	Route::get('renew-act/insurance-info/company', 'ActController@getInsuranceCompany');
	Route::get('renew-act/insurance-info/type/{company_id}', 'ActController@getInsuranceType');
    Route::get('renew-act/insurance-info/full-name/{full_name}', 'ActController@getDataByName');
    Route::get('renew-act/insurance-info/registration/{registration}', 'ActController@getDataByRegistration');

	Route::post('renew-act/process-bill', 'ActController@postProcessBill');
    Route::post('renew-act/process-bill-v2', 'ActController@postProcessBillV2');
	Route::post('print/bill', 'ActController@postPrintBill');
	
	// Report
    Route::get('report/bill', 'ReportController@getBillReport');
    Route::get('report/bill2', 'ReportController@getBillReport2');
	Route::get('report/print/bill/process/{cus_id}/{regis_id}/{bill_id}', 'ReportController@processPrintBill');
	Route::get('report/edit-bill/{cus_id}/{regis_id}/{bill_id}', 'ReportController@getEditBill');
	Route::post('report/process-edit-bill', 'ReportController@postEditBill');

	Route::get('report/export-bill', 'ReportController@exportBill');

	// Remind
	Route::get('remind', 'RemindController@getIndex');
	Route::get('remind/detail/{cus_id}/{regis_id}', 'RemindController@getDetail');
	Route::get('remind/is_follow_up/{regis_id}/{is_follow_up}', 'RemindController@setFollow');
	Route::get('remind/export', 'RemindController@exportRemind');

	Route::group(['middleware' => 'isAdmin', 'prefix' => 'admin'], function () {
		Route::get('user','RegisterController@userList');
		Route::get('user/edit/{id}','RegisterController@userEdit');
		Route::post('user/edit-process','RegisterController@editProcess');
		Route::controller('register','RegisterController');
		Route::controller('branch','BranchController');
		Route::controller('insurance','InsuranceController');

		// Branch
	});

	Route::get('gen-bill-no', 'LabController@getGenBillNo');

	// Home page
	Route::group(['middleware' => 'isAdmin'], function () {
		Route::controller('/', 'DashboardController');
	});

	

	
});

